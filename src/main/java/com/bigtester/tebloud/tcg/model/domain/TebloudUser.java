package com.bigtester.tebloud.tcg.model.domain;

import javax.persistence.MappedSuperclass;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.List;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity(name = "demo$TebloudUser")

public class TebloudUser {//extends User {
    private static final long serialVersionUID = -8447990504332642154L;

    @JoinTable(name = "DEMO_TEBLOUD_USER_TEBLOUD_USER_ZONE_LINK",
        joinColumns = @JoinColumn(name = "TEBLOUD_USER_ID"),
        inverseJoinColumns = @JoinColumn(name = "TEBLOUD_USER_ZONE_ID"))
    //@ManyToMany
    //@OnDeleteInverse(DeletePolicy.UNLINK)
    //@OnDelete(DeletePolicy.UNLINK)
    protected List<TebloudUserZone> tebloudUserZone;

    public List<TebloudUserZone> getTebloudUserZone() {
        return tebloudUserZone;
    }

    public void setTebloudUserZone(List<TebloudUserZone> tebloudUserZone) {
        this.tebloudUserZone = tebloudUserZone;
    }




}