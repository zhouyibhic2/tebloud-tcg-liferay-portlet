package com.bigtester.tebloud.tcg.model.domain;


import com.bigtester.ate.tcg.model.domain.AbstractScreenNode;
import com.bigtester.ate.tcg.model.relationship.Relations;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.validation.constraints.NotNull;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
public class TebloudUserZone {//extends StandardEntity {

    @NotNull
    @Property
    protected String zoneDomainName;

    @GraphId
    protected Long graphId;

    @Property
    protected String nodeLabelName;

	public Set<AbstractScreenNode> getScreens() {
		return screens;
	}

	public void setScreens(Set<AbstractScreenNode> screens) {
		this.screens = screens;
	}

    /** The testcases. */
	@Relationship(type = Relations.CONTAINS_SCREEN)
	private Set<AbstractScreenNode> screens = new HashSet<AbstractScreenNode>();

	@Property
    protected Set<Long> userIds = new HashSet<Long>();

    public void setUserIds(Set<Long> tebloudUserIds) {
        this.userIds = tebloudUserIds;
    }

    public Set<Long> getUserIds() {
        return userIds;
    }


	public TebloudUserZone(String domainName, Long userId) {
		this.nodeLabelName = "TebloudUserZone";
		this.zoneDomainName = domainName;
		this.userIds.add(userId);
	}

	public TebloudUserZone() {
		this.nodeLabelName = "TebloudUserZone";
		this.zoneDomainName = "";
	}


    public void setGraphId(Long graphId) {
        this.graphId = graphId;
    }

    public Long getGraphId() {
        return graphId;
    }

    public void setNodeLabelName(String nodeLabelName) {
        this.nodeLabelName = nodeLabelName;
    }

    public String getNodeLabelName() {
        return nodeLabelName;
    }



    public void setZoneDomainName(String zoneDomainName) {
        this.zoneDomainName = zoneDomainName;
    }

    public String getZoneDomainName() {
        return zoneDomainName;
    }

    /**
	 * {@inheritDoc}
	 */
	//@Override
	public String getIdentifierString() {
		if (this.getUserIds().size()==0)
			throw new IllegalStateException("tebloudUserLogin not populated");
		return this.getNodeLabelName();
	}

}