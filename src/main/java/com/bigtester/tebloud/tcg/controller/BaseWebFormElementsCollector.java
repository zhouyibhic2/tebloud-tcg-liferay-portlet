/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.tcg.controller;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.jdt.annotation.Nullable;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.bigtester.ate.tcg.utils.GlobalUtils;
import com.bigtester.ate.tcg.utils.exception.Html2DomException;

// TODO: Auto-generated Javadoc
/**
 * This class FormElementCollector defines ....
 * 
 * @author Peidong Hu
 *
 */
public class BaseWebFormElementsCollector {

	/** The dom doc. */
	@Nullable
	private Document domDoc;

	/** The cleaned doc. */
	@Nullable
	private Document cleanedDoc;

	/** The parent frame. */
	@Nullable
	private String xpathOfParentFrame;
	public BaseWebFormElementsCollector() {
	
	}
	/**
	 * Instantiates a new web form elements collector.
	 *
	 * @param domDoc
	 *            the dom doc
	 * @param parentFrame
	 *            the parent frame, Nullable
	 * @throws ParserConfigurationException
	 *             the parser configuration exception
	 * @throws Html2DomException
	 */
	public void cleanUpDoc(Document domDoc,
			String xpathOfParentFrame) throws ParserConfigurationException,
			Html2DomException {
		this.domDoc = domDoc;
		this.xpathOfParentFrame = ""; 
		cleanedDoc = GlobalUtils.cleanUpDoc(domDoc, xpathOfParentFrame);
		this.setXpathOfParentFrame(xpathOfParentFrame);
	}

	
	/**
	 * Gets the dom doc.
	 *
	 * @return the webDriver
	 */
	public Document getDomDoc() {
		return domDoc;
	}

	/**
	 * Gets the cleaned doc.
	 *
	 * @return the cleanedDoc
	 */
	public final Document getCleanedDoc() {
		return cleanedDoc;
	}

	/**
	 * @return the xpathOfParentFrame
	 */
	public String getXpathOfParentFrame() {
		return xpathOfParentFrame;
	}

	/**
	 * @param xpathOfParentFrame
	 *            the xpathOfParentFrame to set
	 */
	public void setXpathOfParentFrame(String xpathOfParentFrame) {
		this.xpathOfParentFrame = xpathOfParentFrame;
	}
}
