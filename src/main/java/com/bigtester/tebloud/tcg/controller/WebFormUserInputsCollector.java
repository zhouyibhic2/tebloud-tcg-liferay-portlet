/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.tcg.controller;//NOPMD

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.ArrayUtils;
import org.eclipse.jdt.annotation.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.common.collect.Iterables;
import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms;
import com.bigtester.ate.tcg.model.UserInputDom;
import com.bigtester.tebloud.tcg.config.RunningModeProperties;
import com.bigtester.tebloud.tcg.config.RunningModeProperties.RunningMode;
import com.bigtester.tebloud.tcg.service.uicollection.IUiInvisibilityExceptionalPattern;
import com.bigtester.tebloud.tcg.service.uicollection.matcher.IUitrOnScreenLabelMatcher;
import com.bigtester.tebloud.tcg.service.uicollection.matcher.IUserInputConsolidationMatcher;
import com.bigtester.ate.tcg.utils.exception.Html2DomException;

import static org.joox.JOOX.*;

// TODO: Auto-generated Javadoc
/**
 * This class InputsCollector defines ....
 *
 * @author Peidong Hu
 *
 */
@Service
@Scope("prototype")
public class WebFormUserInputsCollector extends BaseWebFormElementsCollector {

	/** The false invisible filters. */
	@Autowired
	@Nullable
	private List<IUiInvisibilityExceptionalPattern> falseInvisibleFilters;

	@Autowired
	@Nullable
	private List<IUitrOnScreenLabelMatcher> labelMatchers;

	@Autowired
	@Nullable
	private List<IUserInputConsolidationMatcher> userInputConsolidationMatchers;

	@Autowired
	@Nullable
	private RunningModeProperties runningModeProperties;

	/** The user inputs. */
	final private List<UserInputDom> userInputs = new ArrayList<UserInputDom>();

	/**
	 * Instantiates a new web form user inputs collector.
	 *
	 * @param domDoc
	 *            the dom doc
	 * @param parentFrame
	 *            the parent frame nullable
	 * @throws ParserConfigurationException
	 *             the parser configuration exception
	 * @throws IOException
	 * @throws Html2DomException
	 */
	public void collectUserInputs(Document domDoc, String xpathOfParentFrame, boolean forRunner)
			throws ParserConfigurationException, IOException, Html2DomException {
		super.cleanUpDoc(domDoc, xpathOfParentFrame);
		collectUserInputs(super.getCleanedDoc(), forRunner);
	}

	public WebFormUserInputsCollector() {

		super();

	}


	private boolean isFalseInvisible(Document domDoc, Node evaluationNode, String invisiblityAttName, String invisibilityAttYesValue) {
		boolean retVal = false;
		for (int i = 0; i < getFalseInvisibleFilters().size(); i++) {
			if (getFalseInvisibleFilters().get(i).isFalseInvisible(domDoc, evaluationNode, invisiblityAttName, invisibilityAttYesValue))
				retVal = true;
		}
		return retVal;
	}

	/**
	 * Match labels.
	 *
	 * @param domDoc the dom doc
	 * @param coreInputNode the core input node
	 * @return the list
	 */
	private List<Node> matchLabels(Document domDoc, Node coreInputNode) {
		List<Node> retVal = new ArrayList<Node>();
		for (int i = 0; i < this.getLabelMatchers().size(); i++) {
			if ((retVal = getLabelMatchers().get(i).matchPattern(domDoc, coreInputNode)).size() > 0) {
				retVal.removeIf(node->!this.isVisibleAndUnprocessed(node, domDoc));
				retVal.removeIf(node->node.getTextContent()==null || node.getTextContent().trim().isEmpty());
				if (retVal.size()>0)
					break;
			}
		}
		return retVal;
	}
	private Map<UserInputDom, List<UserInputDom>> matchConsolidatedUserInputs(Document domDoc, List<UserInputDom> uids) {
		Map<UserInputDom, List<UserInputDom>> retVal = new ConcurrentHashMap<UserInputDom, List<UserInputDom>>();
		for (int i = 0; i < this.getUserInputConsolidationMatchers().size(); i++) {
			Map<UserInputDom, List<UserInputDom>> matched =getUserInputConsolidationMatchers().get(i).matchPattern(domDoc, uids);
			//Only match to one matcher pattern
			uids.removeAll(matched.values().stream().flatMap(valueList->valueList.stream()).collect(Collectors.toList()));
			retVal.putAll(matched);
		}
		return retVal;
	}
	private boolean isUserChangableInputType(Node node) {
		boolean retVal = true; // NOPMD
		String nodeTag = node.getNodeName();

		if ("input".equalsIgnoreCase(nodeTag)) {
			if (null == $(node).attr("type")) {
				retVal = true;
			} else {
				for (int i = 0; i < WebFormUserInputsCollectorHtmlTerms.USER_NOT_CHANGABLE_INPUT_TYPES.length; i++) {
					if ($(node).attr("type").equalsIgnoreCase(
							WebFormUserInputsCollectorHtmlTerms.USER_NOT_CHANGABLE_INPUT_TYPES[i])) {
						retVal = false;// NOPMD
						break;
					}
				}
			}
		} else {
			retVal = true;
		}
		return retVal;
	}

	private boolean isUserFormSubmitInputType(Node node) {
		boolean retVal = false; // NOPMD
		String nodeTag = node.getNodeName();

		if ("input".equalsIgnoreCase(nodeTag)) {
			if (null == $(node).attr("type")) {
				retVal = false;
			} else {
				for (int i = 0; i < WebFormUserInputsCollectorHtmlTerms.USER_FORM_SUBMIT_INPUT_TYPES.length; i++) {
					if ($(node).attr("type").equalsIgnoreCase(
							WebFormUserInputsCollectorHtmlTerms.USER_FORM_SUBMIT_INPUT_TYPES[i])) {
						retVal = true;// NOPMD
						break;
					}
				}
			}
		} else {
			retVal = false;
		}
		return retVal;
	}
	private boolean isUserClickableType(Node node) {
		boolean retVal = false; // NOPMD
		String nodeTag = node.getNodeName();
		for (int i = 0; i < WebFormUserInputsCollectorHtmlTerms.USER_CLICKABLE_TAGS_EXCEPT_ALINK.length; i++) {
			if ($(node).tag().equalsIgnoreCase(
					WebFormUserInputsCollectorHtmlTerms.USER_CLICKABLE_TAGS_EXCEPT_ALINK[i])) {
				retVal = true;// NOPMD
				break;
			}
		}

		for (int i = 0; i < WebFormUserInputsCollectorHtmlTerms.USER_NOT_CHANGABLE_INPUT_TYPES.length; i++) {
			if ($(node).attr("type").equalsIgnoreCase(
					WebFormUserInputsCollectorHtmlTerms.USER_NOT_CHANGABLE_INPUT_TYPES[i])) {
				retVal = false;// NOPMD
				break;
			}
		}

		return retVal;
	}

	/**
	 * Collect user inputs.
	 *
	 * @param cleanedDoc
	 *            the cleaned doc
	 * @param originalDoc
	 *            the original doc
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void collectUserInputs(Document cleanedDoc, boolean forRunner) throws IOException {

		for (int j = 0; j < WebFormUserInputsCollectorHtmlTerms.USER_COLLECTABLE_TAGS_EXCEPT_ALINK.length; j++) {
			NodeList htmlInputs = cleanedDoc
					.getElementsByTagName(WebFormUserInputsCollectorHtmlTerms.USER_COLLECTABLE_TAGS_EXCEPT_ALINK[j]);

			if (null != htmlInputs) {
				//System.out.println(htmlInputs.item(0).getNodeName());
				collectHtmlElements(htmlInputs, cleanedDoc);
			}

		}
		//add input consolidation code here to consolidate radio button or checkbox groups
		Map<UserInputDom, List<UserInputDom>> consolidatedResult = matchConsolidatedUserInputs(cleanedDoc, this.getUserInputs());
		this.getUserInputs().addAll(consolidatedResult.keySet());

		if (runningModeProperties.getRunningMode().equals(RunningMode.USECASE_APP.toString()) && !forRunner) {
			NodeList htmlInputs = cleanedDoc.getElementsByTagName("a");
			if (null != htmlInputs)
				collectHtmlLinkElements(htmlInputs, cleanedDoc);
		}
//
//		for (int j = 0; j < USER_CHANGABLE_INPUT_TAGS.length; j++) {
//			NodeList htmlInputs = cleanedDoc
//					.getElementsByTagName(USER_CHANGABLE_INPUT_TAGS[j]);
//			if (null != htmlInputs)
//				collectHtmlElements(htmlInputs, cleanedDoc);
//
//		}
//		for (int j = 0; j < USER_CLICKABLE_TAGS.length; j++) {
//			NodeList htmlClickables = cleanedDoc
//					.getElementsByTagName(USER_CLICKABLE_TAGS[j]);
//			if (null != htmlClickables)
//				collectHtmlElements(htmlClickables, cleanedDoc);
//		}
	}

	private boolean hasInvisibleParent(Node coreNode, Document cleanedDoc) {
		List<Element> parentsInvisible = $(coreNode).parents().get();
		boolean invisibleParent = false;
		for (Element parent : parentsInvisible) {

			if (parent.getAttribute(WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME)
					.equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE)
					&& !isFalseInvisible(cleanedDoc, parent,
							WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME, WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE)) {
				invisibleParent = true;
				break;
			}
		}
		return invisibleParent;
	}

	private boolean isVisibleAndUnprocessed(Node coreNode, Document cleanedDoc) {
		boolean invisibleParent = hasInvisibleParent(coreNode, cleanedDoc);
		return !(((Element) coreNode).getAttribute(WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME)
				.equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE) && !isFalseInvisible(cleanedDoc,
				coreNode, WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME, WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE))
				&& !((Element) coreNode).getAttribute(WebFormUserInputsCollectorHtmlTerms.PREVIOUS_PROCESSED_ATTR_NAME)
				.equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE)
				&& !invisibleParent;

	}

	private void collectHtmlLinkElements(NodeList htmlElements, Document cleanedDoc) {
		for (int i = 0; i < htmlElements.getLength(); i++) {
			Node coreNode = htmlElements.item(i);
			if (isVisibleAndUnprocessed(coreNode, cleanedDoc)) {
				if (null != coreNode) {
						userInputs.add(initLinkUserInputDom(coreNode));
				}
			}
		}

	}

	private void collectHtmlElements(NodeList htmlElements, Document cleanedDoc) {
		for (int i = 0; i < htmlElements.getLength(); i++) {
			Node coreNode = htmlElements.item(i);


//			List<Element> parentsInvisible = $(coreNode).parents().get();
//			boolean invisibleParent = false;
//			for (Element parent : parentsInvisible) {
//
//				if (parent.getAttribute(ATE_INVISIBLE_ATTR_NAME)
//						.equalsIgnoreCase(ATE_YES_ATTR_VALUE)
//						&& !isFalseInvisible(cleanedDoc, parent,
//								ATE_INVISIBLE_ATTR_NAME, ATE_YES_ATTR_VALUE)) {
//					invisibleParent = true;
//					break;
//				}
//			}
			if (isVisibleAndUnprocessed(coreNode, cleanedDoc) && (isUserChangableInputType(coreNode)
							|| isUserFormSubmitInputType(coreNode) || isUserClickableType(coreNode))) {
				List<Element> parentsUntilForm = $(coreNode).parentsUntil(
						"form").get();// NOPMD
				if (null != coreNode
						&& (parentsUntilForm.isEmpty() || !Iterables
								.get(parentsUntilForm,
										parentsUntilForm.size() - 1)
								.getNodeName().equalsIgnoreCase("html"))

						) {

					if (parentsUntilForm.isEmpty()) {
						Node coreNodeParent = coreNode.getParentNode();

						userInputs.add(initUserInputDomInsideOfForm(
								cleanedDoc, coreNode, coreNodeParent));

					} else {
						List<Element> parents = $(coreNode)
								.parentsUntil("form").parent().get();
						Node tempNode = parents.get(parents.size() - 1);
						userInputs.add(initUserInputDomInsideOfForm(
								cleanedDoc, coreNode, tempNode));
					}
				} else if (null != coreNode
						&& Iterables
								.get(parentsUntilForm,
										parentsUntilForm.size() - 1)
								.getNodeName().equalsIgnoreCase("html")) {
					// TODO collect input out of form element
					userInputs.add(initUserInputDomOutOfForm(cleanedDoc,
							coreNode));
				}
			}
		}

	}

	private void fillOutNonLabeledFieldLabelDomPointer(
			UserInputDom valueHolder, Node searchStartingNode,
			Node searchUpEndingNode, boolean endingNodeInclusive,
			boolean leftLabeled) {
		Node tempParent2 = searchStartingNode;

		if (leftLabeled) {

			while (tempParent2.getPreviousSibling() == null
					&& !tempParent2.equals(searchUpEndingNode)) {
				// && tempParent2 != searchUpEndingNode) {
				tempParent2 = tempParent2.getParentNode();
			}
			// if tempParent2 is form node, we will use the form's
			// previous sibling as the label node;
			// Or we will use the nearest input sibling node as the
			// label node;
			if (tempParent2.equals(searchUpEndingNode) && !endingNodeInclusive)
				return; //NOPMD

			Node tempPreviousSibling = tempParent2.getPreviousSibling();
			if (null == tempPreviousSibling) {
				throw new IllegalStateException("tempPreviousSibling is null");
			} else {
				valueHolder.addLabelDomPointer(tempPreviousSibling);
			}
		} else {
			while (tempParent2.getNextSibling() == null
					&& !tempParent2.equals(searchUpEndingNode)) {
				tempParent2 = tempParent2.getParentNode();
			}
			// if tempParent2 is form node, we will use the form's
			// previous sibling as the label node;
			// Or we will use the nearest input sibling node as the
			// label node;
			if (tempParent2.equals(searchUpEndingNode) && !endingNodeInclusive)
				return;
			Node tempNextSibling = tempParent2.getNextSibling();
			if (null == tempNextSibling) {
				throw new IllegalStateException("tempNextSibling is null");
			} else {
				valueHolder.addLabelDomPointer(tempNextSibling);
			}
		}

	}

	private void fillOutAddtionalInfoNode(UserInputDom valueHolder,
			Node searchStartingNode, @Nullable Node searchUpEndingNode,
			boolean endingNodeInclusive, boolean nextSiblingOnly) {
		// fill out addtional info nodes
		Node tempParent2 = searchStartingNode;
		Node tempNode = tempParent2;// NOPMD

		while (tempParent2.getNextSibling() == null
				&& !tempParent2.equals(searchUpEndingNode)) {
			tempParent2 = tempParent2.getParentNode();
			tempNode = tempParent2; // NOPMD
		}
		if (tempNode.equals(searchUpEndingNode) && !endingNodeInclusive)
			return;
		List<Node> additionalInfoNodes = new ArrayList<Node>();
		if (nextSiblingOnly)
			additionalInfoNodes.add(tempNode.getNextSibling());
		else
			while (tempNode.getNextSibling() != null) {
				additionalInfoNodes.add(tempNode.getNextSibling());
				if ("form".equalsIgnoreCase(tempNode.getNodeName()))
					break;
				tempNode = tempNode.getNextSibling();
			}
		valueHolder.setAdditionalInfoNodes(additionalInfoNodes);
	}

	private void fillOutUserViewablePreviousSibling(UserInputDom valueHolder,
			Node searchStartingNode, @Nullable Node searchUpEndingNode,
			boolean endingNodeInclusive) {
		Node tempParent2 = searchStartingNode;

		while (tempParent2.getPreviousSibling() == null
				&& !tempParent2.equals(searchUpEndingNode)) {
			tempParent2 = tempParent2.getParentNode();
		}
		// if tempParent2 is form node, we will use the form's
		// previous sibling as the label node;
		// Or we will use the nearest input sibling node as the
		// label node;
		if (tempParent2.equals(searchUpEndingNode) && !endingNodeInclusive)
			return;
		Node tempPreviousSibling = tempParent2.getPreviousSibling();
		if (null == tempPreviousSibling) {
			throw new IllegalStateException("tempPreviousSibling is null");
		} else {
			valueHolder.setPreviousUserViewableHtmlSibling(tempPreviousSibling);
		}

	}

	private void fillOutUserViewableNextSibling(UserInputDom valueHolder,
			Node maxInputParentNoOtherChild, @Nullable Node searchUpEndingNode,
			boolean endingNodeInclusive) {

		Node tempParent2 = maxInputParentNoOtherChild;
		while (tempParent2.getNextSibling() == null
				&& !tempParent2.equals(searchUpEndingNode)) {
			// && tempParent2 != searchUpEndingNode) {
			tempParent2 = tempParent2.getParentNode();
		}
		if (tempParent2.equals(searchUpEndingNode) && !endingNodeInclusive)
			return;
		Node tempNextSibling = tempParent2.getNextSibling();
		if (null == tempNextSibling) {
			throw new IllegalStateException("tempNextSibling is null");
		} else {
		valueHolder
				.setNextUserViewableHtmlSibling(tempNextSibling);
		}
	}

	private Node getMaxInputParentNoOtherChild(Node inputNode) {
		Node tempParent2 = inputNode.getParentNode();
		Node maxInputParentNoOtherChild = inputNode; // NOPMD
		while (tempParent2 != null
				&& $(tempParent2).children().get().size() <= 1) {
			maxInputParentNoOtherChild = tempParent2;// NOPMD
			tempParent2 = tempParent2.getParentNode();
		}
		return maxInputParentNoOtherChild;
	}

	private int getNumberOfUserChangableInputsInNode(Node node) {
		int retVal = 0; // NOPMD
		for (int i = 0; i < WebFormUserInputsCollectorHtmlTerms.USER_COLLECTABLE_TAGS_EXCEPT_ALINK.length; i++) {
			retVal = $(node).find(WebFormUserInputsCollectorHtmlTerms.USER_COLLECTABLE_TAGS_EXCEPT_ALINK[i]).get().size()
					+ retVal;
		}
//		for (int i = 0; i < USER_CHANGABLE_INPUT_TAGS.length; i++) {
//			retVal = $(node).find(USER_CHANGABLE_INPUT_TAGS[i]).get().size()
//					+ retVal;
//		}
		return retVal;
	}

	private Node getMaxInputParentNoOtherInput(Node inputNode,
			boolean inputInForm) {
		Node tempParent = inputNode.getParentNode();
		Node maxInputParentNoOtherInput = inputNode; //NOPMD
		while (tempParent != null
				&& getNumberOfUserChangableInputsInNode(tempParent) <= 1) {
			maxInputParentNoOtherInput = tempParent; //NOPMD
			if (inputInForm
					&& tempParent.getNodeName().equalsIgnoreCase("form")) {
				break;
			}
			tempParent = tempParent.getParentNode();
		}
		return maxInputParentNoOtherInput;
	}

	private boolean isLeftLabeled(Node inputNode) {
		Node inputType = inputNode.getAttributes().getNamedItem("type");
		boolean retVal = true; // NOPMD
		if (null != inputType) { // NOPMD
			retVal = Arrays.asList(WebFormUserInputsCollectorHtmlTerms.LEFT_LABELED_INPUT_TYPES).contains(
					inputType.getNodeValue());
		} else if (inputNode.getNodeName().equalsIgnoreCase("textarea")) {
			retVal = true;
		} else if (inputNode.getNodeName().equalsIgnoreCase("select")) {
			retVal = true;
		}
		return retVal;
	}

	private boolean isNoNeedLabelInput(Node inputNode) {
		return !(isLeftLabeled(inputNode) || isRightLabeled(inputNode)) || isFormSubmitInput(inputNode) || isClickableButtonInput(inputNode);
	}

	private boolean isFormSubmitInput(Node inputNode) {
		Node inputType = inputNode.getAttributes().getNamedItem("type");
		boolean retVal = false; // NOPMD
		if (null != inputType) { // NOPMD
			retVal = Arrays.asList(WebFormUserInputsCollectorHtmlTerms.USER_FORM_SUBMIT_INPUT_TYPES).contains(
					inputType.getNodeValue());
		}
		return retVal;
	}


	private boolean isClickableButtonInput(Node inputNode) {
		String nodeName = inputNode.getNodeName();
		boolean retVal = false; // NOPMD
		if (null != nodeName) { // NOPMD
			retVal = Arrays.asList(WebFormUserInputsCollectorHtmlTerms.USER_CLICKABLE_TAGS_EXCEPT_ALINK).contains(
					nodeName);
		}
		return retVal;
	}

	/**
	 * @param inputNode
	 * @return
	 */
	private boolean isRightLabeled(Node inputNode) {
		Node inputType = inputNode.getAttributes().getNamedItem("type");
		boolean retVal = false; // NOPMD
		if (null != inputType) { // NOPMD
			retVal = Arrays.asList(WebFormUserInputsCollectorHtmlTerms.RIGHT_LABELED_INPUT_TYPES).contains(
					inputType.getNodeValue());
		}
		return retVal;
	}

	private boolean isVisible(Node node) {
		boolean retVal = false;
		if (node.getAttributes()==null) {
			return true;
		}
		Node attr = node.getAttributes().getNamedItem(WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME);
		if (attr == null)
			retVal = true;
		else {
			if (!attr.getNodeValue().equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE)) retVal = true;
		}
 		return retVal;
	}

	private boolean siblingHasVisibleInput(Node node) {
		boolean retVal = false; // NOPMD
		for (int i = 0; i < WebFormUserInputsCollectorHtmlTerms.USER_COLLECTABLE_TAGS_EXCEPT_ALINK.length; i++) {
			boolean nextSiblingIsInput = false; // NOPMD
			if (node.getNextSibling() != null && isVisible(node.getNextSibling())
					&& $(node.getNextSibling()).tag() != null) {
				nextSiblingIsInput = Arrays.asList(WebFormUserInputsCollectorHtmlTerms.USER_COLLECTABLE_TAGS_EXCEPT_ALINK)
						.contains($(node.getNextSibling()).tag().toLowerCase())
						|| $(node.getNextSibling()).find(
								WebFormUserInputsCollectorHtmlTerms.USER_COLLECTABLE_TAGS_EXCEPT_ALINK[i]).isNotEmpty();
			}
			boolean previousSiblingIsInput = false; //NOPMD

			if (node.getPreviousSibling() != null  && isVisible(node.getPreviousSibling())
					&& $(node.getPreviousSibling()).tag() != null) {
				previousSiblingIsInput = Arrays.asList(
						WebFormUserInputsCollectorHtmlTerms.USER_COLLECTABLE_TAGS_EXCEPT_ALINK).contains(
						$(node.getPreviousSibling()).tag().toLowerCase())
						|| $(node.getPreviousSibling()).find(
								WebFormUserInputsCollectorHtmlTerms.USER_COLLECTABLE_TAGS_EXCEPT_ALINK[i]).isNotEmpty();
			}
			retVal = nextSiblingIsInput || previousSiblingIsInput;
			if (retVal)
				break;
		}
//		for (int i = 0; i < USER_CHANGABLE_INPUT_TAGS.length; i++) {
//			boolean nextSiblingIsInput = false; // NOPMD
//			if (node.getNextSibling() != null && isVisible(node.getNextSibling())
//					&& $(node.getNextSibling()).tag() != null) {
//				nextSiblingIsInput = Arrays.asList(USER_CHANGABLE_INPUT_TAGS)
//						.contains($(node.getNextSibling()).tag().toLowerCase())
//						|| $(node.getNextSibling()).find(
//								USER_CHANGABLE_INPUT_TAGS[i]).isNotEmpty();
//			}
//			boolean previousSiblingIsInput = false; //NOPMD
//
//			if (node.getPreviousSibling() != null  && isVisible(node.getPreviousSibling())
//					&& $(node.getPreviousSibling()).tag() != null) {
//				previousSiblingIsInput = Arrays.asList(
//						USER_CHANGABLE_INPUT_TAGS).contains(
//						$(node.getPreviousSibling()).tag().toLowerCase())
//						|| $(node.getPreviousSibling()).find(
//								USER_CHANGABLE_INPUT_TAGS[i]).isNotEmpty();
//			}
//			retVal = nextSiblingIsInput || previousSiblingIsInput;
//			if (retVal)
//				break;
//		}
		return retVal;
	}

	private boolean leftRightDirectedSiblingHasNoInput(Node node,
			boolean leftLabeled) {
		boolean retVal = true; // NOPMD
		for (int i = 0; i < WebFormUserInputsCollectorHtmlTerms.USER_COLLECTABLE_TAGS_EXCEPT_ALINK.length; i++) {

			if (leftLabeled) {
				if (node.getPreviousSibling() == null) {
					retVal = true; // NOPMD
				} else if (isVisible(node.getPreviousSibling()) && Arrays.asList(WebFormUserInputsCollectorHtmlTerms.USER_COLLECTABLE_TAGS_EXCEPT_ALINK).contains(
						node.getPreviousSibling().getNodeName().toLowerCase()))
					retVal = false; //NOPMD
				else
					retVal = retVal
							&& $(node.getPreviousSibling()).find(
									WebFormUserInputsCollectorHtmlTerms.USER_COLLECTABLE_TAGS_EXCEPT_ALINK[i]).isEmpty();
			}
			if (!leftLabeled) {
				if (node.getNextSibling() == null) {
					retVal = true; // NOPMD
				} else if (isVisible(node.getNextSibling()) && Arrays.asList(WebFormUserInputsCollectorHtmlTerms.USER_COLLECTABLE_TAGS_EXCEPT_ALINK).contains(
						node.getNextSibling().getNodeName().toLowerCase()))
					retVal = false;// NOPMD
				else
					retVal = retVal
							&& $(node.getNextSibling()).find(
									WebFormUserInputsCollectorHtmlTerms.USER_COLLECTABLE_TAGS_EXCEPT_ALINK[i]).isEmpty();
			}
		}
//		for (int i = 0; i < USER_CHANGABLE_INPUT_TAGS.length; i++) {
//
//			if (leftLabeled) {
//				if (node.getPreviousSibling() == null) {
//					retVal = true; // NOPMD
//				} else if (isVisible(node.getPreviousSibling()) && Arrays.asList(USER_CHANGABLE_INPUT_TAGS).contains(
//						node.getPreviousSibling().getNodeName().toLowerCase()))
//					retVal = false; //NOPMD
//				else
//					retVal = retVal
//							&& $(node.getPreviousSibling()).find(
//									USER_CHANGABLE_INPUT_TAGS[i]).isEmpty();
//			}
//			if (!leftLabeled) {
//				if (node.getNextSibling() == null) {
//					retVal = true; // NOPMD
//				} else if (isVisible(node.getNextSibling()) && Arrays.asList(USER_CHANGABLE_INPUT_TAGS).contains(
//						node.getNextSibling().getNodeName().toLowerCase()))
//					retVal = false;// NOPMD
//				else
//					retVal = retVal
//							&& $(node.getNextSibling()).find(
//									USER_CHANGABLE_INPUT_TAGS[i]).isEmpty();
//			}
//		}
		return retVal;
	}

	private boolean leftRightDirectedSiblingWithLabel(Node node,
			boolean leftLabeled) {
		boolean retVal = false; // NOPMD
		if (leftLabeled) {
			if (node.getPreviousSibling() != null && isVisible(node.getPreviousSibling())) {
				retVal = $(node.getPreviousSibling()).find("label") // NOPMD
						.isNotEmpty()
						|| node.getPreviousSibling().getNodeName()
								.equalsIgnoreCase("label");
			}
		} else {
			if (node.getNextSibling() != null && isVisible(node.getNextSibling())) {
				retVal = $(node.getNextSibling()).find("label").isNotEmpty()
						|| node.getNextSibling().getNodeName()
								.equalsIgnoreCase("label");
			}
		}
		return retVal;
	}

	private List<Element> getLeftRightDirectedSiblingLabelInNode(Node node,
			boolean leftLabeled) {
		List<Element> labels2 = new ArrayList<Element>(); // NOPMD

		if (leftLabeled) {
			if (node.getPreviousSibling().getNodeName()
					.equalsIgnoreCase("label"))
				labels2.add((Element) node.getPreviousSibling());
			else
				labels2 = $(node.getPreviousSibling()).find("label").get();
		} else {
			if (node.getNextSibling().getNodeName().equalsIgnoreCase("label") && node.getNextSibling().getNodeType()==Node.ELEMENT_NODE)
				labels2.add((Element) node.getNextSibling());
			else
				labels2 = $(node.getNextSibling()).find("label").get();
		}
		if (labels2 == null)
			labels2 = new ArrayList<Element>();
		return labels2;
	}

	private UserInputDom initUserInputDomInsideOfForm(Document domDoc,
			Node inputNode, @Nullable Node form) {
		// NodeList allInputNodes, Node inputNodeParentForm) {
		UserInputDom retVal = new UserInputDom(inputNode);

		boolean leftLabeled = isLeftLabeled(inputNode); // NOPMD
		String tempstr = $(inputNode).xpath();
		if (tempstr != null)
			retVal.setXPath(tempstr);
		retVal.setParentFormPointer(form);

		Node maxInputParentNoOtherInput = getMaxInputParentNoOtherInput(
				inputNode, true);

		// Node tempParent = inputNode.getParentNode();
		boolean singleInputFieldForm = false; //NOPMD

 		// if signlefieldform leastInputsCommonParent is the form node
		Node leastInputsCommonParent;
		if (maxInputParentNoOtherInput.getNodeName().equalsIgnoreCase("form")) {
			leastInputsCommonParent = maxInputParentNoOtherInput; //NOPMD
			singleInputFieldForm = true;
		} else
			leastInputsCommonParent = maxInputParentNoOtherInput //NOPMD
					.getParentNode();

		boolean singleFieldFormInputHasNoSibling = false; //NOPMD
		Node maxInputParentNoOtherChild = getMaxInputParentNoOtherChild(inputNode);
 		if (maxInputParentNoOtherChild.getNodeName().equalsIgnoreCase("form")) {
			singleFieldFormInputHasNoSibling = true;
			// new a return result in a single input and no input
			// sibling form
			List<Node> temp1 = new ArrayList<Node>();
			if (maxInputParentNoOtherChild.getChildNodes().item(0)!=null)
				temp1.add(maxInputParentNoOtherChild.getChildNodes().item(0));
			retVal.addAllMachineLearningDomHtmlPointers(temp1);

			retVal.addLabelDomPointer(maxInputParentNoOtherChild
					.getPreviousSibling());

			if (leftLabeled) {
				List<Node> temp = new ArrayList<Node>();
				if (maxInputParentNoOtherChild.getNextSibling()!=null)
					temp.add(maxInputParentNoOtherChild.getNextSibling());
				retVal.setAdditionalInfoNodes(temp);
			}
			retVal.setPreviousUserViewableHtmlSibling(maxInputParentNoOtherChild
					.getPreviousSibling());
			retVal.setNextUserViewableHtmlSibling(maxInputParentNoOtherChild
					.getNextSibling());
	 	}
	 	// TODO the parent of maxInputParentNoOtherChild might have input
		// fields, might not have input fields
		Node leastNonInputSiblingsParent = maxInputParentNoOtherChild //NOPMD
				.getParentNode();
		if (singleInputFieldForm && !singleFieldFormInputHasNoSibling) {
			List<Node> temp = new ArrayList<Node>();
			for (int i = 0; i < leastInputsCommonParent.getChildNodes()
					.getLength(); i++) {
				temp.add(leastInputsCommonParent.getChildNodes().item(i));
			}
			retVal.addAllMachineLearningDomHtmlPointers(temp);

			List<Element> labels = $(leastInputsCommonParent).find("label")
					.get();
			labels.removeIf(node->!this.isVisibleAndUnprocessed(node, domDoc));
			labels.removeIf(node->node.getTextContent()==null || node.getTextContent().trim().isEmpty());

			if (!labels.isEmpty()) { //NOPMD
				if (!isNoNeedLabelInput(inputNode) && !labels.isEmpty())
					retVal.addLabelDomPointer(labels.get(0));
			} else {
				if (!isNoNeedLabelInput(inputNode) && !labels.isEmpty())
					fillOutNonLabeledFieldLabelDomPointer(retVal,
						maxInputParentNoOtherChild, leastInputsCommonParent,
						false, leftLabeled);

			}

			// fill out addtional info nodes
			if (leftLabeled)
				fillOutAddtionalInfoNode(retVal, maxInputParentNoOtherChild,
						leastInputsCommonParent, false, false);

			// fill out non empty user viewable previous sibling
			fillOutUserViewablePreviousSibling(retVal,
					maxInputParentNoOtherChild, leastInputsCommonParent, false);

			// set non empty user viewable next sibling
			fillOutUserViewableNextSibling(retVal, maxInputParentNoOtherChild,
					leastInputsCommonParent, false);

		} else if (!singleFieldFormInputHasNoSibling) {

			List<Element> labels = $(maxInputParentNoOtherInput).find("label")
					.get();
			labels.removeIf(node->!this.isVisibleAndUnprocessed(node, domDoc));
			labels.removeIf(node->node.getTextContent()==null || node.getTextContent().trim().isEmpty());

			if (!labels.isEmpty()) { //NOPMD

				List<Node> tempList = new ArrayList<Node>();
				//tempList.add(maxInputParentNoOtherInput);
				if (!isNoNeedLabelInput(inputNode) && !labels.isEmpty())
					tempList.add(labels.get(0));
				tempList.add(inputNode);
				retVal.addAllMachineLearningDomHtmlPointers(tempList);

				retVal.addLabelDomPointer(labels.get(0));
				if (leftLabeled && leastNonInputSiblingsParent!=null)
					fillOutAddtionalInfoNode(retVal,
							maxInputParentNoOtherChild,
							leastNonInputSiblingsParent, false, false);
				fillOutUserViewablePreviousSibling(retVal,
						maxInputParentNoOtherChild,
						leastNonInputSiblingsParent, false);
				fillOutUserViewableNextSibling(retVal,
						maxInputParentNoOtherChild,
						leastNonInputSiblingsParent, false);
			} else {
				if (this.isUserFormSubmitInputType(inputNode)) {
					List<Node> tempList = new ArrayList<Node>();
					tempList.add(inputNode);
					retVal.addAllMachineLearningDomHtmlPointers(tempList);

				} else if (leftRightDirectedSiblingHasNoInput(
						maxInputParentNoOtherInput, leftLabeled)
						&& leftRightDirectedSiblingWithLabel(
								maxInputParentNoOtherInput, leftLabeled)) {
					List<Element> labels2 = getLeftRightDirectedSiblingLabelInNode(
							maxInputParentNoOtherInput, leftLabeled);
					labels2.removeIf(node->!this.isVisibleAndUnprocessed(node, domDoc));
					labels2.removeIf(node->node.getTextContent()==null || node.getTextContent().trim().isEmpty());
					List<Node> tempList = new ArrayList<Node>();
					if (leftLabeled) {
						if (maxInputParentNoOtherInput
								.getPreviousSibling()!=null)
							tempList.add(maxInputParentNoOtherInput
								.getPreviousSibling());
						tempList.add(maxInputParentNoOtherInput);
					} else {

						tempList.add(maxInputParentNoOtherInput);
						if (maxInputParentNoOtherInput
								.getNextSibling()!=null)
							tempList.add(maxInputParentNoOtherInput
								.getNextSibling());
					}
					retVal.addAllMachineLearningDomHtmlPointers(tempList);
					if (!isNoNeedLabelInput(inputNode) && !labels2.isEmpty())
						retVal.addLabelDomPointer(labels2.get(0));

					if (leftLabeled
							&& leftRightDirectedSiblingHasNoInput(
									maxInputParentNoOtherInput, false))
						fillOutAddtionalInfoNode(retVal,
								maxInputParentNoOtherInput,
								maxInputParentNoOtherInput.getParentNode(),
								false, true);
					fillOutUserViewablePreviousSibling(retVal,
							maxInputParentNoOtherInput,
							maxInputParentNoOtherInput.getParentNode(), false);
					fillOutUserViewableNextSibling(retVal,
							maxInputParentNoOtherInput,
							maxInputParentNoOtherInput.getParentNode(), false);
				} else if (this.siblingHasVisibleInput(maxInputParentNoOtherInput)) {
					// most likely field (label equivelant information has been
					// inside Node
					// maxInputParentNoOtherInput
					List<Node> tempList = new ArrayList<Node>();
					tempList.add(maxInputParentNoOtherInput);
					retVal.addAllMachineLearningDomHtmlPointers(tempList);

					fillOutNonLabeledFieldLabelDomPointer(retVal,
							maxInputParentNoOtherChild,
							maxInputParentNoOtherInput, false, leftLabeled);
					if (leftLabeled)
						fillOutAddtionalInfoNode(retVal,
								maxInputParentNoOtherChild,
								maxInputParentNoOtherInput, false, false);
					fillOutUserViewablePreviousSibling(retVal,
							maxInputParentNoOtherChild,
							maxInputParentNoOtherInput, false);
					fillOutUserViewableNextSibling(retVal,
							maxInputParentNoOtherChild,
							maxInputParentNoOtherInput, false);

				} else if (leftRightDirectedSiblingHasNoInput(
						maxInputParentNoOtherInput, leftLabeled)) {
					// most likely field information is out of
					// maxInputParentNoOtherInput in its siblings with no label
					// tag
					List<Node> tempList = new ArrayList<Node>();
					if (leftLabeled) {
						if (maxInputParentNoOtherInput
								.getPreviousSibling()!=null)
							tempList.add(maxInputParentNoOtherInput
								.getPreviousSibling());
						tempList.add(maxInputParentNoOtherInput);
					} else {
						tempList.add(maxInputParentNoOtherInput);
						if (maxInputParentNoOtherInput
								.getNextSibling()!=null)
							tempList.add(maxInputParentNoOtherInput
								.getNextSibling());
					}
					retVal.addAllMachineLearningDomHtmlPointers(tempList);

					if (leftLabeled) {
						if (maxInputParentNoOtherInput
								.getPreviousSibling()!=null)
							retVal.addLabelDomPointer(maxInputParentNoOtherInput
								.getPreviousSibling());
					}
					else {
						if (maxInputParentNoOtherInput
								.getNextSibling()!=null)
							retVal.addLabelDomPointer(maxInputParentNoOtherInput
								.getNextSibling());
					}

					if (leftLabeled
							&& leftRightDirectedSiblingHasNoInput(
									maxInputParentNoOtherInput, false))
						fillOutAddtionalInfoNode(retVal,
								maxInputParentNoOtherInput,
								maxInputParentNoOtherInput.getParentNode(),
								false, true);

					fillOutUserViewablePreviousSibling(retVal,
							maxInputParentNoOtherInput,
							maxInputParentNoOtherInput.getParentNode(), false);
					fillOutUserViewableNextSibling(retVal,
							maxInputParentNoOtherInput,
							maxInputParentNoOtherInput.getParentNode(), false);

				}
			}

		}

		List<Element> labels = getLabelForElements(domDoc, retVal);
		labels.removeIf(node->!this.isVisibleAndUnprocessed(node, domDoc));
		labels.removeIf(node->node.getTextContent()==null || node.getTextContent().trim().isEmpty());

		//System.out.println("");
		boolean alreadyAddedLabel = false;
		for (int index=0; index<labels.size(); index++) {
			List<Node> mlDomNodes = retVal.getMachineLearningDomHtmlPointers();
			for (Node mlNode : mlDomNodes) {
				if (mlNode.getTextContent().contains(labels.get(index).getTextContent())) {
					alreadyAddedLabel = true;
					break;
				}
			}
			if (!alreadyAddedLabel && !isNoNeedLabelInput(inputNode) )
				retVal.getMachineLearningDomHtmlPointers().add(0, labels.get(index));
			alreadyAddedLabel = false;
		}

		return retVal;
	}

	boolean alreadyAddedLabel(Node label, UserInputDom retVal) {
		boolean alreadyAddedLabel  = false;
		List<Node> mlDomNodes = retVal.getMachineLearningDomHtmlPointers();
		for (Node mlNode : mlDomNodes) {
			if (mlNode.getTextContent().contains(label.getTextContent())) {
				alreadyAddedLabel = true;
				break;
			}
		}
		return alreadyAddedLabel;
	}

	private List<Element> getLabelForElements(Document domDoc, UserInputDom retVal) {
		String inputId = "";
		List<Element> labels = new ArrayList<Element>();
		if ((inputId = ((Element)retVal.getDomNodePointer()).getAttribute("id")).length() == 0)
			if ((inputId = ((Element)retVal.getDomNodePointer()).getAttribute("Id")).length() == 0)
 				if ((inputId = ((Element)retVal.getDomNodePointer()).getAttribute("iD")).length() == 0)
 					if ((inputId = ((Element)retVal.getDomNodePointer()).getAttribute("ID")).length() == 0) {

 					}
		if (inputId.length()>0){
			String jooxQuery = "label[for=\""+ inputId +"\"]";//NOPMD
			labels = $(domDoc).find("label").filter(attr("for", inputId)).get();
		}
		labels.removeIf(node->!this.isVisibleAndUnprocessed(node, domDoc));
		labels.removeIf(node->node.getTextContent()==null || node.getTextContent().trim().isEmpty());
		return labels;

	}

	private UserInputDom initUserInputDomOutOfForm(Document domDoc,
			Node inputNode) {
		// NodeList allInputNodes, Node inputNodeParentForm) {
		UserInputDom retVal = new UserInputDom(inputNode);

		//boolean leftLabeled = isLeftLabeled(inputNode); // NOPMD
		String tempstr = $(inputNode).xpath();
		if (tempstr != null)
			retVal.setXPath(tempstr);
		retVal.setParentFormPointer(null);
		List<Node> tempList = new ArrayList<Node>();
		if (inputNode!=null)
			tempList.add(inputNode);
		//add core inputNode into machine learning html nodes
		retVal.addAllMachineLearningDomHtmlPointers(tempList);

		List<Element> labels = getLabelForElements(domDoc, retVal);
		labels.removeIf(node->!this.isVisibleAndUnprocessed(node, domDoc));
		labels.removeIf(node->node.getTextContent()==null || node.getTextContent().trim().isEmpty());
		if (!isNoNeedLabelInput(inputNode))
				addLabelsIntoTrainingHtmlDoms(labels, retVal);
//		boolean alreadyAddedLabel = false;
//		for (int index=0; index<labels.size(); index++) {
//			//List<Node> mlDomNodes = retVal.getMachineLearningDomHtmlPointers();
//			alreadyAddedLabel = alreadyAddedLabel((Node) labels.get(index), retVal);
//			if (!alreadyAddedLabel)
//				retVal.getMachineLearningDomHtmlPointers().add(0, labels.get(index));
//			alreadyAddedLabel = false;
//		}

		//check if there is only one node in machine learning html doms, then we haven't find label yet.
		//will try to match patterns to see if we can find it.
		if (retVal.getMachineLearningDomHtmlPointers().size()==1 && !isNoNeedLabelInput(inputNode)) {//NOPMD
			List<Node> matchedLabels = matchLabels(domDoc, inputNode);
			addLabelsIntoTrainingHtmlDoms(matchedLabels, retVal);
		}

		return retVal;
	}

	private UserInputDom initLinkUserInputDom(
			Node inputNode) {
		UserInputDom retVal = new UserInputDom(inputNode);

		String tempstr = $(inputNode).xpath();
		if (tempstr != null)
			retVal.setXPath(tempstr);
		retVal.setParentFormPointer(null);
		List<Node> tempList = new ArrayList<Node>();
		if (inputNode!=null)
			tempList.add(inputNode);
		//add core inputNode into machine learning html nodes
		retVal.addAllMachineLearningDomHtmlPointers(tempList);

		return retVal;
	}


	private void addLabelsIntoTrainingHtmlDoms(List<? extends Node> labels, UserInputDom uid) {
		boolean alreadyAddedLabel = false;
		for (int index=0; index<labels.size(); index++) {
			//List<Node> mlDomNodes = retVal.getMachineLearningDomHtmlPointers();
			alreadyAddedLabel = alreadyAddedLabel((Node) labels.get(index), uid);
			if (!alreadyAddedLabel)
				uid.getMachineLearningDomHtmlPointers().add(0, labels.get(index));
			alreadyAddedLabel = false;
		}
	}


	/**
	 * @return the userInputs
	 */
	public final List<UserInputDom> getUserInputs() {
		return userInputs;
	}

	/**
	 * @return the falseInvisibleFilters
	 */
	public List<IUiInvisibilityExceptionalPattern> getFalseInvisibleFilters() {
		return falseInvisibleFilters;
	}

	/**
	 * @param falseInvisibleFilters the falseInvisibleFilters to set
	 */

	public void setFalseInvisibleFilters(List<IUiInvisibilityExceptionalPattern> falseInvisibleFilters) {
		this.falseInvisibleFilters = falseInvisibleFilters;
	}

	/**
	 * @return the labelMatchers
	 */
	public List<IUitrOnScreenLabelMatcher> getLabelMatchers() {
		return labelMatchers;
	}

	/**
	 * @param labelMatchers the labelMatchers to set
	 */
	public void setLabelMatchers(List<IUitrOnScreenLabelMatcher> labelMatchers) {
		this.labelMatchers = labelMatchers;
	}

	/**
	 * @return the userInputConsolidationMatchers
	 */
	public List<IUserInputConsolidationMatcher> getUserInputConsolidationMatchers() {
		return userInputConsolidationMatchers;
	}

	/**
	 * @param userInputConsolidationMatchers the userInputConsolidationMatchers to set
	 */
	public void setUserInputConsolidationMatchers(
			List<IUserInputConsolidationMatcher> userInputConsolidationMatchers) {
		this.userInputConsolidationMatchers = userInputConsolidationMatchers;
	}


}
