/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.tcg.ws;//NOPMD


import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jdt.annotation.Nullable;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.bigtester.RunnerGlobalUtils;
import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms;
import com.bigtester.ate.tcg.model.IntermediateResult;
import com.bigtester.ate.tcg.model.ScreenNamePredictResult;
import com.bigtester.ate.tcg.model.ScreenNamePredictStrategy;
import com.bigtester.ate.tcg.model.domain.HTMLSource;
import com.bigtester.ate.tcg.runner.model.DevToolMessage;
import com.bigtester.ate.tcg.runner.model.ITestStepRunner;
import com.bigtester.ate.tcg.service.UitrParser;
import com.bigtester.ate.tcg.model.domain.ComputedCssSize;
import com.bigtester.ate.tcg.model.domain.InScreenJumperTrainingRecord;
import com.bigtester.ate.tcg.model.domain.Neo4jScreenNode;
import com.bigtester.ate.tcg.model.domain.PredictedFieldName;
import com.bigtester.ate.tcg.model.domain.ScreenElementChangeUITR;
import com.bigtester.ate.tcg.model.domain.ScreenJumperElementTrainingRecord;
import com.bigtester.ate.tcg.model.domain.TestSuite;
import com.bigtester.ate.tcg.model.domain.UserInputTrainingRecord;
import com.bigtester.ate.tcg.model.domain.UserInputValue;
import com.bigtester.ate.tcg.model.domain.WebDomain;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord.UserInputType;
import com.bigtester.ate.tcg.model.domain.WindowsSystemFilePickerScreenNode;
import com.bigtester.ate.tcg.model.ml.ITrainingEntityPioRepo;
import com.bigtester.ate.tcg.model.ml.MLPredictor;
import com.bigtester.ate.tcg.model.ml.TrainingEntityPioRepo;
import com.bigtester.ate.tcg.model.relationship.StepOut;
import com.bigtester.ate.tcg.model.ws.api.ScreenStepsAdvice;
import com.bigtester.ate.tcg.utils.GlobalUtils;
import com.bigtester.ate.tcg.utils.exception.Html2DomException;
import com.bigtester.tebloud.tcg.controller.WebFormUserInputsCollector;
import com.bigtester.tebloud.tcg.service.uitr.ScreenProbabilityCalculator;
import com.bigtester.tebloud.tcg.ws.entity.WsPredictedFieldNames;
import com.bigtester.tebloud.tcg.ws.entity.WsScreenNames;
import com.bigtester.tebloud.tcg.ws.entity.WsUserInputValues;

/**
 * The Class GreetingController.
 */
@RestController
//@RequestMapping(value = "/{domainName}/{testSuiteName}/{testCaseName}")
public class TestCaseUitrController extends BaseWsController {


	@Autowired
	@Nullable
	private WebApplicationContext context;


	/**
	 * Pio predict.
	 *
	 * @param records
	 *            the records
	 * @return the list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 * @throws ExecutionException
	 *             the execution exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 */
	@CrossOrigin
	@RequestMapping(value = "/{domainName}/{testSuiteName}/{testCaseName}/uitrs/pioPredict", method = RequestMethod.POST)
	public Map<UserInputType, List<WebElementTrainingRecord>> pioPredict(@PathVariable String domainName, @PathVariable String testCaseName,
			@RequestBody Map<String, List<WebElementTrainingRecord>> records)
			throws IOException, ClassNotFoundException, ExecutionException,
			InterruptedException {
		Map<UserInputType, List<WebElementTrainingRecord>> retVal = new HashMap<UserInputType, List<WebElementTrainingRecord>>();
		List<WebElementTrainingRecord> tmp = this.getPredictorService(domainName).uitrPredict(domainName, testCaseName, null, records.values().stream().flatMap(x->x.stream()).collect(Collectors.toList()), false, this.getRunningModeProperties().getRunningMode());
		retVal.put(UserInputType.INSCREENJUMPER, tmp.stream().filter(uitr->uitr.getUserInputType().equals(UserInputType.INSCREENJUMPER)).collect(Collectors.toList()));
		retVal.put(UserInputType.SCREENJUMPER, tmp.stream().filter(uitr->uitr.getUserInputType().equals(UserInputType.SCREENJUMPER)).collect(Collectors.toList()));
		retVal.put(UserInputType.SCREENCHANGEUITR, tmp.stream().filter(uitr->uitr.getUserInputType().equals(UserInputType.SCREENCHANGEUITR)).collect(Collectors.toList()));
		retVal.put(UserInputType.USERINPUT, tmp.stream().filter(uitr->uitr.getUserInputType().equals(UserInputType.USERINPUT)).collect(Collectors.toList()));
		retVal.put(UserInputType.unPredicted, tmp.stream().filter(uitr->uitr.getUserInputType().equals(UserInputType.unPredicted)).collect(Collectors.toList()));
		return retVal;
	}










	private void fixJacksonIgnoredLoopReference(
			IntermediateResult intermediateResult) {
		// TODO add prcocessing for screen element change UITRS

		// for (WebElementTrainingRecord uitr :
		// intermediateResult.getScreenChangeUitrs()) {
		// for (Iterator<InTestCase> itr = uitr.getTestcases().iterator();
		// itr.hasNext();) {
		// InTestCase iTc = itr.next();
		// if (iTc.getStartNode() == null && (iTc.getNodeId()!=null &&
		// iTc.getNodeId()!=0)) iTc.setStartNode(uitr);
		// }
		// for (Iterator<StepOut> itr2 = uitr.getStepOuts().iterator();
		// itr2.hasNext();) {
		// StepOut stepOut = itr2.next();
		// if (stepOut.getStartNode() == null && (stepOut.getNodeId()!=null &&
		// stepOut.getNodeId()!=0)) stepOut.setStartNode(uitr);
		// }
		// }

		for (ScreenJumperElementTrainingRecord uitr : intermediateResult.getScreenNode()
				.getActionUitrs()) {
			// for (Iterator<InTestCase> itr = uitr.getTestcases().iterator();
			// itr.hasNext();) {
			// InTestCase iTc = itr.next();
			// if (iTc.getStartNode() == null
			// && (iTc.getNodeId()!=null
			// && iTc.getNodeId()!=0))
			// iTc.setStartNode(uitr);
			// }
			for (Iterator<StepOut> itr2 = uitr.getStepOuts().iterator(); itr2
					.hasNext();) {
				StepOut stepOut = itr2.next();
				if (stepOut.getStartNode() == null
						&& (stepOut.getNodeId() != null && stepOut.getNodeId() != 0))
					stepOut.setStartNode(uitr);
			}
		}
		// for (WebElementTrainingRecord uitr :
		// intermediateResult.getInScreenJumperUitrs()) {
		// for (Iterator<InTestCase> itr = uitr.getTestcases().iterator();
		// itr.hasNext();) {
		// InTestCase iTc = itr.next();
		// if (iTc.getStartNode() == null && (iTc.getNodeId()!=null &&
		// iTc.getNodeId()!=0)) iTc.setStartNode(uitr);
		// }
		// for (Iterator<StepOut> itr2 = uitr.getStepOuts().iterator();
		// itr2.hasNext();) {
		// StepOut stepOut = itr2.next();
		// if (stepOut.getStartNode() == null && (stepOut.getNodeId()!=null &&
		// stepOut.getNodeId()!=0)) stepOut.setStartNode(uitr);
		// }
		// }
		// for (WebElementTrainingRecord uitr :
		// intermediateResult.getUserInputUitrs()) {
		// for (Iterator<InTestCase> itr = uitr.getTestcases().iterator();
		// itr.hasNext();) {
		// InTestCase iTc = itr.next();
		// if (iTc.getStartNode() == null && (iTc.getNodeId()!=null &&
		// iTc.getNodeId()!=0)) iTc.setStartNode(uitr);
		// }
		// for (Iterator<StepOut> itr2 = uitr.getStepOuts().iterator();
		// itr2.hasNext();) {
		// StepOut stepOut = itr2.next();
		// if (stepOut.getStartNode() == null && (stepOut.getNodeId()!=null &&
		// stepOut.getNodeId()!=0)) stepOut.setStartNode(uitr);
		// }
		// }

	}

	private boolean deleteInTestCaseRelationship(String testCaseName, WebElementTrainingRecord uitr) {
		boolean[] retVal = { true };
		uitr.getTestcases()
				.stream()
				.filter(tc -> tc.getEndNode().getName().equals(testCaseName))
				.collect(Collectors.toList())
				.stream()
				.findFirst()
				.ifPresent(
						inTc -> {
							if (!this.getScreenNodeCrud()
									.deleteInTestCaseRelationship(
											inTc.getGraphId()))
								retVal[0] = false;
							uitr.getTestcases().remove(inTc);
						});

		return retVal[0];
	}


	@CrossOrigin
	@Transactional
	@RequestMapping(value = "/{domainName}/{testSuiteName}/{testCaseName}/deleteInTestCaseRelationship", method = RequestMethod.POST)
	public boolean deleteTestCaseRelationship(@PathVariable String testCaseName,
			@RequestBody WebElementTrainingRecord uitr) {


		return this.deleteInTestCaseRelationship(testCaseName, uitr);



	}


	/*private void nomalizeUitr(WebElementTrainingRecord uitr,
			IntermediateResult iResult) {
		uitr.setInputMLHtmlCode(uitr.getInputMLHtmlCode().replaceAll("\r", "")
				.replaceAll("\n", ""));

		uitr.setInputMLHtmlWords(GlobalUtils.removeTagAndAttributeNames(uitr
				.getInputMLHtmlCode()));

		uitr.setElementCoreHtmlCode(uitr.getElementCoreHtmlCode()
				.replaceAll("\r", "").replaceAll("\n", ""));
		uitr.setElementCoreHtmlCodeWithoutGuidValue(GlobalUtils
				.convertToComparableString(uitr.getElementCoreHtmlCode()));

		ComputedCssSize size = GlobalUtils.parseComputedCssSize(uitr
				.getInputMLHtmlCode());

		if (uitr.getElementCoreHtmlCode() != null) {
			ComputedCssSize coreSize = GlobalUtils.parseComputedCssSize(uitr
					.getElementCoreHtmlCode());
			if (coreSize.getUitrSize() > size.getUitrSize())
				size = coreSize;
		}
		size.setTopDocumentWidth(iResult.getTopDocumentWidth());
		size.setTopDocumentHeight(iResult.getTopDocumentHeight());
		HashSet<ComputedCssSize> sizes = new HashSet<ComputedCssSize>();
		sizes.add(size);
		uitr.setComputedCssSizes(sizes);
	}



	// /**
	// * Train input pio.
	// *
	// * @param records
	// * the records
	// * @return the list
	// * @throws ExecutionException
	// * the execution exception
	// * @throws InterruptedException
	// * the interrupted exception
	// * @throws IOException
	// * Signals that an I/O exception has occurred.
	// */
	// @CrossOrigin
	// @RequestMapping(value = "/trainIntoPIO", method = RequestMethod.POST)
	// public Set<? extends WebElementTrainingRecord> trainInputPIO(
	// @RequestBody Set<? extends WebElementTrainingRecord> records)
	// throws ExecutionException, InterruptedException, IOException {
	//
	// for (WebElementTrainingRecord record : records) {
	// if (null != record) {
	// trainInputPIO(record);
	// }
	// }
	// return records;
	// }

	// private WebElementTrainingRecord trainInputPIO(
	// WebElementTrainingRecord record)
	// throws ExecutionException, InterruptedException, IOException {
	// // List<Greeting> greetings = new ArrayList<Greeting>();
	//
	// if (null != record) {
	// String eventId = PredictionIOTrainer.sentTrainingEntity(record);
	// record.setTrainedResult(eventId);
	// }
	//
	// return record;
	// }












	private void markTestCaseBelongingFlagByCompareTwoList(
			List<UserInputTrainingRecord> uitrsInDb,
			List<UserInputTrainingRecord> uitrsInAdvice) {

		if (!uitrsInAdvice.isEmpty()) {

			for (UserInputTrainingRecord adviceRecord : uitrsInAdvice) {
				List<UserInputTrainingRecord> sameLabeledUitrsInDb = uitrsInDb
						.parallelStream()
						.filter(uitrInDb -> uitrInDb
								.getPioPredictLabelResult()
								.getValue()
								.equalsIgnoreCase(
										adviceRecord.getPioPredictLabelResult()
												.getValue())
								&& !uitrInDb.getTestcases().isEmpty())
						.collect(Collectors.toList());
				if (!sameLabeledUitrsInDb.isEmpty()) {

					adviceRecord.setBelongToCurrentTestCase(true);
					break;
				}
			}
		}
	}

	private void markTestCaseBelongingFlagByCompareWithLabelNames(
			Iterable<String> labelNamesInDb,
			List<UserInputTrainingRecord> uitrsInAdvice) {

		if (!uitrsInAdvice.isEmpty()) {

			for (UserInputTrainingRecord adviceRecord : uitrsInAdvice) {
				List<String> sameLabeledNamesInDb = StreamSupport
						.stream(labelNamesInDb.spliterator(), true)
						.filter(labelNameInDb -> labelNameInDb
								.equalsIgnoreCase(adviceRecord
										.getPioPredictLabelResult().getValue()))
						.collect(Collectors.toList());
				if (!sameLabeledNamesInDb.isEmpty()) {

					adviceRecord.setBelongToCurrentTestCase(true);
					break;
				}
			}
		}
	}

	private void moveNonTestCaseBelongedUitrsToArchiveSet(
			Set<? extends WebElementTrainingRecord> from, Set toSet) {
		for (Iterator<? extends WebElementTrainingRecord> itr = from.iterator(); itr
				.hasNext();) {
			WebElementTrainingRecord uitrRecord = itr.next();
			if (!uitrRecord.isBelongToCurrentTestCase()) {
				toSet.add(uitrRecord);
				itr.remove();
			}
		}
	}

	private void moveNonTestCaseBelongedClickablesToArchiveSet(
			List<? extends WebElementTrainingRecord> from, List toList) {
		for (Iterator<? extends WebElementTrainingRecord> itr = from.iterator(); itr
				.hasNext();) {
			WebElementTrainingRecord uitrRecord = itr.next();
			if (!uitrRecord.isBelongToCurrentTestCase()) {
				toList.add(uitrRecord);
				itr.remove();
			}
		}
	}

	// private void unifyClickablesBySameScreenNameNode(Neo4jScreenNode
	// screenNode, ScreenStepsAdvice stepAdvice) {
	// boolean inScreenJumperUnifyResult = false;
	//
	// Set<InScreenJumperTrainingRecord> inScreenUitrInAdvice =
	// stepAdvice.getInScreenJumperUitrs();
	// if (!inScreenUitrInAdvice.isEmpty()) {
	// inScreenJumperUnifyResult =
	// unifyClickablesByCompareTwoList(screenNode.getClickUitrs(),
	// inScreenUitrInAdvice);
	//
	// }
	// boolean screenElementChangerUnifyResult = false;
	// Set<ScreenElementChangeUITR> screenChangeUitrInAdvice =
	// stepAdvice.getScreenElementChangeUitrs();
	// if (!screenChangeUitrInAdvice.isEmpty()) {
	// screenElementChangerUnifyResult =
	// unifyClickablesByCompareTwoList(screenNode.getScreenElementChangeUitrs(),
	// screenChangeUitrInAdvice );
	//
	// }
	//
	// boolean screenJumperUnifyResult = false;
	// Set<ScreenJumperElementTrainingRecord> screenUitrInAdvice =
	// stepAdvice.getScreenJumperUitrs();
	// if (!screenUitrInAdvice.isEmpty()) {
	// screenJumperUnifyResult =
	// unifyClickablesByCompareTwoList(screenNode.getActionUitrs(),
	// screenUitrInAdvice);
	//
	// }
	//
	// if (inScreenJumperUnifyResult || screenElementChangerUnifyResult ||
	// screenJumperUnifyResult) {
	// moveNonTestCaseBelongedUitrsToArchiveSet(stepAdvice.getInScreenJumperUitrs(),
	// (Set) stepAdvice.getArchivedInScreenJumperUitrs());
	//
	//
	// moveNonTestCaseBelongedUitrsToArchiveSet(stepAdvice.getScreenElementChangeUitrs(),
	// (Set) stepAdvice.getArchivedScreenElementChangeUitrs());
	//
	//
	// moveNonTestCaseBelongedUitrsToArchiveSet(stepAdvice.getScreenJumperUitrs(),
	// (Set) stepAdvice.getArchivedScreenJumperUitrs());
	// }
	//
	// }

	// private void unifyClickablesByJumperInSameScreenNameNode(Iterable<String>
	// jumperLabelNames, ScreenStepsAdvice stepAdvice) {
	// boolean inScreenJumperUnifyResult = false;
	//
	// Set<InScreenJumperTrainingRecord> inScreenUitrInAdvice =
	// stepAdvice.getInScreenJumperUitrs();
	// if (!inScreenUitrInAdvice.isEmpty()) {
	// inScreenJumperUnifyResult =
	// unifyClickablesByCompareWithPossibleJumperLabelNames(jumperLabelNames,
	// inScreenUitrInAdvice);
	//
	// }
	// boolean screenElementChangerUnifyResult = false;
	// Set<ScreenElementChangeUITR> screenChangeUitrInAdvice =
	// stepAdvice.getScreenElementChangeUitrs();
	// if (!screenChangeUitrInAdvice.isEmpty()) {
	// screenElementChangerUnifyResult =
	// unifyClickablesByCompareWithPossibleJumperLabelNames(jumperLabelNames,
	// screenChangeUitrInAdvice );
	//
	// }
	//
	// boolean screenJumperUnifyResult = false;
	// Set<ScreenJumperElementTrainingRecord> screenUitrInAdvice =
	// stepAdvice.getScreenJumperUitrs();
	// if (!screenUitrInAdvice.isEmpty()) {
	// screenJumperUnifyResult =
	// unifyClickablesByCompareWithPossibleJumperLabelNames(jumperLabelNames,
	// screenUitrInAdvice);
	//
	// }
	//
	// if (inScreenJumperUnifyResult || screenElementChangerUnifyResult ||
	// screenJumperUnifyResult) {
	// moveNonTestCaseBelongedUitrsToArchiveSet(stepAdvice.getInScreenJumperUitrs(),
	// (Set) stepAdvice.getArchivedInScreenJumperUitrs());
	//
	//
	// moveNonTestCaseBelongedUitrsToArchiveSet(stepAdvice.getScreenElementChangeUitrs(),
	// (Set) stepAdvice.getArchivedScreenElementChangeUitrs());
	//
	//
	// moveNonTestCaseBelongedUitrsToArchiveSet(stepAdvice.getScreenJumperUitrs(),
	// (Set) stepAdvice.getArchivedScreenJumperUitrs());
	// }
	//
	// }

	private boolean markWebElementTrainingRecordBelongingThisTestCaseByFieldNameWithoutConsultScreenNode(
			Set<? extends UserInputTrainingRecord> clickables) {
		// Note: add judgement on confidence. low confidence should considered
		// to be handled specially.
		boolean retVal = false;
		for (UserInputTrainingRecord uitr : clickables) {
			Iterable<? extends UserInputTrainingRecord> sameNameRecords = getUserClickInputTrainingRecordRepo()
					.findByPioPredictLabelResultValue(
							uitr.getPioPredictLabelResult().getValue());
			Set<UserInputTrainingRecord> inTestCaseRecords = StreamSupport
					.stream(sameNameRecords.spliterator(), true)
					.filter(sameNameRecord -> !sameNameRecord.getTestcases()
							.isEmpty()).collect(Collectors.toSet());

			if (inTestCaseRecords.size() > 0) {
				uitr.setBelongToCurrentTestCase(true);
				retVal = true;

			}
		}
		return retVal;
	}

	// private void
	// archiveClickablesNotBelongingThisTestCaseByFieldNameWithoutConsultScreenNode(ScreenStepsAdvice
	// stepAdvice) {
	// if
	// (markWebElementTrainingRecordBelongingThisTestCaseByFieldNameWithoutConsultScreenNode(stepAdvice.getInScreenJumperUitrs()))
	// {
	// moveNonTestCaseBelongedUitrsToArchiveSet(stepAdvice.getInScreenJumperUitrs(),
	// (Set) stepAdvice.getArchivedInScreenJumperUitrs());
	// }
	//
	// if
	// (markWebElementTrainingRecordBelongingThisTestCaseByFieldNameWithoutConsultScreenNode(stepAdvice.getScreenElementChangeUitrs()))
	// {
	// moveNonTestCaseBelongedUitrsToArchiveSet(stepAdvice.getScreenElementChangeUitrs(),
	// (Set) stepAdvice.getArchivedScreenElementChangeUitrs());
	// }
	// if
	// (markWebElementTrainingRecordBelongingThisTestCaseByFieldNameWithoutConsultScreenNode(stepAdvice.getScreenJumperUitrs()))
	// {
	// moveNonTestCaseBelongedUitrsToArchiveSet(stepAdvice.getScreenJumperUitrs(),
	// (Set) stepAdvice.getArchivedScreenJumperUitrs());
	// }
	//
	// }
	private boolean isEndScreen(ScreenStepsAdvice stepAdvice) {
		boolean retVal = false;
		Iterable<String> endScreenNames = this.getScreenNodeCrud()
				.findDistinctEndScreenNamesInTestCase(null);
		long matchEndScreenNamesInTestCase = StreamSupport
				.stream(endScreenNames.spliterator(), true)
				.filter(endScreenName -> endScreenName
						.equalsIgnoreCase(stepAdvice.getScreenName())).count();
		if (matchEndScreenNamesInTestCase > 0)
			retVal = true;
		return retVal;
	}

	Map<Long, Set<UserInputType>> findAlternativeUserInputType(
			List<WebElementTrainingRecord> predictedUitrs) {
		final Map<Long, Set<UserInputType>> retVal = new ConcurrentHashMap<Long, Set<UserInputType>>();
		for (WebElementTrainingRecord uitr : predictedUitrs) {
			Iterable<UserInputTrainingRecord> uitrsInDb = getUserClickInputTrainingRecordRepo()
					.findByPioPredictLabelResultValue(
							uitr.getPioPredictLabelResult().getValue());
			Set<UserInputTrainingRecord> sameLabeledUitrsWithDifferentUserInputType = StreamSupport
					.stream(uitrsInDb.spliterator(), true)
					.filter(uitrInDb -> !uitrInDb.getUserInputType().equals(
							uitr.getUserInputType()))
					.collect(Collectors.toSet());
			if (!sameLabeledUitrsWithDifferentUserInputType.isEmpty()) {
				for (UserInputTrainingRecord sameLabeledUitrWithDifferentUserInputType : sameLabeledUitrsWithDifferentUserInputType) {
					if (uitr.getGraphId() != null && uitr.getGraphId() > 0
							&& retVal.containsKey(uitr.getGraphId())) {
						if (!retVal.get(uitr.getGraphId()).contains(
								sameLabeledUitrWithDifferentUserInputType
										.getUserInputType())) {
							retVal.get(uitr.getGraphId()).add(
									sameLabeledUitrWithDifferentUserInputType
											.getUserInputType());
						}
					} else {
						Set<UserInputType> userInputTypes = new HashSet<UserInputType>();
						userInputTypes
								.add(sameLabeledUitrWithDifferentUserInputType
										.getUserInputType());
						retVal.put(uitr.getGraphId(), userInputTypes);
					}
				}
			}
		}

		return retVal;
	}

	private void markNonClickablesTestCaseBelongingFlagBySameNameScreen(
			Neo4jScreenNode screenNode, ScreenStepsAdvice stepAdvice) {
		List<UserInputTrainingRecord> uitrsInAdvice = stepAdvice
				.getUserInputUitrs();

		if (!uitrsInAdvice.isEmpty()) {
			markTestCaseBelongingFlagByCompareTwoList(
					screenNode.getUserInputUitrs(), uitrsInAdvice);
		}
	}

	private void markNonClickablesTestCaseBelongingFlagByLabelNamesInSameNameScreen(
			Iterable<String> labelnames, ScreenStepsAdvice stepAdvice) {
		List<UserInputTrainingRecord> uitrsInAdvice = stepAdvice
				.getUserInputUitrs();

		if (!uitrsInAdvice.isEmpty()) {
			markTestCaseBelongingFlagByCompareWithLabelNames(labelnames,
					uitrsInAdvice);
		}
	}

	private void archiveNonClickableTestCaseBelongingFlagByFieldNameWithoutConsultScreenNode(
			ScreenStepsAdvice stepAdvice) {
		Set<UserInputTrainingRecord> uitrs = new LinkedHashSet<UserInputTrainingRecord>();
		uitrs.addAll(stepAdvice.getUserInputUitrs());
		if (markWebElementTrainingRecordBelongingThisTestCaseByFieldNameWithoutConsultScreenNode(uitrs)) {
			moveNonTestCaseBelongedClickablesToArchiveSet(
					stepAdvice.getUserInputUitrs(),
					(List) stepAdvice.getArchivedUserInputUitrs());

		}
	}



	private Map<WebElementTrainingRecord, List<WebElementTrainingRecord>> findSimilarStructuredUitrs(
			List<WebElementTrainingRecord> sameLabeledUitrs) {

		Map<WebElementTrainingRecord, List<WebElementTrainingRecord>> retVal = new ConcurrentHashMap<WebElementTrainingRecord, List<WebElementTrainingRecord>>();
		if (sameLabeledUitrs.size() > 1) {
			sameLabeledUitrs
					.stream()
					.forEach(
							uitrE -> {
								if (retVal
										.values()
										.stream()
										.filter(uitrList -> uitrList
												.contains(uitrE))
										.collect(Collectors.toSet()).size() == 0) {
									List<WebElementTrainingRecord> similarUitrs = sameLabeledUitrs
											.stream()
											.parallel()
											.filter(uitr -> GlobalUtils
													.isSimilarHtmlStructure(
															uitr.getElementCoreHtmlNode()
																	.get(),
															uitrE.getElementCoreHtmlNode()
																	.get(),
															uitr.getInputMLHtmlCode(),
															uitrE.getInputMLHtmlCode()))
											.collect(Collectors.toList());

									// if
									// (retVal.keySet().stream().parallel().filter(uitr2->
									// similarUitrs.size()>1 && GlobalUtils
									// .isSimilarHtmlStructure(
									// uitr2.getElementCoreHtmlNode().get(),
									// uitrE.getElementCoreHtmlNode().get(),
									// uitr2.getInputMLHtmlCode(),
									// uitrE.getInputMLHtmlCode())).count()==0 )
									// {
									// //similarUitrs.remove(uitrE);
									// retVal.put(uitrE, similarUitrs);
									// }
									retVal.put(uitrE, similarUitrs);
								}
							});
		}
		return retVal;

	}



	/**
	 * @return the context
	 */
	public WebApplicationContext getContext() {
		final WebApplicationContext context2 = context;
		if (context2 == null) {
			throw new IllegalStateException(
					"Web Application Context Not Initialized!");
		} else {
			return context2;
		}
	}

	/**
	 * @param context
	 *            the context to set
	 */
	public void setContext(WebApplicationContext context) {
		this.context = context;
	}

	public WebFormUserInputsCollector getWebFormUserInputsCollector() {
		WebFormUserInputsCollector retVal = getContext().getBean(
				WebFormUserInputsCollector.class);
		if (retVal == null) {
			throw new IllegalStateException(
					"WebFormUserInputsCollector bean not initialized.");
		} else {
			return retVal;
		}
	}

}
