/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.tcg.ws;//NOPMD

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.eclipse.jdt.annotation.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;

import com.bigtester.ate.tcg.model.ml.FastTextTrainer;
import com.bigtester.ate.tcg.model.ml.IFastTextTrainer;
import com.bigtester.ate.tcg.model.ml.IRemoteTebloudFastText;
import com.bigtester.ate.tcg.model.ml.TrainingEntityPioRepo;
import com.bigtester.tebloud.machine.fasttext.config.TrainingParams;
import com.bigtester.tebloud.tcg.controller.WebFormUserInputsCollector;

/**
 * The Class is controller for fastText machine
 */
@RestController
//@RequestMapping(value = "/fastText")
public class FastTextController extends BaseWsController {





	@Autowired
	@Nullable
	private WebApplicationContext context;






//	@CrossOrigin
//	@RequestMapping(value = "/fastText/trainLocalFastText/{autName:.+}", method = RequestMethod.GET)
//
//	public String train(@PathVariable String autName){
//		//doesn't know if it is done correctly
//		try {
//			return this.createFastTextMachine(autName).trainFastTextModelForAppEngine(this.createTrainerMachine(autName).queryAllUitrTrainingEntitiesForAut());
//		} catch (ExecutionException | InterruptedException | IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return "failed";
//		}
//
//	}
	@CrossOrigin
	@RequestMapping(value = "/fastText/reloadRemoteFastText/{autName:.+}", method = RequestMethod.GET)

	public String reloadRemoteFastTextModel(@PathVariable String autName) {
		//TODO needs to conditionally judge if the remoteTebloudFastText has been loaded or not,
		//If no, the system will load the model twice, as the getPredictorService will load it first time and loadModel will reload it again.
		if (this.getPredictorService(autName).getTebloudPredictorMachine().getAteFastText() instanceof IRemoteTebloudFastText) {
			((IRemoteTebloudFastText) this.getPredictorService(autName).getTebloudPredictorMachine().getAteFastText()).unloadModel();
			((IRemoteTebloudFastText) this.getPredictorService(autName).getTebloudPredictorMachine().getAteFastText()).loadModel(autName, this.getRunningModeProperties(), this.getAppProperties());
			return "Done";
		} else {
			return "remote fasttext machine is not started";
		}
	}

	@CrossOrigin
	@RequestMapping(value = "/fastText/train/{autName:.+}", method = RequestMethod.POST)

	public String trainRemoteFastTextModel(@PathVariable String autName, @RequestBody TrainingParams trainingParams ) throws ExecutionException, InterruptedException, IOException {
		//TODO needs to conditionally judge if the remoteTebloudFastText has been loaded or not,
		//If no, the system will load the model twice, as the getPredictorService will load it first time and loadModel will reload it again.

		if (this.getPredictorService(autName).getTebloudPredictorMachine().getAteFastText() instanceof IRemoteTebloudFastText) {
			if (trainingParams!=null) {
				((IRemoteTebloudFastText) this.getPredictorService(autName).getTebloudPredictorMachine().getAteFastText()).remoteTraining(autName, trainingParams);
			} else {
				((IRemoteTebloudFastText) this.getPredictorService(autName).getTebloudPredictorMachine().getAteFastText()).remoteTraining(autName, this.getAppProperties().getFasttext().getTrainingParams());
			}
			return "Done";
		} else {
			return "remote fasttext machine is not started";
		}
	}

	/**
	 * @return the context
	 */
	public WebApplicationContext getContext() {
		final WebApplicationContext context2 = context;
		if (context2 == null) {
			throw new IllegalStateException(
					"Web Application Context Not Initialized!");
		} else {
			return context2;
		}
	}

	/**
	 * @param context
	 *            the context to set
	 */
	public void setContext(WebApplicationContext context) {
		this.context = context;
	}

	public WebFormUserInputsCollector getWebFormUserInputsCollector() {
		WebFormUserInputsCollector retVal = getContext().getBean(
				WebFormUserInputsCollector.class);
		if (retVal == null) {
			throw new IllegalStateException(
					"WebFormUserInputsCollector bean not initialized.");
		} else {
			return retVal;
		}
	}



}
