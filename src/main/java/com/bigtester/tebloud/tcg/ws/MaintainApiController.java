/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.tcg.ws;



import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.stream.StreamSupport;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



//import com.bigtester.ate.tcg.controller.PredictionIOTrainer;
import com.bigtester.ate.tcg.model.domain.Neo4jScreenNode;
import com.bigtester.ate.tcg.model.ml.ITrainingEntityPioRepo;
import com.bigtester.ate.tcg.model.ml.ITrainingEntityPioRepo.MyPair;
import com.bigtester.ate.tcg.model.ml.TrainingEntityPioRepo;
import com.bigtester.ate.tcg.utils.GlobalUtils;

import io.prediction.AteEvent;



// TODO: Auto-generated Javadoc
/**
 * This class MaintainApiController defines ....
 * @author Peidong Hu
 *
 */
@RestController
public class MaintainApiController extends BaseWsController{

	/**
	 *
	 */
	public MaintainApiController() {
		// TODO Auto-generated constructor stub
	}


	@CrossOrigin
	@RequestMapping("/rebuildEventTable")
	public boolean rebuild(@RequestParam String domainName) {
		ITrainingEntityPioRepo appTrainer = this.createTrainerMachine(domainName);
		StreamSupport.stream(getUserInputTrainingRecordRepo().findAllUitrLabelAndTrainingWords().spliterator(), true).forEach(labelWords->{
			try {
				appTrainer.trainLabelWordsForAppEngine(GlobalUtils.removeAllDigits(GlobalUtils.removeGuidId(labelWords.get("trainingWords"))).replaceAll("[<>\\_'\",!;\\.\\-:\\)\\(]", " ").replaceAll("\\s+", " "), labelWords.get("label"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			}
		});

		StreamSupport.stream(this.getScreenNodeRepo().findAllScreenLabelAndTrainingWords().spliterator(), true).forEach(labelWords->{
			try {
				appTrainer.trainScreenNameForAppEngine(labelWords.get("trainingWords"), labelWords.get("label"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			}
		});
		return true;
	}

	@CrossOrigin
	@RequestMapping(value = "/rebuildZoneLabelEventTable/{domainName:.+}", method = RequestMethod.GET)
	public boolean rebuildZoneLabelEvents(@PathVariable String domainName) throws ExecutionException, InterruptedException, IOException {
		ITrainingEntityPioRepo appTrainer = this.createTrainerMachine(domainName);
		List<AteEvent> labelWords = appTrainer.queryAllEventsForAut();
		labelWords.forEach(lw->{
			try {
				appTrainer.deleteUitrTrainingEvent(lw.getEventId());
			} catch (ExecutionException | InterruptedException | IOException e) {
				_log.error(e.getLocalizedMessage());
			}
		});
		StreamSupport.stream(getUserInputTrainingRecordRepo().findAllUitrsInZone(domainName).spliterator(), true).forEach(uitr->{
			try {
				appTrainer.sentUitrTrainingEntity(uitr, this.getRunningModeProperties().getRunningMode());
				//appTrainer.trainLabelWordsForAppEngine(GlobalUtils.removeAllDigits(GlobalUtils.removeGuidId(labelWords.get("trainingWords"))).replaceAll("[<>\\_'\",!;\\.\\-:\\)\\(]", " ").replaceAll("\\s+", " "), labelWords.get("label"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				_log.error("Failed to train " + uitr.getInputLabelName());

			}
		});


		return true;
	}
	@CrossOrigin
	@RequestMapping(value = "/deleteZoneDecisionTreeEventTable/{domainName:.+}", method = RequestMethod.GET)
	public boolean rebuildZoneDecisionTreeEvents(@PathVariable String domainName) throws ExecutionException, InterruptedException, IOException {
		ITrainingEntityPioRepo appTrainer = this.createTrainerMachine(domainName);
		List<AteEvent> decisionTreeEvents = appTrainer.queryAllDecisionTreeEventsForAut();
		decisionTreeEvents.forEach(lw->{
			try {
				appTrainer.deleteUitrDecisionTrainingEvent(lw.getEventId());
			} catch (ExecutionException | InterruptedException | IOException e) {
				_log.error(e.getLocalizedMessage());
			}
		});
//		StreamSupport.stream(getUserInputTrainingRecordRepo().findAllUitrsInZone(domainName).spliterator(), true).forEach(uitr->{
//			try {
//				appTrainer.sentUitrTrainingEntity(uitr, this.getRunningModeProperties().getRunningMode());
//				//appTrainer.trainLabelWordsForAppEngine(GlobalUtils.removeAllDigits(GlobalUtils.removeGuidId(labelWords.get("trainingWords"))).replaceAll("[<>\\_'\",!;\\.\\-:\\)\\(]", " ").replaceAll("\\s+", " "), labelWords.get("label"));
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				_log.error("Failed to train " + uitr.getInputLabelName());
//
//			}
//		});


		return true;
	}

	@CrossOrigin
	@RequestMapping("/rebuildScreenNameEventTable")
	public boolean rebuildScreenName() {
		ITrainingEntityPioRepo appTrainer = this.createTrainerMachine("");
		StreamSupport.stream(this.getScreenNodeRepo().findAllScreenLabelAndTrainingWords().spliterator(), true).forEach(labelWords->{
			try {
				appTrainer.trainScreenNameForAppEngine(labelWords.get("trainingWords"), labelWords.get("label"));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			}
		});
		return true;
	}

	@CrossOrigin
	@RequestMapping("/screen/{screenNodeId}/deleteTrainedEntities")
	public int deleteTrainedEntities(@PathVariable Long screenNodeId) {

		ITrainingEntityPioRepo appTrainer = this.createTrainerMachine("");
		Optional<Neo4jScreenNode> intermediateResult = Optional.ofNullable(this.getScreenNodeRepo().findOne(screenNodeId));
		Set<String> deletedEvents = new HashSet<String>();
		intermediateResult.ifPresent(sNode->{
			if (!sNode.getScreenTrainedEntityId().isEmpty()) {
				try{
					deletedEvents.add(this.createTrainerMachine("").deleteScreenNameTrainingEntity(sNode.getScreenTrainedEntityId()));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (!sNode.getUitrTrainedEntityIds().isEmpty()) {
				sNode.getUitrTrainedEntityIds().forEach(iId->{
					try {
						deletedEvents.add(appTrainer.deleteUitrTrainingEntity(iId));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				});
			}
			if (!sNode.getDecisionUitrTrainedEntityIds().isEmpty()) {
				sNode.getDecisionUitrTrainedEntityIds().forEach(iId->{
					try {
						deletedEvents.add(appTrainer.deleteUitrDecisionTrainingEntity(iId));
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
			}
		});
		return deletedEvents.size();
	}


}
