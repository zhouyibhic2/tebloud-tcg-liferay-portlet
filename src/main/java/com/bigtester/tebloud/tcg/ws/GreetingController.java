/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.tcg.ws;//NOPMD


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;












import org.apache.commons.lang3.StringUtils;
import org.eclipse.jdt.annotation.Nullable;
import org.neo4j.ogm.model.Result;
import org.neo4j.ogm.session.Session;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.WebApplicationContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;












import com.bigtester.RunnerGlobalUtils;
import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms;
import com.bigtester.ate.tcg.model.IntermediateResult;
import com.bigtester.ate.tcg.model.ScreenNamePredictResult;
import com.bigtester.ate.tcg.model.ScreenNamePredictStrategy;
import com.bigtester.ate.tcg.model.domain.HTMLSource;
import com.bigtester.ate.tcg.runner.model.DevToolMessage;
import com.bigtester.ate.tcg.runner.model.ITestStepRunner;
import com.bigtester.ate.tcg.service.ITebloudPredictor;
import com.bigtester.ate.tcg.service.UitrParser;
import com.bigtester.ate.tcg.service.parallel.TrainerQueueManager;
import com.bigtester.ate.tcg.model.domain.ComputedCssSize;
import com.bigtester.ate.tcg.model.domain.InScreenJumperTrainingRecord;
import com.bigtester.ate.tcg.model.domain.Neo4jScreenNode;
import com.bigtester.ate.tcg.model.domain.PredictedFieldName;
import com.bigtester.ate.tcg.model.domain.ScreenElementChangeUITR;
import com.bigtester.ate.tcg.model.domain.ScreenJumperElementTrainingRecord;
import com.bigtester.ate.tcg.model.domain.TestSuite;
import com.bigtester.ate.tcg.model.domain.UserInputTrainingRecord;
import com.bigtester.ate.tcg.model.domain.UserInputValue;
import com.bigtester.ate.tcg.model.domain.WebDomain;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord.UserInputType;
import com.bigtester.ate.tcg.model.domain.WindowsSystemFilePickerScreenNode;
import com.bigtester.ate.tcg.model.ml.ITrainingEntityPioRepo;
import com.bigtester.ate.tcg.model.ml.TebloudMachine;
import com.bigtester.ate.tcg.model.ml.TrainingEntityPioRepo;
import com.bigtester.ate.tcg.model.relationship.StepOut;
import com.bigtester.ate.tcg.model.ws.api.ScreenStepsAdvice;
import com.bigtester.ate.tcg.utils.GlobalUtils;
import com.bigtester.ate.tcg.utils.exception.Html2DomException;
import com.bigtester.tebloud.tcg.config.RunningModeProperties;
import com.bigtester.tebloud.tcg.controller.WebFormUserInputsCollector;
import com.bigtester.tebloud.tcg.cuba.portal.security.TebloudPortalUserSessionSource;
import com.bigtester.tebloud.tcg.model.domain.TebloudUser;
import com.bigtester.tebloud.tcg.model.domain.TebloudUserZone;
import com.bigtester.tebloud.tcg.service.ml.TebloudPredictor;
import com.bigtester.tebloud.tcg.service.ml.TebloudPioRepo;
import com.bigtester.tebloud.tcg.service.repository.TebloudUserZoneRepo;
import com.bigtester.tebloud.tcg.service.uitr.ScreenProbabilityCalculator;
import com.bigtester.tebloud.tcg.ws.entity.WsPredictedFieldNames;
import com.bigtester.tebloud.tcg.ws.entity.WsScreenNames;
import com.bigtester.tebloud.tcg.ws.entity.WsTestCaseNames;
import com.bigtester.tebloud.tcg.ws.entity.WsUserInputValues;
import com.bigtester.tebloud.tcg.ws.entity.WsTestCaseNames.TestCasename;
import com.bigtester.tebloud.tcg.ws.entity.WsTestSuiteNames;





/**
 * The Class GreetingController.
 */
@RestController
//@RequestMapping(value = "/{domainName}/{testSuiteName}/{testCaseName}")
//@Service ("bigtester_TestScreenService")
public class GreetingController extends BaseWsController {


	@Autowired
	private TrainerQueueManager trainerQueque;


	/**
	 * Gets the system training mode properties.
	 *
	 * @return the system training mode properties
	 */
	@CrossOrigin
	@RequestMapping("/system/properties/runningMode")
	public RunningModeProperties getSystemRunningModeProperties() {
		return this.getRunningModeProperties();
	}

	/**
	 * Predict.
	 *
	 * @return the list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 */
	@CrossOrigin
	@RequestMapping("/{domainName}/{testSuiteName}/{testCaseName}/predict")
	@Deprecated
	public List<WebElementTrainingRecord> predict() throws IOException,
			ClassNotFoundException {

		// traverseFrameTraining(firefox, null);

		// System.out.println("\n=======****FILE PARSING IS DONE for: "
		// + TEST_HTML_FILES[j] + "****=========\n");

		com.bigtester.tebloud.tcg.controller.UserInputsTrainer trainer = new com.bigtester.tebloud.tcg.controller.UserInputsTrainer();
		List<WebElementTrainingRecord> trainedRecords = trainer.train();

		com.bigtester.tebloud.tcg.controller.TrainingFileDB.writeCacheCsvFile(
				com.bigtester.tebloud.tcg.controller.UserInputsTrainer.CACHEPATH
						+ "firsttest" + ".txt", "begin", "end", trainedRecords,
				true);
		return trainedRecords;
	}





	private List<HTMLSource> removeIllegalXmlChars(List<HTMLSource> sources) {
		if (sources != null) {
			String xml10pattern = "[^" + "\u0009\r\n" + "\u0020-\uD7FF"
					+ "\uE000-\uFFFD" + "\ud800\udc00-\udbff\udfff" + "]";
			String xml11pattern = "[^" + "\u0001-\uD7FF" + "\uE000-\uFFFD"
					+ "\ud800\udc00-\udbff\udfff" + "]+";
			sources.forEach(source -> {
				source.setDomDoc(source.getDomDoc()
						.replaceAll(xml10pattern, "")
						.replaceAll(xml11pattern, ""));
			});
		}
		return sources;
	}

	/**
	 * Page predict.
	 *
	 * @param pageFrames
	 *            the page frames
	 * @return the map
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 * @throws ExecutionException
	 *             the execution exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 */
	@CrossOrigin
	@RequestMapping(value = "/{domainName}/{testSuiteName}/{testCaseName}/pagePredict", method = RequestMethod.POST)
	public Map<ScreenNamePredictStrategy, Map<String, ScreenNamePredictResult>> pagePredict(@PathVariable String testSuiteName, @PathVariable String domainName,@PathVariable String testCaseName,
			@RequestBody Map<String, List<HTMLSource>> doms)
			throws IOException, ClassNotFoundException, ExecutionException,
			InterruptedException {
		return this.getPredictorService(domainName).pagePredict(testCaseName, domainName,
				doms, false);
		/*Map<ScreenNamePredictStrategy, Map<String, ScreenNamePredictResult>> retVal = new HashMap<ScreenNamePredictStrategy, Map<String, ScreenNamePredictResult>>();// NOPMD
		UitrParser uitrParser = new UitrParser(doms, this.getPioPredictor());

		//List<HTMLSource> previousDoms = uitrParser.getPreviousHtmlSource().orElse(null);//doms.get("previousDoms");
		//List<HTMLSource> newDoms = uitrParser.getNewHtmlSource();// doms.get("newDoms");
		List<Document> newDoms = uitrParser.getNewDomDocuments();// doms.get("newDoms");
		if (uitrParser.getNewHtmlSource().isEmpty()) {
			HashMap<String, ScreenNamePredictResult> tmpMap = new HashMap<String, ScreenNamePredictResult>();
			tmpMap.put("", new ScreenNamePredictResult());
			retVal.put(
					ScreenNamePredictStrategy.PREDICTED_BY_VISIBLE_AND_UNPROCESSED_ELEMENT_TEXT,
					tmpMap);
		} else {
			// removeIllegalXmlChars(previousDoms);
			// removeIllegalXmlChars(newDoms);
			//uitrParser has done this in the construction.
			//markPreviousScreenDomProcessedElements(previousDoms, newDoms);
			ScreenNamePredictStrategy[] possibleValues = ScreenNamePredictStrategy
					.values();
			List<String> screenWords = new ArrayList<String>();
			List<String> screenNames = new ArrayList<String>();
			for (int index = 0; index < possibleValues.length; index++) {
				Map<String, ScreenNamePredictResult> tmpVal = PredictionIOTrainer
						.predictScreenName(newDoms,
								possibleValues[index]);
				if (!screenWords.contains(tmpVal.values().iterator().next()
						.getScreenTrainingWords())
						&& !screenNames.contains(tmpVal.keySet().iterator()
								.next())) {
					retVal.put(possibleValues[index], tmpVal);
					screenWords.add(tmpVal.values().iterator().next()
							.getScreenTrainingWords());
					screenNames.add(tmpVal.keySet().iterator().next());
				}
			}

		}
		retVal = retVal
				.entrySet()
				.stream()
				.parallel()
				.filter(RunnerGlobalUtils.distinctByKey(mapEntry -> mapEntry
						.getValue().keySet().iterator().next()))
				.collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
		return retVal;
		*/
	}

	/**
	 * Gets the screen names.
	 *
	 * @param domainIndustryCode
	 *            the domain industry code
	 * @return the screen names
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 * @throws ExecutionException
	 *             the execution exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 */
	@CrossOrigin
	@RequestMapping(value = "/{domainName}/{testSuiteName}/{testCaseName}/queryScreenNames", method = RequestMethod.GET)
	public WsScreenNames queryScreenNames(
			@RequestParam(value = "q", required = false) String queryStr,
			@RequestParam(required = false, value = "domainIndustryCode") String domainIndustryCode)
			throws IOException, ClassNotFoundException, ExecutionException,
			InterruptedException {

		Iterable<String> allNames = getScreenNodeRepo()
				.getDistinctScreenNamesInTestCase();
		WsScreenNames retVal = new WsScreenNames(0, false);
		for (String nodeName : allNames) {
			retVal.getScreenNames().add(retVal.new ScreenName(nodeName, ""));
		}
		return retVal;
	}
	@CrossOrigin
	@RequestMapping(value = "/{domainName}/{testSuiteName}/{testCaseName}/queryTestSuiteNames", method = RequestMethod.GET)
	public WsTestSuiteNames queryTestSuiteNames(
			@RequestParam(required = false, value = "domainIndustryCode") String domainIndustryCode)
			throws IOException, ClassNotFoundException, ExecutionException,
			InterruptedException {

		Iterable<String> allNames = getScreenNodeRepo()
				.getDistinctTestSuiteNames();
		WsTestSuiteNames retVal = new WsTestSuiteNames(0, false);
		for (String nodeName : allNames) {
			retVal.getSuiteNames().add(retVal.new SuiteName(nodeName, ""));
		}
		return retVal;
	}



	@CrossOrigin
	@RequestMapping(value = "/{domainName}/{testSuiteName}/{testCaseName}/queryTestCaseNames", method = RequestMethod.GET)
	public WsTestCaseNames queryTestCaseNames(
			@RequestParam(value = "q", required = false) String queryStr,
			@RequestParam(required = false, value = "domainIndustryCode") String domainIndustryCode)
			throws IOException, ClassNotFoundException, ExecutionException,
			InterruptedException {
		Iterable<String> allNames = getTestCaseRepo().getDistinctTestCaseNames();
		WsTestCaseNames retVal = new WsTestCaseNames(0, false);
		for (String nodeName : allNames) {
			retVal.getTestCaseNames().add(retVal.new TestCasename(nodeName, ""));
		}
		return retVal;
	}
	/**
	 * Query user input values.
	 *
	 * @param queryStr
	 *            the query str
	 * @param domainIndustryCode
	 *            the domain industry code
	 * @return the ws user input values
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 * @throws ExecutionException
	 *             the execution exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 */
	@CrossOrigin
	@RequestMapping(value = "/{domainName}/{testSuiteName}/{testCaseName}/queryUserInputValues", method = RequestMethod.GET)
	public WsUserInputValues queryUserInputValues(
			@RequestParam(value = "q", required = false) String queryStr,
			@RequestParam(required = false, value = "domainIndustryCode") String domainIndustryCode)
			throws IOException, ClassNotFoundException, ExecutionException,
			InterruptedException {

		Iterable<String> allNodes = getUserInputValueRepo()
				.getDistinctUserInputValuesInTestCase();
		WsUserInputValues retVal = new WsUserInputValues(0, false);
		for (String nodeValue : allNodes) {
			retVal.getUserValues().add(new UserInputValue(nodeValue, ""));
		}
		return retVal;
	}

	/**
	 * Query pio predicted field names.
	 *
	 * @param queryStr
	 *            the query str
	 * @param domainIndustryCode
	 *            the domain industry code
	 * @return the ws predicted field names
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 * @throws ExecutionException
	 *             the execution exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 */
	@CrossOrigin
	@RequestMapping(value = "/{domainName}/{testSuiteName}/{testCaseName}/queryPioPredictedFieldNames", method = RequestMethod.GET)
	public WsPredictedFieldNames queryPioPredictedFieldNames(
			@RequestParam(value = "q", required = false) String queryStr,
			@RequestParam(required = false, value = "domainIndustryCode") String domainIndustryCode)
			throws IOException, ClassNotFoundException, ExecutionException,
			InterruptedException {

		Iterable<Map<String, String>> allNodes = getPredictedFieldNameRepo()
				.getDistinctPredictedFieldNamesAndUserInputValuePairInTestCase();
		WsPredictedFieldNames retVal = new WsPredictedFieldNames(0, false);

		Map<String, List<String>> fieldValues = new ConcurrentHashMap<String, List<String>>();
		Iterator<Map<String, String>>  itr = allNodes.iterator();
		while (itr.hasNext()) {
			Map<String, String> next = itr.next();
			String key  = next.get("name");
			String value = next.get("value");

			if (fieldValues.get(key) == null) {
				List<String> valueArr = new ArrayList<String>();
				if (!StringUtils.isEmpty(value))
					valueArr.add(value);
				fieldValues.put(key, valueArr);
			} else if(!StringUtils.isEmpty(value) && !fieldValues.get(key).contains(value)) {
				fieldValues.get(key).add(value);
			}

		}
		fieldValues.entrySet().forEach(entry->{

			retVal.getFieldNames().add(new PredictedFieldName(entry.getKey(), entry.getValue().size()>0?entry.getValue().get(0):""));
		});
		return retVal;
	}

	private void fixJacksonIgnoredLoopReference(
			IntermediateResult intermediateResult) {
		// TODO add prcocessing for screen element change UITRS

		// for (WebElementTrainingRecord uitr :
		// intermediateResult.getScreenChangeUitrs()) {
		// for (Iterator<InTestCase> itr = uitr.getTestcases().iterator();
		// itr.hasNext();) {
		// InTestCase iTc = itr.next();
		// if (iTc.getStartNode() == null && (iTc.getNodeId()!=null &&
		// iTc.getNodeId()!=0)) iTc.setStartNode(uitr);
		// }
		// for (Iterator<StepOut> itr2 = uitr.getStepOuts().iterator();
		// itr2.hasNext();) {
		// StepOut stepOut = itr2.next();
		// if (stepOut.getStartNode() == null && (stepOut.getNodeId()!=null &&
		// stepOut.getNodeId()!=0)) stepOut.setStartNode(uitr);
		// }
		// }

		for (ScreenJumperElementTrainingRecord uitr : intermediateResult.getScreenNode()
				.getActionUitrs()) {
			// for (Iterator<InTestCase> itr = uitr.getTestcases().iterator();
			// itr.hasNext();) {
			// InTestCase iTc = itr.next();
			// if (iTc.getStartNode() == null
			// && (iTc.getNodeId()!=null
			// && iTc.getNodeId()!=0))
			// iTc.setStartNode(uitr);
			// }
			for (Iterator<StepOut> itr2 = uitr.getStepOuts().iterator(); itr2
					.hasNext();) {
				StepOut stepOut = itr2.next();
				if (stepOut.getStartNode() == null
						&& (stepOut.getNodeId() != null && stepOut.getNodeId() != 0))
					stepOut.setStartNode(uitr);
			}
		}
		// for (WebElementTrainingRecord uitr :
		// intermediateResult.getInScreenJumperUitrs()) {
		// for (Iterator<InTestCase> itr = uitr.getTestcases().iterator();
		// itr.hasNext();) {
		// InTestCase iTc = itr.next();
		// if (iTc.getStartNode() == null && (iTc.getNodeId()!=null &&
		// iTc.getNodeId()!=0)) iTc.setStartNode(uitr);
		// }
		// for (Iterator<StepOut> itr2 = uitr.getStepOuts().iterator();
		// itr2.hasNext();) {
		// StepOut stepOut = itr2.next();
		// if (stepOut.getStartNode() == null && (stepOut.getNodeId()!=null &&
		// stepOut.getNodeId()!=0)) stepOut.setStartNode(uitr);
		// }
		// }
		// for (WebElementTrainingRecord uitr :
		// intermediateResult.getUserInputUitrs()) {
		// for (Iterator<InTestCase> itr = uitr.getTestcases().iterator();
		// itr.hasNext();) {
		// InTestCase iTc = itr.next();
		// if (iTc.getStartNode() == null && (iTc.getNodeId()!=null &&
		// iTc.getNodeId()!=0)) iTc.setStartNode(uitr);
		// }
		// for (Iterator<StepOut> itr2 = uitr.getStepOuts().iterator();
		// itr2.hasNext();) {
		// StepOut stepOut = itr2.next();
		// if (stepOut.getStartNode() == null && (stepOut.getNodeId()!=null &&
		// stepOut.getNodeId()!=0)) stepOut.setStartNode(uitr);
		// }
		// }

	}


	@CrossOrigin
	@Transactional
	@RequestMapping(value = "/{domainName}/{testSuiteName}/{testCaseName}/rel/{startNodeGraphId}/{relationship}/{endNodeGraphId}", method = RequestMethod.DELETE)
	public int deleteRelationship(@PathVariable Long startNodeGraphId,@PathVariable String relationship, @PathVariable Long endNodeGraphId) {
		return this.deleteRelationship(startNodeGraphId, relationship, endNodeGraphId, getNeo4jSession());

	}

	private int deleteRelationship(Long startNodeId, String relationship, Long endNodeId, Session neo4jSession) {

		Map<String, String> params = new ConcurrentHashMap<String, String>();

		Result queryResult = neo4jSession.query("match (n)-[r:"+relationship +"]-(m) where id(n)="+startNodeId+" and id(m)=" +endNodeId+ " delete r", params);

		neo4jSession.clear();
		return queryResult.queryStatistics().getRelationshipsDeleted();
	}

	private boolean deleteInTestCaseRelationship(WebElementTrainingRecord uitr) {
		boolean retVal = true;
		if (!uitr.isBelongToCurrentTestCase() && uitr.getGraphId() != null
				&& uitr.getGraphId() != 0 && !uitr.getTestcases().isEmpty()) {
			if (!this.getScreenNodeCrud().deleteInTestCaseRelationship(
					uitr.getTestcases().iterator().next().getGraphId()))
				retVal = false;
			//uitr = getNeo4jSession().load(WebElementTrainingRecord.class,uitr.getGraphId());
			// TODO need to re-write to support multiple testcases
			uitr.getTestcases().removeAll(uitr.getTestcases());
		}
		return retVal;
	}

	private int updateNodeLabel(Long nodeId, String labelToDelete, String newLabel, Session neo4jSession) {

		Map<String, String> params = new ConcurrentHashMap<String, String>();
//		params.put("graphId", nodeId.toString());
//		params.put("label", label);
//
		Result queryResult = neo4jSession.query("match (n) where id(n)="+nodeId+" remove n:" + labelToDelete, params);
		Result queryResult2 = neo4jSession.query("match (n) where id(n)="+nodeId+" set n:" + newLabel, params);
		neo4jSession.clear();
		return queryResult.queryStatistics().getLabelsRemoved() + queryResult2.queryStatistics().getLabelsAdded();
	}

	private int deleteNodeInDb(Long nodeId, String relToDelete, Long relatedGraphId, Session neo4jSession) {

		Map<String, String> params = new ConcurrentHashMap<String, String>();


		Result queryResult = neo4jSession.query("match (n)-[r]-(m) where id(n)="+nodeId+" and id(m)="+relatedGraphId+" delete n, r", params);
		if (queryResult.queryStatistics().getNodesDeleted()==1 && queryResult.queryStatistics().getRelationshipsDeleted()==1) {
			return 2;
		} else if (queryResult.queryStatistics().getNodesDeleted()==0 && queryResult.queryStatistics().getRelationshipsDeleted()==0){
			//failed, try delete the relationship only,
			Result queryResult2 = neo4jSession.query("match (n)-[r]-(m) where id(n)="+nodeId+" and id(m)="+relatedGraphId+" delete r", params);
			if (queryResult2.queryStatistics().getNodesDeleted()==0 && queryResult2.queryStatistics().getRelationshipsDeleted()==1) {
				return 1;
			} else {
				return 0;
			}
		}
		return 0;
	}

	@CrossOrigin
	@Transactional
	@RequestMapping(value = "/node/{graphId}/label/{labelName}", method = RequestMethod.PUT)
	public int updateNodeLabel(@PathVariable String labelName,@PathVariable Long graphId, @RequestBody Map<String, String> newLabel) {
		return this.updateNodeLabel(graphId, labelName, newLabel.get("newLabel"), getNeo4jSession());

	}

	@CrossOrigin
	@Transactional
	@RequestMapping(value = "/node/{graphId}/{rel}/", method = RequestMethod.DELETE)
	public int deleteNode(@PathVariable Long graphId, @PathVariable String rel, @RequestBody Map<String, Long> relatedGraphId) {
		return this.deleteNodeInDb(graphId, rel, relatedGraphId.get("relatedNodeGraphId"), getNeo4jSession());

	}


	/*private void nomalizeUitr(WebElementTrainingRecord uitr,
			IntermediateResult iResult) {
		uitr.setInputMLHtmlCode(uitr.getInputMLHtmlCode().replaceAll("\r", "")
				.replaceAll("\n", ""));

		uitr.setInputMLHtmlWords(GlobalUtils.removeTagAndAttributeNames(uitr
				.getInputMLHtmlCode()));

		uitr.setElementCoreHtmlCode(uitr.getElementCoreHtmlCode()
				.replaceAll("\r", "").replaceAll("\n", ""));
		uitr.setElementCoreHtmlCodeWithoutGuidValue(GlobalUtils
				.convertToComparableString(uitr.getElementCoreHtmlCode()));

		ComputedCssSize size = GlobalUtils.parseComputedCssSize(uitr
				.getInputMLHtmlCode());

		if (uitr.getElementCoreHtmlCode() != null) {
			ComputedCssSize coreSize = GlobalUtils.parseComputedCssSize(uitr
					.getElementCoreHtmlCode());
			if (coreSize.getUitrSize() > size.getUitrSize())
				size = coreSize;
		}
		size.setTopDocumentWidth(iResult.getTopDocumentWidth());
		size.setTopDocumentHeight(iResult.getTopDocumentHeight());
		HashSet<ComputedCssSize> sizes = new HashSet<ComputedCssSize>();
		sizes.add(size);
		uitr.setComputedCssSizes(sizes);
	}

	private void nomalizeUitr(WebElementTrainingRecord uitr) {
		uitr.setInputMLHtmlCode(uitr.getInputMLHtmlCode().replaceAll("\r", "")
				.replaceAll("\n", ""));

		uitr.setInputMLHtmlWords(GlobalUtils.removeTagAndAttributeNames(uitr
				.getInputMLHtmlCode()));

		uitr.setElementCoreHtmlCode(uitr.getElementCoreHtmlCode()
				.replaceAll("\r", "").replaceAll("\n", ""));
		uitr.setElementCoreHtmlCodeWithoutGuidValue(GlobalUtils
				.convertToComparableString(uitr.getElementCoreHtmlCode()));

		ComputedCssSize size = GlobalUtils.parseComputedCssSize(uitr
				.getInputMLHtmlCode());

		if (uitr.getElementCoreHtmlCode() != null) {
			ComputedCssSize coreSize = GlobalUtils.parseComputedCssSize(uitr
					.getElementCoreHtmlCode());
			if (coreSize.getUitrSize() > size.getUitrSize())
				size = coreSize;
		}

		HashSet<ComputedCssSize> sizes = new HashSet<ComputedCssSize>();
		sizes.add(size);
		uitr.setComputedCssSizes(sizes);
	}*/

	public Map<String, List<UserInputTrainingRecord>> sortMapByCoordY(Map<String, List<UserInputTrainingRecord>> unsortedMap) {
		Map<String, List<UserInputTrainingRecord>> SortedSameLabeledUitrGroups = new LinkedHashMap<String, List<UserInputTrainingRecord>>();
		Comparator<Map.Entry<String, List<UserInputTrainingRecord>>> cmpMapFirstEntryValueY = Comparator.comparingDouble(entry->entry.getValue().get(0).getComputedCssSizes().iterator().next().getLeftTopCornerY());
		List<Map.Entry<String, List<UserInputTrainingRecord>>> sortingList = new ArrayList<Map.Entry<String, List<UserInputTrainingRecord>>>(unsortedMap.entrySet());
		sortingList = sortingList.stream().sorted(cmpMapFirstEntryValueY).collect(Collectors.toList());
		for (Iterator<Map.Entry<String, List<UserInputTrainingRecord>>> it = sortingList.iterator(); it.hasNext();) {
			Map.Entry<String, List<UserInputTrainingRecord>> uitrEntry =  it.next();
			SortedSameLabeledUitrGroups.put(uitrEntry.getKey(), uitrEntry.getValue());
		}
		return SortedSameLabeledUitrGroups;
	}
	public Map<Double, List<List<UserInputTrainingRecord>>> groupingFields(IntermediateResult iResult) {
		//assumption, user fields are orged from top to bottom and from left to right.
		List<UserInputTrainingRecord> uitrs = iResult.getScreenNode().getUserInputUitrs();
		Comparator<UserInputTrainingRecord> cmpY = Comparator.comparingDouble(uitr->uitr.getComputedCssSizes().iterator().next().getLeftTopCornerY());
		Comparator<UserInputTrainingRecord> cmpX = Comparator.comparingDouble(uitr->uitr.getComputedCssSizes().iterator().next().getLeftTopCornerX());
		Map<String, List<UserInputTrainingRecord>> sameLabeledUitrGroups = uitrs.stream().collect(Collectors.groupingBy(uitr->uitr.getInputLabelName())).entrySet().stream().filter(entry->entry.getValue().size()>1).collect(Collectors.toMap(Entry::getKey, Entry::getValue));

		sameLabeledUitrGroups.entrySet().forEach(entry->{
			if (entry.getValue().size()>1)
				entry.setValue(entry.getValue().stream().sorted(cmpY).collect(Collectors.toList()));
		});


//		Map<String, List<UserInputTrainingRecord>> SortedSameLabeledUitrGroups = new LinkedHashMap<String, List<UserInputTrainingRecord>>();
//		Comparator<Map.Entry<String, List<UserInputTrainingRecord>>> cmpMapFirstEntryValueY = Comparator.comparingDouble(entry->entry.getValue().get(0).getComputedCssSizes().iterator().next().getLeftTopCornerY());
//		List<Map.Entry<String, List<UserInputTrainingRecord>>> sortingList = new ArrayList<Map.Entry<String, List<UserInputTrainingRecord>>>(sameLabeledUitrGroups.entrySet());
//		sortingList = sortingList.stream().sorted(cmpMapFirstEntryValueY).collect(Collectors.toList());
//		for (Iterator<Map.Entry<String, List<UserInputTrainingRecord>>> it = sortingList.iterator(); it.hasNext();) {
//			Map.Entry<String, List<UserInputTrainingRecord>> uitrEntry =  it.next();
//			SortedSameLabeledUitrGroups.put(uitrEntry.getKey(), uitrEntry.getValue());
//		}

		Map<String, List<UserInputTrainingRecord>> SortedSameLabeledUitrGroups = sortMapByCoordY(sameLabeledUitrGroups);
		Double[] firstFieldGroupYRoomCoords = {0d, 0d};
		Map<Double, Map<String, List<UserInputTrainingRecord>>> groupsOfFieldGroup = new LinkedHashMap<Double, Map<String, List<UserInputTrainingRecord>>>();
		for (Map.Entry<String, List<UserInputTrainingRecord>> entry: SortedSameLabeledUitrGroups.entrySet()) {
			if (firstFieldGroupYRoomCoords[0] == 0d && !groupsOfFieldGroup.values().stream().map(map->map.entrySet()).flatMap(x->x.stream()).collect(Collectors.toSet()).contains(entry)) {
				Double firstGroupHeadUitrY = entry.getValue().get(0).getComputedCssSizes().iterator().next().getLeftTopCornerY();
				Double secondGroupHeadUitrY = entry.getValue().get(1).getComputedCssSizes().iterator().next().getLeftTopCornerY();
				List<String> firstGroupLabels = new ArrayList<String>();
				SortedSameLabeledUitrGroups.entrySet().forEach(entryS->{
					if (entryS.getValue().get(0).getComputedCssSizes().iterator().next().getLeftTopCornerY()>=firstGroupHeadUitrY && entryS.getValue().get(0).getComputedCssSizes().iterator().next().getLeftTopCornerY() <secondGroupHeadUitrY) {
						firstGroupLabels.add(entryS.getKey());
					}
				});
				//this will cause the sorting of groups lost
				groupsOfFieldGroup.put(firstGroupHeadUitrY, SortedSameLabeledUitrGroups.entrySet().stream().filter(entrySe->firstGroupLabels.contains(entrySe.getKey())).collect(Collectors.toMap(Entry::getKey, Entry::getValue)));
				firstFieldGroupYRoomCoords[0]= 0d;firstFieldGroupYRoomCoords[1]= 0d;
			}

		}
		groupsOfFieldGroup.entrySet().forEach(superGroup->{
			superGroup.setValue(this.sortMapByCoordY(superGroup.getValue()));
		});

		Map<Double, List<List<UserInputTrainingRecord>>> groupsOfAggregatedFieldGroup = new LinkedHashMap<Double, List<List<UserInputTrainingRecord>>>();
		groupsOfFieldGroup.entrySet().forEach(superGroupEntry->{
			if (groupsOfAggregatedFieldGroup.get(superGroupEntry.getKey())==null)
				groupsOfAggregatedFieldGroup.put(superGroupEntry.getKey(), new ArrayList<List<UserInputTrainingRecord>>());
			if (!superGroupEntry.getValue().entrySet().isEmpty()) {
				for (int index=0; index<superGroupEntry.getValue().entrySet().iterator().next().getValue().size(); index++) {
					if (groupsOfAggregatedFieldGroup.get(superGroupEntry.getKey()).size()<index+1) {
						groupsOfAggregatedFieldGroup.get(superGroupEntry.getKey()).add(new ArrayList<UserInputTrainingRecord>());
					}
	//				for (int indj=0; indj<superGroupEntry.getValue().entrySet().size(); indj++){
	//					superGroupEntry.getValue().entrySet().stream().map(entry->entry.getValue().get(indj)).collect(Collectors.toList());
	//				}
					int[] currentIndex = {index};
					superGroupEntry.getValue().entrySet().forEach(fieldGroupEntry->{
						for (int indj=0; indj<fieldGroupEntry.getValue().size(); indj++){
							if (indj == currentIndex[0])
								groupsOfAggregatedFieldGroup.get(superGroupEntry.getKey()).get(currentIndex[0]).add(fieldGroupEntry.getValue().get(indj));
						}
					});
					//groupsOfAggregatedFieldGroup.get(superGroupEntry.getKey()).get(index).add(superGroupEntry.getValue().entrySet().iterator().next().getValue().get(index));
				}
			}
		});

		groupsOfAggregatedFieldGroup.entrySet().forEach(entrys->{
			entrys.getValue().forEach(group->{
				for (int idx=0; idx<group.size(); idx++) {
					if (idx < group.size() - 1
							&& (group
									.get(idx + 1)
									.getChildPositionUitrInGroup() == null || group
									.get(idx + 1)
									.getChildPositionUitrInGroup()
									.getGraphId() != group
									.get(idx)
									.getGraphId())) {
						//avoid the same y line fields loop relationship
						group.get(idx).setChildPositionUitrInGroup(group.get(idx+1));
					}
				}
			});
		});
		return groupsOfAggregatedFieldGroup;
	}


	@Transactional(timeout=10000)

	public IntermediateResult saveUitrs(
			IntermediateResult intermediateResult, boolean groupFieldsAndSavePositionTree)
			throws IOException, ClassNotFoundException, ExecutionException,
			InterruptedException {
		List<TestSuite> testSuites;

		testSuites = getTestSuiteCrud()
				.createOrUpdate(intermediateResult, true);
		getTestCaseCrud().createOrUpdate(intermediateResult, true);

		intermediateResult.setTestSuitesMap(testSuites);

		Neo4jScreenNode currentNode;
		if (intermediateResult.getScreenNode().getScreenType().equals(
				Neo4jScreenNode.ScreenType.WINDOWFILEPICKER)) {
			currentNode = getWindowsSystemFilePickerScreenNodeCrud()
					.generateNode(intermediateResult, true);
		} else {
			intermediateResult.setUserId(this.getRequestUserWorkingZone().getUserIds().iterator().next());
			currentNode = getScreenNodeCrud().createOrUpdate(
					intermediateResult, true); // false will cause issue in
												// updateTestCaseRelationshipsCall.
												// the screenContextIds can't be
												// corrected set
		}

		getScreenNodeCrud().updateTestCaseRelationships(
				intermediateResult, true);// false;


		TebloudUserZone domainNode = this.getRequestUserWorkingZone();// false
		domainNode = getTebloudUserZoneCrud().updateScreenNodes(domainNode,
				currentNode, true);

		Long tmp = currentNode.getId();
		if (null == tmp)
			throw new IllegalStateException("node id");

		if (groupFieldsAndSavePositionTree) {

			Map<Double, List<List<UserInputTrainingRecord>>> groups = this.groupingFields(intermediateResult);
			if (intermediateResult.isSamePageUpdate() && groups.size()>0) {
				//delete existing field group relationships in this screen nodes,
				getScreenNodeCrud().deleteFieldGroupRel(intermediateResult.getScreenNode().getGraphId());
				currentNode = getScreenNodeCrud().createOrUpdate(
						intermediateResult, true);
			} else if (groups.size()>0) {
				currentNode = getScreenNodeCrud().createOrUpdate(
						intermediateResult, true);
			}

		}
		return intermediateResult;
	}

	private void normalizeAllUitrs(IntermediateResult iResult) {
		iResult.getScreenNode().getActionUitrs().forEach(uitr->{
			if (StringUtils.isEmpty(uitr.getElementCoreHtmlCodeWithoutGuidValue()))
				GlobalUtils.nomalizeUitr(uitr);
		});
		iResult.getScreenNode().getClickUitrs().forEach(uitr->{
			if (StringUtils.isEmpty(uitr.getElementCoreHtmlCodeWithoutGuidValue()))
				GlobalUtils.nomalizeUitr(uitr);
		});
		iResult.getScreenNode().getScreenElementChangeUitrs().forEach(uitr->{
			if (StringUtils.isEmpty(uitr.getElementCoreHtmlCodeWithoutGuidValue()))
				GlobalUtils.nomalizeUitr(uitr);
		});
		iResult.getScreenNode().getUserInputUitrs().forEach(uitr->{
			if (StringUtils.isEmpty(uitr.getElementCoreHtmlCodeWithoutGuidValue()))
				GlobalUtils.nomalizeUitr(uitr);
		});
	}

	/**
	 * Save intermediate result.
	 *
	 * @param intermediateResult
	 *            the intermediate result
	 * @return true, if successful
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 * @throws ExecutionException
	 *             the execution exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 */
	@CrossOrigin
	//@Transactional(timeout=10000)
	@RequestMapping(value = "/{domainName}/{testSuiteName}/{testCaseName}/saveIntermediateResult", method = RequestMethod.POST)
	public IntermediateResult saveIntermediateResult(
			@RequestBody IntermediateResult intermediateResult)
			throws IOException, ClassNotFoundException, ExecutionException,
			InterruptedException {

		//normalizeAllUitrs(intermediateResult);
		//normalize will be done in trainingRepo saving action
		ITebloudPredictor pioP = this.getPredictorService(intermediateResult.getDomainName());
		ITrainingEntityPioRepo pioEventRepo = this.createTrainerMachine(intermediateResult.getDomainName());
		_log.info("pio repo status: " + ((TrainingEntityPioRepo)pioEventRepo).getPioDecisionTreeEventClient().getStatus());
		getNeo4jSession().clear();
		fixJacksonIgnoredLoopReference(intermediateResult);
		this.saveUitrs(intermediateResult, false);
		/////////////////////////////////////replace by java promise async
		this.getTrainerQueque().getThreadExecutor().pause();


		CompletableFuture<IntermediateResult> future = CompletableFuture
				.supplyAsync(
						new TebloudPioRepo(intermediateResult,
								GlobalUtils.getTargetObject(pioP), pioEventRepo, this.getRunningModeProperties(), this.getAppProperties()),
						this.getTrainerQueque().getThreadExecutor())
				.thenApply(
						(trainedIResult) -> {
							try {
								_log.info("start saving..." + intermediateResult.getScreenNode()
														.getName());
								IntermediateResult retVal = this.saveUitrs(
										trainedIResult, true);
								_log.info("Currently finished screen saving is, "
												+ retVal.getScreenNode()
														.getName());
								_log.info("unfinished # of training futures is, "
												+ (getTrainerQueque()
														.getFuturesPool()
														.entrySet()
														.stream()
														.filter(entry -> {
															return !entry
																	.getKey()
																	.isDone();
														}).count() - 1));
								return retVal;
							} catch (Exception e) {
								// TODO Auto-generated catch block
								_log.info("commit uitrs failed" + e.getMessage());
								return null;
							}
						});
		this.getTrainerQueque().getFuturesPool().put(future, intermediateResult);
		//boolean[] chained = {false};
		List<CompletableFuture<IntermediateResult>> keyList = new ArrayList<CompletableFuture<IntermediateResult>>(this.getTrainerQueque().getFuturesPool().keySet());
		for ( int index = keyList.size()-1 ; index >= 0 ; index-- ) {
			CompletableFuture<IntermediateResult> key = keyList.get(index);
			IntermediateResult value = this.getTrainerQueque().getFuturesPool().get(keyList.get(index));
			if (!key.isDone() && value.getScreenNode().getGraphId().equals(intermediateResult.getScreenNode().getGraphId())) {

				key.thenApplyAsync((previousResult)-> future, getTrainerQueque().getThreadExecutor());
				//chained[0]= true;
				break;
			}
		};
//		if (!chained[0]) {
//			CompletableFuture<IntermediateResult> future = CompletableFuture.supplyAsync(new PioTrainer(intermediateResult, this.getPioPredictor() ), this.getTrainerQueque().getThreadExecutor()).thenApply((trainedIResult)->{
//				try {
//					IntermediateResult retVal =  this.saveUitrs(trainedIResult, true);
//					System.out.println("Currently finished screen saving is, " + retVal.getScreenNode().getName() );
//					System.out.println("unfinished # of training futures is, " + (getTrainerQueque().getFuturesPool().entrySet().stream().filter(entry->{
//						return !entry.getKey().isDone();
//					}).count()-1));
//					return retVal;
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//					return null;
//				}
//			});
//			this.getTrainerQueque().getFuturesPool().put(future, intermediateResult);
//		}
		this.getTrainerQueque().getFuturesPool().entrySet().removeIf(entry->entry.getKey().isDone());
		this.getTrainerQueque().getThreadExecutor().resume();
		_log.info("future is ready to go, return the save iresult call to ui");
		return intermediateResult;
	}

	/**
	 * Save intermediate result for windows file picker.
	 *
	 * @param intermediateResult
	 *            the intermediate result
	 * @return the intermediate result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 * @throws ExecutionException
	 *             the execution exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 */
	@CrossOrigin
	@RequestMapping(value = "/{domainName}/{testSuiteName}/{testCaseName}/saveIntermediateResultForWindowsFilePicker", method = RequestMethod.POST)
	public IntermediateResult saveIntermediateResultForWindowsFilePicker(
			@RequestBody IntermediateResult intermediateResult)
			throws IOException, ClassNotFoundException, ExecutionException,
			InterruptedException {

		getTestSuiteCrud().createOrUpdate(intermediateResult, true);

		WindowsSystemFilePickerScreenNode currentNode = getWindowsSystemFilePickerScreenNodeCrud()
				.createOrUpdate(intermediateResult, false); // false

		getTestCaseCrud().createOrUpdate(intermediateResult, true);

		currentNode = getWindowsSystemFilePickerScreenNodeCrud()
				.updateTestCaseRelationships(currentNode, intermediateResult,
						false);// false


//		WebDomain domainNode = getWebDomainCrud().createOrUpdate(
//				intermediateResult, false);// false
//
//		domainNode = getWebDomainCrud().updateScreenNodes(domainNode,
//				currentNode, true);

		Long tmp = currentNode.getId();
		if (null == tmp)
			throw new IllegalStateException("node id");
		intermediateResult.getScreenNode().setGraphId(tmp);
		return intermediateResult;
	}

	// /**
	// * Train input pio.
	// *
	// * @param records
	// * the records
	// * @return the list
	// * @throws ExecutionException
	// * the execution exception
	// * @throws InterruptedException
	// * the interrupted exception
	// * @throws IOException
	// * Signals that an I/O exception has occurred.
	// */
	// @CrossOrigin
	// @RequestMapping(value = "/trainIntoPIO", method = RequestMethod.POST)
	// public Set<? extends WebElementTrainingRecord> trainInputPIO(
	// @RequestBody Set<? extends WebElementTrainingRecord> records)
	// throws ExecutionException, InterruptedException, IOException {
	//
	// for (WebElementTrainingRecord record : records) {
	// if (null != record) {
	// trainInputPIO(record);
	// }
	// }
	// return records;
	// }

	// private WebElementTrainingRecord trainInputPIO(
	// WebElementTrainingRecord record)
	// throws ExecutionException, InterruptedException, IOException {
	// // List<Greeting> greetings = new ArrayList<Greeting>();
	//
	// if (null != record) {
	// String eventId = PredictionIOTrainer.sentTrainingEntity(record);
	// record.setTrainedResult(eventId);
	// }
	//
	// return record;
	// }










	/**
	 * Find new triggered elements.
	 *
	 * @param doms
	 *            the doms
	 * @return the list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ParserConfigurationException
	 *             the parser configuration exception
	 * @throws TransformerException
	 *             the transformer exception
	 * @throws Html2DomException
	 *             the html2 dom exception
	 */
	@CrossOrigin
	@RequestMapping(value = "/{domainName}/{testSuiteName}/{testCaseName}/findNewTriggeredElements", method = RequestMethod.POST)
	public List<WebElementTrainingRecord> findNewTriggeredElements(@PathVariable String testSuiteName, @PathVariable String domainName, @PathVariable String testCaseName,
			@RequestBody Map<String, List<HTMLSource>> doms)
			throws IOException, ParserConfigurationException,
			TransformerException, Html2DomException {
		List<WebElementTrainingRecord> retVal =  preprocessing(domainName, testCaseName, doms);
		retVal.forEach(uitr->{
			uitr.setUserInputType(UserInputType.unPredicted);
		});

		return retVal;
	}


	/**
	 * Preprocessing.
	 *
	 * @param dom
	 *            the dom
	 * @return the list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ParserConfigurationException
	 *             the parser configuration exception
	 * @throws TransformerException
	 *             the transformer exception
	 * @throws Html2DomException
	 */
	@CrossOrigin
	@RequestMapping(value = "/{domainName}/{testSuiteName}/{testCaseName}/preprocessing", method = RequestMethod.POST, produces = "application/json")
	// preprocessing doesn't need a screenNamePredictStrategy to choose if we
	// want to remove the processed elements becuase
	// preprocessing always remove processed elements
	//@Transactional
	@ResponseBody
	public List<WebElementTrainingRecord> preprocessing(@PathVariable String domainName, @PathVariable String testCaseName,
			@RequestBody Map<String, List<HTMLSource>> doms)
			throws IOException, ParserConfigurationException,
			TransformerException, Html2DomException {

		List<String> csvStrings = new ArrayList<String>();
		// List<String> coreDomStrings = new ArrayList<String>();
		List<Node> coreDomNodes = new ArrayList<Node>();
		List<String> coreDomNodeSources = new ArrayList<String>();
		UitrParser uitrParser = new UitrParser(testCaseName, domainName, null, doms.get("previousDoms"), doms.get("newDoms"), this.getPredictorService(domainName));
		//List<HTMLSource> previousDoms = uitrParser.getPreviousHtmlSource().orElse(null);//doms.get("previousDoms");
		List<HTMLSource> newDoms = uitrParser.getNewHtmlSource();//doms.get("newDoms");
		//markPreviousScreenDomProcessedElements(previousDoms, newDoms);
		for (int i = 0; i < newDoms.size(); i++) {
			HTMLSource tempNewDom = newDoms.get(i);
			//System.out.println(tempNewDom.getDomDoc().length());

			if (!tempNewDom.isVisible())
				continue;

			Document doc = tempNewDom.getDomDocument();//GlobalUtils.html2Dom(tempNewDom.getDomDoc());

			// GlobalUtils.printDocument(doc.getDocumentElement(), System.out);
			WebFormUserInputsCollector col = getWebFormUserInputsCollector();
			col.collectUserInputs(doc, newDoms.get(i).getXpathOfFrame(), false);
			// System.out.println("\n*******************\n");
			// System.out.println("\n*******************\n");

			for (com.bigtester.ate.tcg.model.UserInputDom inputDom : col
					.getUserInputs()) {
				StringBuffer temp = new StringBuffer(
						inputDom.generateMachineLearningDomSource());
				// for (Node node :
				// inputDom.getMachineLearningDomHtmlPointers()) {
				// if (null != node) {
				// ByteArrayOutputStream stringOutput = new
				// ByteArrayOutputStream();
				// GlobalUtils.printDocument(node, stringOutput);
				// stringOutput.toString();
				// temp.append(stringOutput.toString());
				// }
				// }
				if (StringUtils.isNotEmpty(temp)) {
					csvStrings.add(temp.toString());
					// coreDomStrings.add(inputDom.generateCoreDomNodeSource());
					coreDomNodes.add(inputDom.getDomNodePointer());
					coreDomNodeSources
							.add(inputDom.generateCoreDomNodeSource());
				}

				// System.out.println("\n--above Node print----\n");

				// List<Node> nodes =
				// inputDom.getMachineLearningDomHtmlPointers();
				// if (nodes != null)
				// for (Node node : nodes)
				// GlobalUtils.printDocument(node, System.out);
				// System.out.println("\n------above node ML code---------\n");
				// GlobalUtils.printDocument(inputDom.getLabelDomPointer(),
				// System.out);
				// System.out
				// .println("\n------above node lable code-----------------------\n");

				// List<Node> nodes2 = inputDom.getAdditionalInfoNodes();
				// if (nodes2 != null)
				// for (Node node2 : nodes2)
				// GlobalUtils.printDocument(node2, System.out);
				// System.out
				// .println("\n=======above node additional info code=========\n");

			}
		}



		List<WebElementTrainingRecord> retVal = new ArrayList<WebElementTrainingRecord>();
		for (int i = 0; i < csvStrings.size(); i++) {
			WebElementTrainingRecord uitr = new WebElementTrainingRecord();
			StringBuffer temp = new StringBuffer(csvStrings.get(i));
			String temp2 = temp.toString();
			if (null == temp2)
				uitr.setInputMLHtmlCode("");
			else
				uitr.setInputMLHtmlCode(temp2.replaceAll("\r", "").replaceAll(
						"\n", ""));

			String tagName = coreDomNodes.get(i).getNodeName();
			uitr.setElementCoreHtmlCode(coreDomNodeSources.get(i)
					.replaceAll("\r", "").replaceAll("\n", ""));
			uitr.setElementCoreHtmlCodeWithoutGuidValue(GlobalUtils
					.convertToComparableString(coreDomNodeSources.get(i)));
			uitr.setInputMLHtmlWords(GlobalUtils
					.removeTagAndAttributeNamesAndDigits(uitr.getInputMLHtmlCode()));
			if (tagName.equalsIgnoreCase("a")) {
				uitr.setUserInputType(UserInputType.SCREENJUMPER);
				uitr.setBelongToCurrentTestCase(false);
			} else {
				uitr.setBelongToCurrentTestCase(true);
			}
			if (tagName.equalsIgnoreCase("input")) {
				if (coreDomNodes.get(i).getAttributes().getNamedItem("type") != null
						&& ((Element) coreDomNodes.get(i)).getAttribute("type")
								.equalsIgnoreCase("file")) {
					uitr.setUserInputType(UserInputType.INSCREENJUMPER);
				} else if (coreDomNodes.get(i).getAttributes()
						.getNamedItem("type") != null
						&& ((Element) coreDomNodes.get(i)).getAttribute("type")
								.equalsIgnoreCase("submit")) {
					uitr.setUserInputType(UserInputType.SCREENJUMPER);
				} else if (coreDomNodes.get(i).getAttributes()
						.getNamedItem("type") != null
						&& ((Element) coreDomNodes.get(i)).getAttribute("type")
								.equalsIgnoreCase("button")) {
					uitr.setUserInputType(UserInputType.SCREENJUMPER);
				}
			}
			if (tagName.equalsIgnoreCase("button")) {

				uitr.setUserInputType(UserInputType.SCREENJUMPER);

			}

			retVal.add(uitr);

		}

		return retVal;
	}

	/**
	 * Fill out step advice.
	 *
	 * @param screenName
	 *            the screen name
	 * @param screenNameConfidence
	 *            the screen name confidence
	 * @param uitrs
	 *            the uitrs
	 * @return the step advice
	 */
	private ScreenStepsAdvice initializeStepAdvice(
			ScreenNamePredictStrategy strategy, String screenName, String testCaseName,
			Double screenNameConfidence, List<WebElementTrainingRecord> uitrs) {

		final ScreenStepsAdvice retVal = new ScreenStepsAdvice();
		retVal.setScreenNamePredictStrategy(strategy);
		retVal.setScreenName(screenName);
		retVal.setScreenNameConfidence(screenNameConfidence);
		Iterator<TestSuite> itr = this.getTestSuiteRepo().getTestSuitesByTestCaseName(testCaseName).iterator();
		retVal.getTestSuiteMap().add( itr.next());
		retVal.getTestSuiteMap().add( itr.next());
		retVal.setTestCaseName(testCaseName);

		for (int index = 0; index < uitrs.size(); index++) {
			WebElementTrainingRecord record = uitrs.get(index);
			if (StringUtils.isEmpty(record.getInputLabelName())
					&& StringUtils.isEmpty(record.getPioPredictLabelResult()
							.getValue())) {
				continue;
			} else {
				if (StringUtils.isEmpty(record.getInputLabelName())) {
					record.setInputLabelName(record.getPioPredictLabelResult()
							.getValue());
				}
			}
			if (record.getGraphId() != null && record.getGraphId() > 0)
				record.setBelongToCurrentTestCase(false);
			// if (record.getUserInputType().equals(UserInputType.USERINPUT)) {
			// retVal.getUserInputUitrs().add(new UserInputTrainingRecord(
			// record));
			// } else {
			boolean labeledActionRecordHasBeenCategorized = false;
			boolean aiPredictedScreenChangerLabel = false;
			boolean aiPredictedInScreenJumper = false;
			boolean aiPredictScreenJumperLabel = false;

			if (record.getGraphId() != null
					&& record.getGraphId() > 0
					&& record.getUserInputType().equals(
							UserInputType.SCREENCHANGEUITR)
					&& record.isActionTrigger()
					&& !record.getTestcases().isEmpty()
					|| (aiPredictedScreenChangerLabel = this
							.getScreenNodeCrud()
							.matchScreenElementChangerLabelNamesInTestCase(
									retVal.getScreenName(),
									record.getInputLabelName(), null))) {
				// Set<ScreenElementChangeUITR> sameLabeledUitrs =
				// retVal.getScreenElementChangeUitrs().stream().parallel().filter(uitr->uitr.getInputLabelName().equalsIgnoreCase(record.getInputLabelName())).collect(Collectors.toSet());
				// if (sameLabeledUitrs.isEmpty()) {
				ScreenElementChangeUITR tempUitr = new ScreenElementChangeUITR(
						record);
				if (aiPredictedScreenChangerLabel) {
					tempUitr.setId(0l);
					tempUitr.setElementChangedScreen(null);
					tempUitr.setActionTrigger(true);
					tempUitr.setBelongToCurrentTestCase(true);
					// tempUitr.getStepOuts().removeAll(tempUitr.getStepOuts());
					// tempUitr.getTestcases().removeAll(tempUitr.getTestcases());
					tempUitr.setTriggeredBy(null);
				}
				// retVal.getScreenElementChangeUitrs().add(tempUitr
				// );
				retVal.getTestStepRunners().add(tempUitr);
				labeledActionRecordHasBeenCategorized = true;
				// } else {
				// ScreenElementChangeUITR mostConfidentSameLabeledRecord =
				// sameLabeledUitrs.stream().max(Comparator.comparing(ScreenElementChangeUITR::getPioPredictConfidence)).get();
				// if (mostConfidentSameLabeledRecord.getPioPredictConfidence()
				// < record.getPioPredictConfidence()) {
				// retVal.getScreenElementChangeUitrs().remove(mostConfidentSameLabeledRecord);
				// retVal.getScreenElementChangeUitrs().add(new
				// ScreenElementChangeUITR(record));
				// labeledActionRecordHasBeCategorized = true;
				// }
				// }

			}

			if (record.getGraphId() != null
					&& record.getGraphId() > 0
					&& record.getUserInputType().equals(
							UserInputType.INSCREENJUMPER)
					&& record.isActionTrigger()
					&& !record.getTestcases().isEmpty()
					|| (aiPredictedInScreenJumper = this.getScreenNodeCrud()
							.matchInScreenJumperLabelNamesInTestCase(
									retVal.getScreenName(),
									record.getInputLabelName(), null))) {
				// Set<InScreenJumperTrainingRecord> sameLabeledUitrs =
				// retVal.getInScreenJumperUitrs().stream().parallel().filter(uitr->uitr.getInputLabelName().equalsIgnoreCase(record.getInputLabelName())).collect(Collectors.toSet());
				// if (sameLabeledUitrs.isEmpty()) {
				InScreenJumperTrainingRecord tempUitr = new InScreenJumperTrainingRecord(
						record);
				if (aiPredictedInScreenJumper) {
					tempUitr.setId(0l);
					tempUitr.setClickTimes(0);
					tempUitr.setActionTrigger(true);
					tempUitr.setBelongToCurrentTestCase(true);
					// tempUitr.getStepOuts().removeAll(tempUitr.getStepOuts());
					// tempUitr.getTestcases().removeAll(tempUitr.getTestcases());
					tempUitr.setTriggeredBy(null);
				}
				// retVal.getInScreenJumperUitrs().add(
				// tempUitr);
				retVal.getTestStepRunners().add(tempUitr);
				labeledActionRecordHasBeenCategorized = true;
				// } else {
				// InScreenJumperTrainingRecord mostConfidentSameLabeledRecord =
				// sameLabeledUitrs.stream().max(Comparator.comparing(InScreenJumperTrainingRecord::getPioPredictConfidence)).get();
				// if (mostConfidentSameLabeledRecord.getPioPredictConfidence()
				// < record.getPioPredictConfidence()) {
				// retVal.getInScreenJumperUitrs().remove(mostConfidentSameLabeledRecord);
				// retVal.getInScreenJumperUitrs().add(new
				// InScreenJumperTrainingRecord(record));
				// labeledActionRecordHasBeCategorized = true;
				// }
				// }

			}

			if (
			// record.getGraphId()>0 && record.getGraphId()!=null
			// && record.getUserInputType().equals(
			// UserInputType.SCREENJUMPER) && record.isActionTrigger() &&
			// !record.getTestcases().isEmpty()
			// ||
			(aiPredictScreenJumperLabel = this.getScreenNodeCrud()
					.matchScreenJumperLabelNamesInTestCase(
							retVal.getScreenName(), record.getInputLabelName(),
							null))) {
				// Set<ScreenJumperElementTrainingRecord> sameLabeledUitrs =
				// retVal.getScreenJumperUitrs().stream().parallel().filter(uitr->uitr.getInputLabelName().equalsIgnoreCase(record.getInputLabelName())).collect(Collectors.toSet());
				// if (sameLabeledUitrs.isEmpty()) {
				ScreenJumperElementTrainingRecord tempUitr = new ScreenJumperElementTrainingRecord(
						record);
				if (aiPredictScreenJumperLabel) {
					tempUitr.setId(0l);
					tempUitr.setActionTrigger(true);
					tempUitr.setBelongToCurrentTestCase(true);
					tempUitr.getStepOuts().removeAll(tempUitr.getStepOuts());
					// tempUitr.getTestcases().removeAll(tempUitr.getTestcases());
					tempUitr.setTriggeredBy(null);
				}
				// retVal.getScreenJumperUitrs().add(
				// tempUitr);
				retVal.getTestStepRunners().add(tempUitr);
				labeledActionRecordHasBeenCategorized = true;
				// } else {
				// ScreenJumperElementTrainingRecord
				// mostConfidentSameLabeledRecord =
				// sameLabeledUitrs.stream().max(Comparator.comparing(ScreenJumperElementTrainingRecord::getPioPredictConfidence)).get();
				// if (mostConfidentSameLabeledRecord.getPioPredictConfidence()
				// < record.getPioPredictConfidence()) {
				// retVal.getScreenJumperUitrs().remove(mostConfidentSameLabeledRecord);
				// retVal.getScreenJumperUitrs().add(new
				// ScreenJumperElementTrainingRecord(record));
				// labeledActionRecordHasBeCategorized = true;
				// }
				// }

			}

			if (!labeledActionRecordHasBeenCategorized) {
				if (record.getUserInputType().equals(UserInputType.USERINPUT)) {
					boolean aiPredictNonActionUserInputLabel = false;
					if (record.getGraphId() != null
							&& record.getGraphId() > 0
							&& record.getUserInputType().equals(
									UserInputType.USERINPUT)
							&& !record.isActionTrigger()
							&& !record.getTestcases().isEmpty()
							|| (aiPredictNonActionUserInputLabel = this
									.getScreenNodeCrud()
									.matchNonActionUserInputLabelNamesInTestCase(
											retVal.getScreenName(),
											record.getInputLabelName(), null))) {
						// Set<ScreenJumperElementTrainingRecord>
						// sameLabeledUitrs =
						// retVal.getScreenJumperUitrs().stream().parallel().filter(uitr->uitr.getInputLabelName().equalsIgnoreCase(record.getInputLabelName())).collect(Collectors.toSet());
						// if (sameLabeledUitrs.isEmpty()) {
						UserInputTrainingRecord tempUitr = new UserInputTrainingRecord(
								record);
						if (aiPredictNonActionUserInputLabel) {
							tempUitr.setId(0l);
							tempUitr.setActionTrigger(false);
							tempUitr.setBelongToCurrentTestCase(true);
							// tempUitr.getStepOuts().removeAll(tempUitr.getStepOuts());
							// tempUitr.getTestcases().removeAll(tempUitr.getTestcases());
							tempUitr.setTriggeredBy(null);
						}
						// retVal.getUserInputUitrs().add(
						// tempUitr);
						retVal.getTestStepRunners().add(tempUitr);
					} else if (!retVal.getUserInputUitrs().isEmpty()) {
						UserInputTrainingRecord tempUitr = new UserInputTrainingRecord(
								record);
						tempUitr.setId(0l);
						tempUitr.setActionTrigger(false);
						tempUitr.setBelongToCurrentTestCase(true);
						// tempUitr.getStepOuts().removeAll(tempUitr.getStepOuts());
						// tempUitr.getTestcases().removeAll(tempUitr.getTestcases());
						tempUitr.setTriggeredBy(null);
						// retVal.getUserInputUitrs().add(
						// tempUitr);
						retVal.getTestStepRunners().add(tempUitr);
					}
				} else if (record.getUserInputType().equals(
						UserInputType.INSCREENJUMPER)) {
					retVal.getArchivedInScreenJumperUitrs().add(
							new InScreenJumperTrainingRecord(record));
				} else if (record.getUserInputType().equals(
						UserInputType.SCREENCHANGEUITR)) {
					retVal.getArchivedScreenElementChangeUitrs().add(
							new ScreenElementChangeUITR(record));
					// } else if
					// (record.getUserInputType().equals(UserInputType.USERINPUT))
					// {
					// retVal.getUserInputUitrs().add(new
					// UserInputTrainingRecord( record));
				} else if (record.getUserInputType().equals(
						UserInputType.SCREENJUMPER)) {
					retVal.getArchivedScreenJumperUitrs().add(
							new ScreenJumperElementTrainingRecord(record));
				}
			}

		}

		if (retVal.getUserInputUitrs().isEmpty()) {
			List<WebElementTrainingRecord> userInputTypeUitrs = uitrs
					.stream()
					.parallel()
					.filter(uitr -> uitr.getUserInputType().equals(
							UserInputType.USERINPUT))
					.collect(Collectors.toList());
			if (!userInputTypeUitrs.isEmpty()) {
				userInputTypeUitrs
						.forEach(uitr -> {
							UserInputTrainingRecord tempUitr = new UserInputTrainingRecord(
									uitr);
							tempUitr.setId(0l);
							tempUitr.setActionTrigger(false);
							tempUitr.setBelongToCurrentTestCase(false);
							// tempUitr.getStepOuts().removeAll(tempUitr.getStepOuts());
							// tempUitr.getTestcases().removeAll(tempUitr.getTestcases());
						tempUitr.setTriggeredBy(null);
						retVal.getArchivedUserInputUitrs().add(tempUitr);
					});
			}
		}
		// retVal.setAlternativeUserInputTypes(alternativeUserInputTypes);
		return retVal;
	}

	private ScreenStepsAdvice initializeStepAdvice(
			ScreenNamePredictStrategy strategy,
			String screenName,
			Double screenNameConfidence,
			Map<WebElementTrainingRecord, List<WebElementTrainingRecord>> probabilityCalculatedUitrsForThisScreen,
			Map<String, List<WebElementTrainingRecord>> probCalculatedUitrsTestRunnerTable) {
		final ScreenStepsAdvice retVal = new ScreenStepsAdvice();
		retVal.setScreenNamePredictStrategy(strategy);
		retVal.setScreenName(screenName);
		retVal.setScreenNameConfidence(screenNameConfidence);
		List<WebElementTrainingRecord> uitrs = new ArrayList<WebElementTrainingRecord>(
				probabilityCalculatedUitrsForThisScreen.keySet());
		for (int index = 0; index < uitrs.size(); index++) {
			WebElementTrainingRecord record = uitrs.get(index);
			if (StringUtils.isEmpty(record.getInputLabelName())
					&& StringUtils.isEmpty(record.getPioPredictLabelResult()
							.getValue())) {
				continue;
			} else {
				if (StringUtils.isEmpty(record.getInputLabelName())) {
					record.setInputLabelName(record.getPioPredictLabelResult()
							.getValue());
				}
			}
			if (record.getGraphId() != null && record.getGraphId() > 0)
				record.setBelongToCurrentTestCase(false);
			// if (record.getUserInputType().equals(UserInputType.USERINPUT)) {
			// retVal.getUserInputUitrs().add(new UserInputTrainingRecord(
			// record));
			// } else {
			boolean labeledActionRecordHasBeenCategorized = false;
			boolean aiPredictedScreenChangerLabel = false;
			boolean aiPredictedInScreenJumper = false;
			boolean aiPredictScreenJumperLabel = false;

			if (record.getGraphId() != null
					&& record.getGraphId() > 0
					&& record.getUserInputType().equals(
							UserInputType.SCREENCHANGEUITR)
					&& record.isActionTrigger()
					&& !record.getTestcases().isEmpty()
					|| (aiPredictedScreenChangerLabel = this
							.getScreenNodeCrud()
							.matchScreenElementChangerLabelNamesInTestCase(
									retVal.getScreenName(),
									record.getInputLabelName(), null))) {
				// Set<ScreenElementChangeUITR> sameLabeledUitrs =
				// retVal.getScreenElementChangeUitrs().stream().parallel().filter(uitr->uitr.getInputLabelName().equalsIgnoreCase(record.getInputLabelName())).collect(Collectors.toSet());
				// if (sameLabeledUitrs.isEmpty()) {
				ScreenElementChangeUITR tempUitr = new ScreenElementChangeUITR(
						record);
				if (aiPredictedScreenChangerLabel) {
					tempUitr.setId(0l);
					tempUitr.setElementChangedScreen(null);
					tempUitr.setActionTrigger(true);
					tempUitr.setBelongToCurrentTestCase(true);
					// tempUitr.getStepOuts().removeAll(tempUitr.getStepOuts());
					// tempUitr.getTestcases().removeAll(tempUitr.getTestcases());
					tempUitr.setTriggeredBy(null);
				}
				// retVal.getScreenElementChangeUitrs().add(tempUitr
				// );
				retVal.getTestStepRunners().add(tempUitr);
				probCalculatedUitrsTestRunnerTable.put(tempUitr.getCoreGuid(),
						probabilityCalculatedUitrsForThisScreen.get(uitrs
								.get(index)));
				labeledActionRecordHasBeenCategorized = true;
				// } else {
				// ScreenElementChangeUITR mostConfidentSameLabeledRecord =
				// sameLabeledUitrs.stream().max(Comparator.comparing(ScreenElementChangeUITR::getPioPredictConfidence)).get();
				// if (mostConfidentSameLabeledRecord.getPioPredictConfidence()
				// < record.getPioPredictConfidence()) {
				// retVal.getScreenElementChangeUitrs().remove(mostConfidentSameLabeledRecord);
				// retVal.getScreenElementChangeUitrs().add(new
				// ScreenElementChangeUITR(record));
				// labeledActionRecordHasBeCategorized = true;
				// }
				// }

			}

			if (record.getGraphId() != null
					&& record.getGraphId() > 0
					&& record.getUserInputType().equals(
							UserInputType.INSCREENJUMPER)
					&& record.isActionTrigger()
					&& !record.getTestcases().isEmpty()
					|| (aiPredictedInScreenJumper = this.getScreenNodeCrud()
							.matchInScreenJumperLabelNamesInTestCase(
									retVal.getScreenName(),
									record.getInputLabelName(), null))) {
				// Set<InScreenJumperTrainingRecord> sameLabeledUitrs =
				// retVal.getInScreenJumperUitrs().stream().parallel().filter(uitr->uitr.getInputLabelName().equalsIgnoreCase(record.getInputLabelName())).collect(Collectors.toSet());
				// if (sameLabeledUitrs.isEmpty()) {
				InScreenJumperTrainingRecord tempUitr = new InScreenJumperTrainingRecord(
						record);
				if (aiPredictedInScreenJumper) {
					tempUitr.setId(0l);
					tempUitr.setClickTimes(0);
					tempUitr.setActionTrigger(true);
					tempUitr.setBelongToCurrentTestCase(true);
					// tempUitr.getStepOuts().removeAll(tempUitr.getStepOuts());
					// tempUitr.getTestcases().removeAll(tempUitr.getTestcases());
					tempUitr.setTriggeredBy(null);
				}
				// retVal.getInScreenJumperUitrs().add(
				// tempUitr);
				retVal.getTestStepRunners().add(tempUitr);
				probCalculatedUitrsTestRunnerTable.put(tempUitr.getCoreGuid(),
						probabilityCalculatedUitrsForThisScreen.get(uitrs
								.get(index)));
				labeledActionRecordHasBeenCategorized = true;
				// } else {
				// InScreenJumperTrainingRecord mostConfidentSameLabeledRecord =
				// sameLabeledUitrs.stream().max(Comparator.comparing(InScreenJumperTrainingRecord::getPioPredictConfidence)).get();
				// if (mostConfidentSameLabeledRecord.getPioPredictConfidence()
				// < record.getPioPredictConfidence()) {
				// retVal.getInScreenJumperUitrs().remove(mostConfidentSameLabeledRecord);
				// retVal.getInScreenJumperUitrs().add(new
				// InScreenJumperTrainingRecord(record));
				// labeledActionRecordHasBeCategorized = true;
				// }
				// }

			}

			if (
			// record.getGraphId()>0 && record.getGraphId()!=null
			// && record.getUserInputType().equals(
			// UserInputType.SCREENJUMPER) && record.isActionTrigger() &&
			// !record.getTestcases().isEmpty()
			// ||
			(aiPredictScreenJumperLabel = this.getScreenNodeCrud()
					.matchScreenJumperLabelNamesInTestCase(
							retVal.getScreenName(), record.getInputLabelName(),
							null))) {
				// Set<ScreenJumperElementTrainingRecord> sameLabeledUitrs =
				// retVal.getScreenJumperUitrs().stream().parallel().filter(uitr->uitr.getInputLabelName().equalsIgnoreCase(record.getInputLabelName())).collect(Collectors.toSet());
				// if (sameLabeledUitrs.isEmpty()) {
				ScreenJumperElementTrainingRecord tempUitr = new ScreenJumperElementTrainingRecord(
						record);
				if (aiPredictScreenJumperLabel) {
					tempUitr.setId(0l);
					tempUitr.setActionTrigger(true);
					tempUitr.setBelongToCurrentTestCase(true);
					tempUitr.getStepOuts().removeAll(tempUitr.getStepOuts());
					// tempUitr.getTestcases().removeAll(tempUitr.getTestcases());
					tempUitr.setTriggeredBy(null);
				}
				// retVal.getScreenJumperUitrs().add(
				// tempUitr);
				retVal.getTestStepRunners().add(tempUitr);
				probCalculatedUitrsTestRunnerTable.put(tempUitr.getCoreGuid(),
						probabilityCalculatedUitrsForThisScreen.get(uitrs
								.get(index)));
				labeledActionRecordHasBeenCategorized = true;
				// } else {
				// ScreenJumperElementTrainingRecord
				// mostConfidentSameLabeledRecord =
				// sameLabeledUitrs.stream().max(Comparator.comparing(ScreenJumperElementTrainingRecord::getPioPredictConfidence)).get();
				// if (mostConfidentSameLabeledRecord.getPioPredictConfidence()
				// < record.getPioPredictConfidence()) {
				// retVal.getScreenJumperUitrs().remove(mostConfidentSameLabeledRecord);
				// retVal.getScreenJumperUitrs().add(new
				// ScreenJumperElementTrainingRecord(record));
				// labeledActionRecordHasBeCategorized = true;
				// }
				// }

			}

			if (!labeledActionRecordHasBeenCategorized) {
				if (record.getUserInputType().equals(UserInputType.USERINPUT)) {
					boolean aiPredictNonActionUserInputLabel = false;
					if (record.getGraphId() != null
							&& record.getGraphId() > 0
							&& record.getUserInputType().equals(
									UserInputType.USERINPUT)
							&& !record.isActionTrigger()
							&& !record.getTestcases().isEmpty()
							|| (aiPredictNonActionUserInputLabel = this
									.getScreenNodeCrud()
									.matchNonActionUserInputLabelNamesInTestCase(
											retVal.getScreenName(),
											record.getInputLabelName(), null))) {
						// Set<ScreenJumperElementTrainingRecord>
						// sameLabeledUitrs =
						// retVal.getScreenJumperUitrs().stream().parallel().filter(uitr->uitr.getInputLabelName().equalsIgnoreCase(record.getInputLabelName())).collect(Collectors.toSet());
						// if (sameLabeledUitrs.isEmpty()) {
						UserInputTrainingRecord tempUitr = new UserInputTrainingRecord(
								record);
						if (aiPredictNonActionUserInputLabel) {
							tempUitr.setId(0l);
							tempUitr.setActionTrigger(false);
							tempUitr.setBelongToCurrentTestCase(true);
							// tempUitr.getStepOuts().removeAll(tempUitr.getStepOuts());
							// tempUitr.getTestcases().removeAll(tempUitr.getTestcases());
							tempUitr.setTriggeredBy(null);
						}
						// retVal.getUserInputUitrs().add(
						// tempUitr);
						retVal.getTestStepRunners().add(tempUitr);
						probCalculatedUitrsTestRunnerTable.put(tempUitr
								.getCoreGuid(),
								probabilityCalculatedUitrsForThisScreen
										.get(uitrs.get(index)));
					} else if (!retVal.getUserInputUitrs().isEmpty()) {
						UserInputTrainingRecord tempUitr = new UserInputTrainingRecord(
								record);
						tempUitr.setId(0l);
						tempUitr.setActionTrigger(false);
						tempUitr.setBelongToCurrentTestCase(true);
						// tempUitr.getStepOuts().removeAll(tempUitr.getStepOuts());
						// tempUitr.getTestcases().removeAll(tempUitr.getTestcases());
						tempUitr.setTriggeredBy(null);
						// retVal.getUserInputUitrs().add(
						// tempUitr);
						retVal.getTestStepRunners().add(tempUitr);
						probCalculatedUitrsTestRunnerTable.put(tempUitr
								.getCoreGuid(),
								probabilityCalculatedUitrsForThisScreen
										.get(uitrs.get(index)));
					}
				} else if (record.getUserInputType().equals(
						UserInputType.INSCREENJUMPER)) {
					retVal.getArchivedInScreenJumperUitrs().add(
							new InScreenJumperTrainingRecord(record));
				} else if (record.getUserInputType().equals(
						UserInputType.SCREENCHANGEUITR)) {
					retVal.getArchivedScreenElementChangeUitrs().add(
							new ScreenElementChangeUITR(record));
					// } else if
					// (record.getUserInputType().equals(UserInputType.USERINPUT))
					// {
					// retVal.getUserInputUitrs().add(new
					// UserInputTrainingRecord( record));
				} else if (record.getUserInputType().equals(
						UserInputType.SCREENJUMPER)) {
					retVal.getArchivedScreenJumperUitrs().add(
							new ScreenJumperElementTrainingRecord(record));
				}
			}

		}

		if (retVal.getUserInputUitrs().isEmpty()) {
			List<WebElementTrainingRecord> userInputTypeUitrs = uitrs
					.stream()
					.parallel()
					.filter(uitr -> uitr.getUserInputType().equals(
							UserInputType.USERINPUT))
					.collect(Collectors.toList());
			if (!userInputTypeUitrs.isEmpty()) {
				userInputTypeUitrs
						.forEach(uitr -> {
							UserInputTrainingRecord tempUitr = new UserInputTrainingRecord(
									uitr);
							tempUitr.setId(0l);
							tempUitr.setActionTrigger(false);
							tempUitr.setBelongToCurrentTestCase(false);
							// tempUitr.getStepOuts().removeAll(tempUitr.getStepOuts());
							// tempUitr.getTestcases().removeAll(tempUitr.getTestcases());
						tempUitr.setTriggeredBy(null);
						retVal.getArchivedUserInputUitrs().add(tempUitr);
					});
			}
		}
		// retVal.setAlternativeUserInputTypes(alternativeUserInputTypes);
		Set<ITestStepRunner> jumpers = retVal.getScreenJumperUitrs();
		if (jumpers.size()>1) {
			Comparator<ITestStepRunner> cmp = (
					uitr1, uitr2) -> Long.valueOf(uitr1
					.getScreenUitrProbability())
					.compareTo(Long.valueOf(
							uitr2.getScreenUitrProbability()));
			List<ITestStepRunner> sortedJumpersByProb = jumpers.stream().sorted(cmp.reversed()).collect(Collectors.toList());
			if (sortedJumpersByProb.get(0).getScreenUitrProbability()==0) {
				cmp = (
						uitr1, uitr2) -> uitr1
						.getPioPredictConfidence()
						.compareTo(
								uitr2.getPioPredictConfidence());
						List<ITestStepRunner> sortedJumpersByConfidence = jumpers.stream().sorted(cmp.reversed()).collect(Collectors.toList());
						sortedJumpersByConfidence.forEach(jumper->{
							if (sortedJumpersByProb.indexOf(jumper)>0) {

								retVal.getTestStepRunners().remove(jumper);
							}
						});
			} else {
				sortedJumpersByProb.forEach(jumper->{
					if (sortedJumpersByProb.indexOf(jumper)>0) {

						retVal.getTestStepRunners().remove(jumper);
					}
				});
			}


		}
		return retVal;
	}

	private boolean unifyClickablesByCompareTwoList(
			Set<? extends WebElementTrainingRecord> uitrsInDB,
			Set<? extends WebElementTrainingRecord> uitrsInAdvice) {
		boolean retVal = false;
		if (!uitrsInAdvice.isEmpty()) {

			for (WebElementTrainingRecord adviceRecord : uitrsInAdvice) {
				for (WebElementTrainingRecord recordInDB : uitrsInDB) {
					if (recordInDB
							.getPioPredictLabelResult()
							.getValue()
							.equalsIgnoreCase(
									adviceRecord.getPioPredictLabelResult()
											.getValue())) {
						if (!recordInDB.getTestcases().isEmpty()
								&& recordInDB.isActionTrigger()) {
							adviceRecord.setBelongToCurrentTestCase(true);

						}
						// don't break out, for example, in job search result
						// screen, only one of the joblink is action and belong
						// to test case.
						// break;
					}

				}
				if (adviceRecord.isBelongToCurrentTestCase()) {
					retVal = true;
					break;
				}
			}

		}
		return retVal;
	}

	private boolean unifyClickablesByCompareWithPossibleJumperLabelNames(
			Iterable<String> uitrsInDB,
			Set<? extends WebElementTrainingRecord> uitrsInAdvice) {
		boolean retVal = false;
		WebElementTrainingRecord mostConfidentClickable = null;
		if (!uitrsInAdvice.isEmpty()) {

			for (WebElementTrainingRecord adviceRecord : uitrsInAdvice) {
				for (String recordInDB : uitrsInDB) {
					if (recordInDB.equalsIgnoreCase(adviceRecord
							.getPioPredictLabelResult().getValue())) {
						adviceRecord.setBelongToCurrentTestCase(true);

						// don't break out, for example, in job search result
						// screen, only one of the joblink is action and belong
						// to test case.
						break;
					}

				}
				if (adviceRecord.isBelongToCurrentTestCase()) {
					retVal = true;
					if (mostConfidentClickable == null) {
						mostConfidentClickable = adviceRecord;
					} else {
						if (mostConfidentClickable.getPioPredictConfidence() > adviceRecord
								.getPioPredictConfidence()) {
							adviceRecord.setBelongToCurrentTestCase(false);
						} else {
							mostConfidentClickable
									.setBelongToCurrentTestCase(false);
							mostConfidentClickable = adviceRecord;
						}
					}
				}
			}

		}

		return retVal;
	}

	private void markTestCaseBelongingFlagByCompareTwoList(
			List<UserInputTrainingRecord> uitrsInDb,
			List<UserInputTrainingRecord> uitrsInAdvice) {

		if (!uitrsInAdvice.isEmpty()) {

			for (UserInputTrainingRecord adviceRecord : uitrsInAdvice) {
				List<UserInputTrainingRecord> sameLabeledUitrsInDb = uitrsInDb
						.parallelStream()
						.filter(uitrInDb -> uitrInDb
								.getPioPredictLabelResult()
								.getValue()
								.equalsIgnoreCase(
										adviceRecord.getPioPredictLabelResult()
												.getValue())
								&& !uitrInDb.getTestcases().isEmpty())
						.collect(Collectors.toList());
				if (!sameLabeledUitrsInDb.isEmpty()) {

					adviceRecord.setBelongToCurrentTestCase(true);
					break;
				}
			}
		}
	}

	private void markTestCaseBelongingFlagByCompareWithLabelNames(
			Iterable<String> labelNamesInDb,
			List<UserInputTrainingRecord> uitrsInAdvice) {

		if (!uitrsInAdvice.isEmpty()) {

			for (UserInputTrainingRecord adviceRecord : uitrsInAdvice) {
				List<String> sameLabeledNamesInDb = StreamSupport
						.stream(labelNamesInDb.spliterator(), true)
						.filter(labelNameInDb -> labelNameInDb
								.equalsIgnoreCase(adviceRecord
										.getPioPredictLabelResult().getValue()))
						.collect(Collectors.toList());
				if (!sameLabeledNamesInDb.isEmpty()) {

					adviceRecord.setBelongToCurrentTestCase(true);
					break;
				}
			}
		}
	}

	private void moveNonTestCaseBelongedUitrsToArchiveSet(
			Set<? extends WebElementTrainingRecord> from, Set toSet) {
		for (Iterator<? extends WebElementTrainingRecord> itr = from.iterator(); itr
				.hasNext();) {
			WebElementTrainingRecord uitrRecord = itr.next();
			if (!uitrRecord.isBelongToCurrentTestCase()) {
				toSet.add(uitrRecord);
				itr.remove();
			}
		}
	}

	private void moveNonTestCaseBelongedClickablesToArchiveSet(
			List<? extends WebElementTrainingRecord> from, List toList) {
		for (Iterator<? extends WebElementTrainingRecord> itr = from.iterator(); itr
				.hasNext();) {
			WebElementTrainingRecord uitrRecord = itr.next();
			if (!uitrRecord.isBelongToCurrentTestCase()) {
				toList.add(uitrRecord);
				itr.remove();
			}
		}
	}

	// private void unifyClickablesBySameScreenNameNode(Neo4jScreenNode
	// screenNode, ScreenStepsAdvice stepAdvice) {
	// boolean inScreenJumperUnifyResult = false;
	//
	// Set<InScreenJumperTrainingRecord> inScreenUitrInAdvice =
	// stepAdvice.getInScreenJumperUitrs();
	// if (!inScreenUitrInAdvice.isEmpty()) {
	// inScreenJumperUnifyResult =
	// unifyClickablesByCompareTwoList(screenNode.getClickUitrs(),
	// inScreenUitrInAdvice);
	//
	// }
	// boolean screenElementChangerUnifyResult = false;
	// Set<ScreenElementChangeUITR> screenChangeUitrInAdvice =
	// stepAdvice.getScreenElementChangeUitrs();
	// if (!screenChangeUitrInAdvice.isEmpty()) {
	// screenElementChangerUnifyResult =
	// unifyClickablesByCompareTwoList(screenNode.getScreenElementChangeUitrs(),
	// screenChangeUitrInAdvice );
	//
	// }
	//
	// boolean screenJumperUnifyResult = false;
	// Set<ScreenJumperElementTrainingRecord> screenUitrInAdvice =
	// stepAdvice.getScreenJumperUitrs();
	// if (!screenUitrInAdvice.isEmpty()) {
	// screenJumperUnifyResult =
	// unifyClickablesByCompareTwoList(screenNode.getActionUitrs(),
	// screenUitrInAdvice);
	//
	// }
	//
	// if (inScreenJumperUnifyResult || screenElementChangerUnifyResult ||
	// screenJumperUnifyResult) {
	// moveNonTestCaseBelongedUitrsToArchiveSet(stepAdvice.getInScreenJumperUitrs(),
	// (Set) stepAdvice.getArchivedInScreenJumperUitrs());
	//
	//
	// moveNonTestCaseBelongedUitrsToArchiveSet(stepAdvice.getScreenElementChangeUitrs(),
	// (Set) stepAdvice.getArchivedScreenElementChangeUitrs());
	//
	//
	// moveNonTestCaseBelongedUitrsToArchiveSet(stepAdvice.getScreenJumperUitrs(),
	// (Set) stepAdvice.getArchivedScreenJumperUitrs());
	// }
	//
	// }

	// private void unifyClickablesByJumperInSameScreenNameNode(Iterable<String>
	// jumperLabelNames, ScreenStepsAdvice stepAdvice) {
	// boolean inScreenJumperUnifyResult = false;
	//
	// Set<InScreenJumperTrainingRecord> inScreenUitrInAdvice =
	// stepAdvice.getInScreenJumperUitrs();
	// if (!inScreenUitrInAdvice.isEmpty()) {
	// inScreenJumperUnifyResult =
	// unifyClickablesByCompareWithPossibleJumperLabelNames(jumperLabelNames,
	// inScreenUitrInAdvice);
	//
	// }
	// boolean screenElementChangerUnifyResult = false;
	// Set<ScreenElementChangeUITR> screenChangeUitrInAdvice =
	// stepAdvice.getScreenElementChangeUitrs();
	// if (!screenChangeUitrInAdvice.isEmpty()) {
	// screenElementChangerUnifyResult =
	// unifyClickablesByCompareWithPossibleJumperLabelNames(jumperLabelNames,
	// screenChangeUitrInAdvice );
	//
	// }
	//
	// boolean screenJumperUnifyResult = false;
	// Set<ScreenJumperElementTrainingRecord> screenUitrInAdvice =
	// stepAdvice.getScreenJumperUitrs();
	// if (!screenUitrInAdvice.isEmpty()) {
	// screenJumperUnifyResult =
	// unifyClickablesByCompareWithPossibleJumperLabelNames(jumperLabelNames,
	// screenUitrInAdvice);
	//
	// }
	//
	// if (inScreenJumperUnifyResult || screenElementChangerUnifyResult ||
	// screenJumperUnifyResult) {
	// moveNonTestCaseBelongedUitrsToArchiveSet(stepAdvice.getInScreenJumperUitrs(),
	// (Set) stepAdvice.getArchivedInScreenJumperUitrs());
	//
	//
	// moveNonTestCaseBelongedUitrsToArchiveSet(stepAdvice.getScreenElementChangeUitrs(),
	// (Set) stepAdvice.getArchivedScreenElementChangeUitrs());
	//
	//
	// moveNonTestCaseBelongedUitrsToArchiveSet(stepAdvice.getScreenJumperUitrs(),
	// (Set) stepAdvice.getArchivedScreenJumperUitrs());
	// }
	//
	// }

	private boolean markWebElementTrainingRecordBelongingThisTestCaseByFieldNameWithoutConsultScreenNode(
			Set<? extends UserInputTrainingRecord> clickables) {
		// Note: add judgement on confidence. low confidence should considered
		// to be handled specially.
		boolean retVal = false;
		for (UserInputTrainingRecord uitr : clickables) {
			Iterable<? extends UserInputTrainingRecord> sameNameRecords = getUserClickInputTrainingRecordRepo()
					.findByPioPredictLabelResultValue(
							uitr.getPioPredictLabelResult().getValue());
			Set<UserInputTrainingRecord> inTestCaseRecords = StreamSupport
					.stream(sameNameRecords.spliterator(), true)
					.filter(sameNameRecord -> !sameNameRecord.getTestcases()
							.isEmpty()).collect(Collectors.toSet());

			if (inTestCaseRecords.size() > 0) {
				uitr.setBelongToCurrentTestCase(true);
				retVal = true;

			}
		}
		return retVal;
	}

	// private void
	// archiveClickablesNotBelongingThisTestCaseByFieldNameWithoutConsultScreenNode(ScreenStepsAdvice
	// stepAdvice) {
	// if
	// (markWebElementTrainingRecordBelongingThisTestCaseByFieldNameWithoutConsultScreenNode(stepAdvice.getInScreenJumperUitrs()))
	// {
	// moveNonTestCaseBelongedUitrsToArchiveSet(stepAdvice.getInScreenJumperUitrs(),
	// (Set) stepAdvice.getArchivedInScreenJumperUitrs());
	// }
	//
	// if
	// (markWebElementTrainingRecordBelongingThisTestCaseByFieldNameWithoutConsultScreenNode(stepAdvice.getScreenElementChangeUitrs()))
	// {
	// moveNonTestCaseBelongedUitrsToArchiveSet(stepAdvice.getScreenElementChangeUitrs(),
	// (Set) stepAdvice.getArchivedScreenElementChangeUitrs());
	// }
	// if
	// (markWebElementTrainingRecordBelongingThisTestCaseByFieldNameWithoutConsultScreenNode(stepAdvice.getScreenJumperUitrs()))
	// {
	// moveNonTestCaseBelongedUitrsToArchiveSet(stepAdvice.getScreenJumperUitrs(),
	// (Set) stepAdvice.getArchivedScreenJumperUitrs());
	// }
	//
	// }
	private boolean isEndScreen(ScreenStepsAdvice stepAdvice) {
		boolean retVal = false;
		Iterable<String> endScreenNames = this.getScreenNodeCrud()
				.findDistinctEndScreenNamesInTestCase(null);
		long matchEndScreenNamesInTestCase = StreamSupport
				.stream(endScreenNames.spliterator(), true)
				.filter(endScreenName -> endScreenName
						.equalsIgnoreCase(stepAdvice.getScreenName())).count();
		if (matchEndScreenNamesInTestCase > 0)
			retVal = true;
		return retVal;
	}

	Map<Long, Set<UserInputType>> findAlternativeUserInputType(
			List<WebElementTrainingRecord> predictedUitrs) {
		final Map<Long, Set<UserInputType>> retVal = new ConcurrentHashMap<Long, Set<UserInputType>>();
		for (WebElementTrainingRecord uitr : predictedUitrs) {
			Iterable<UserInputTrainingRecord> uitrsInDb = getUserClickInputTrainingRecordRepo()
					.findByPioPredictLabelResultValue(
							uitr.getPioPredictLabelResult().getValue());
			Set<UserInputTrainingRecord> sameLabeledUitrsWithDifferentUserInputType = StreamSupport
					.stream(uitrsInDb.spliterator(), true)
					.filter(uitrInDb -> !uitrInDb.getUserInputType().equals(
							uitr.getUserInputType()))
					.collect(Collectors.toSet());
			if (!sameLabeledUitrsWithDifferentUserInputType.isEmpty()) {
				for (UserInputTrainingRecord sameLabeledUitrWithDifferentUserInputType : sameLabeledUitrsWithDifferentUserInputType) {
					if (uitr.getGraphId() != null && uitr.getGraphId() > 0
							&& retVal.containsKey(uitr.getGraphId())) {
						if (!retVal.get(uitr.getGraphId()).contains(
								sameLabeledUitrWithDifferentUserInputType
										.getUserInputType())) {
							retVal.get(uitr.getGraphId()).add(
									sameLabeledUitrWithDifferentUserInputType
											.getUserInputType());
						}
					} else {
						Set<UserInputType> userInputTypes = new HashSet<UserInputType>();
						userInputTypes
								.add(sameLabeledUitrWithDifferentUserInputType
										.getUserInputType());
						retVal.put(uitr.getGraphId(), userInputTypes);
					}
				}
			}
		}

		return retVal;
	}

	private void markNonClickablesTestCaseBelongingFlagBySameNameScreen(
			Neo4jScreenNode screenNode, ScreenStepsAdvice stepAdvice) {
		List<UserInputTrainingRecord> uitrsInAdvice = stepAdvice
				.getUserInputUitrs();

		if (!uitrsInAdvice.isEmpty()) {
			markTestCaseBelongingFlagByCompareTwoList(
					screenNode.getUserInputUitrs(), uitrsInAdvice);
		}
	}

	private void markNonClickablesTestCaseBelongingFlagByLabelNamesInSameNameScreen(
			Iterable<String> labelnames, ScreenStepsAdvice stepAdvice) {
		List<UserInputTrainingRecord> uitrsInAdvice = stepAdvice
				.getUserInputUitrs();

		if (!uitrsInAdvice.isEmpty()) {
			markTestCaseBelongingFlagByCompareWithLabelNames(labelnames,
					uitrsInAdvice);
		}
	}

	private void archiveNonClickableTestCaseBelongingFlagByFieldNameWithoutConsultScreenNode(
			ScreenStepsAdvice stepAdvice) {
		Set<UserInputTrainingRecord> uitrs = new LinkedHashSet<UserInputTrainingRecord>();
		uitrs.addAll(stepAdvice.getUserInputUitrs());
		if (markWebElementTrainingRecordBelongingThisTestCaseByFieldNameWithoutConsultScreenNode(uitrs)) {
			moveNonTestCaseBelongedClickablesToArchiveSet(
					stepAdvice.getUserInputUitrs(),
					(List) stepAdvice.getArchivedUserInputUitrs());

		}
	}


	private Map<WebElementTrainingRecord, List<WebElementTrainingRecord>> findSimilarStructuredUitrs(
			List<WebElementTrainingRecord> sameLabeledUitrs) {

		Map<WebElementTrainingRecord, List<WebElementTrainingRecord>> retVal = new ConcurrentHashMap<WebElementTrainingRecord, List<WebElementTrainingRecord>>();
		if (sameLabeledUitrs.size() > 1) {
			sameLabeledUitrs
					.stream()
					.forEach(
							uitrE -> {
								if (retVal
										.values()
										.stream()
										.filter(uitrList -> uitrList
												.contains(uitrE))
										.collect(Collectors.toSet()).size() == 0) {
									List<WebElementTrainingRecord> similarUitrs = sameLabeledUitrs
											.stream()
											.parallel()
											.filter(uitr -> GlobalUtils
													.isSimilarHtmlStructure(
															uitr.getElementCoreHtmlNode()
																	.get(),
															uitrE.getElementCoreHtmlNode()
																	.get(),
															uitr.getInputMLHtmlCode(),
															uitrE.getInputMLHtmlCode()))
											.collect(Collectors.toList());

									// if
									// (retVal.keySet().stream().parallel().filter(uitr2->
									// similarUitrs.size()>1 && GlobalUtils
									// .isSimilarHtmlStructure(
									// uitr2.getElementCoreHtmlNode().get(),
									// uitrE.getElementCoreHtmlNode().get(),
									// uitr2.getInputMLHtmlCode(),
									// uitrE.getInputMLHtmlCode())).count()==0 )
									// {
									// //similarUitrs.remove(uitrE);
									// retVal.put(uitrE, similarUitrs);
									// }
									retVal.put(uitrE, similarUitrs);
								}
							});
		}
		return retVal;

	}


	private Map<WebElementTrainingRecord, List<WebElementTrainingRecord>> parseAllPredictableUitrsWithSameLabeledUitrsForRunner(String testCaseName, String domainName, String screenName,
			Map<String, List<HTMLSource>> sources) {

		// we will need to predict every single elements on web page to see if
		// we can match some clickables


		Map<WebElementTrainingRecord, List<WebElementTrainingRecord>> retVal = new ConcurrentHashMap<WebElementTrainingRecord, List<WebElementTrainingRecord>>();
		//try {
			UitrParser uitrParser = new UitrParser(testCaseName, domainName, screenName, sources, this.getPredictorService(domainName));
			List<WebElementTrainingRecord> allNodes = uitrParser.parseAllUitrsRawPredicted( false, true, this.getRunningModeProperties().getRunningMode()); //slow level 2
			allNodes.forEach(uitr->{
				if (uitr.getElementCoreHtmlCode().contains(TebloudMachine.MONITORED_NODE_HTML_CODE))
					_log.info(TebloudMachine.MONITORED_NODE_HTML_CODE + " node before removed: " + uitr.getInputLabelName());

			});
			allNodes

			.removeIf(
					entryAB -> {
						Optional<Node> node = entryAB
								.getElementCoreHtmlNode();
						if (!node.isPresent())
							return true;
						String invisible = Optional
								.ofNullable(
										((Element) node.get())
												.getAttribute(WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME))
								.orElse("");
						String processed = Optional
								.ofNullable(
										((Element) node.get())
												.getAttribute(WebFormUserInputsCollectorHtmlTerms.PREVIOUS_PROCESSED_ATTR_NAME))
								.orElse("");
						if (invisible
								.equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE)
								|| processed
										.equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE)) {
							return true;
						}
						return false;
					});
			allNodes.forEach(uitr->{
				if (uitr.getElementCoreHtmlCode().contains(TebloudMachine.MONITORED_NODE_HTML_CODE) && uitr.getElementCoreHtmlCode().length()<TebloudMachine.MONITORED_NODE_LENGTH)
					_log.info(TebloudMachine.MONITORED_NODE_HTML_CODE + " node after removed: " + uitr.getInputLabelName() + ": " + uitr.getPioPredictConfidence() + ":" + uitr.getElementCoreHtmlCode());

			});
			allNodes = allNodes.stream().distinct()
					.filter(uitr -> !uitr.getInputLabelName().isEmpty())
					.collect(Collectors.toList());
			allNodes.stream().parallel().forEach(uitr -> {
				uitr.setInputLabelName(uitr.getInputLabelName().toUpperCase());
			});

			// List<WebElementTrainingRecord> uitrsInDb = allNodes
			// .stream()
			// .parallel()
			// .filter(uitr -> uitr.getPioPredictConfidence().equals(1.0d))
			// .collect(Collectors.toList());
			Map<String, List<WebElementTrainingRecord>> groupedUitrsByInputLabelName = allNodes
					.stream()
					.collect(
							Collectors
									.groupingBy(WebElementTrainingRecord::getInputLabelName));
			if (groupedUitrsByInputLabelName.get(TebloudMachine.MONITORED_LABEL) != null) {
				groupedUitrsByInputLabelName.get(TebloudMachine.MONITORED_LABEL).forEach(uitr->{
					if (uitr.getElementCoreHtmlCode().contains(TebloudMachine.MONITORED_NODE_HTML_CODE) && uitr.getElementCoreHtmlCode().length()<TebloudMachine.MONITORED_NODE_LENGTH)
						_log.info(TebloudMachine.MONITORED_NODE_HTML_CODE + " node after grouping " + uitr.getInputLabelName() + ": " + uitr.getElementCoreHtmlCode());

				});
			}
			if (StringUtils.isEmpty(TebloudMachine.MONITORED_LABEL)) {
				groupedUitrsByInputLabelName.values().forEach(uitrs->{
					uitrs.forEach(uitr->{
					if (uitr.getElementCoreHtmlCode().contains(TebloudMachine.MONITORED_NODE_HTML_CODE) && uitr.getElementCoreHtmlCode().length()<TebloudMachine.MONITORED_NODE_LENGTH)
						_log.info(TebloudMachine.MONITORED_NODE_HTML_CODE + " node after grouping " + uitr.getInputLabelName() + ": " + uitr.getElementCoreHtmlCode());
					});
				});
			}
			groupedUitrsByInputLabelName
					.entrySet()
					.stream()
					.parallel()
					.forEach(
							entry -> {
								List<WebElementTrainingRecord> sameLabeledUitrs = entry
										.getValue();
								/*
								 * //find similar html structured uitrs with
								 * same labels, they are repeated items on
								 * screen. (could be multiple groups)
								 * Map<WebElementTrainingRecord,
								 * List<WebElementTrainingRecord>>
								 * similarStructuredUitrs =
								 * findSimilarStructuredUitrs(sameLabeledUitrs);
								 *
								 * List<WebElementTrainingRecord>
								 * allSimilarUitrs = similarStructuredUitrs
								 * .entrySet().stream()
								 * .map(Map.Entry::getValue)
								 * .collect(Collectors.toList()).stream()
								 * .flatMap(Collection::stream)
								 * .collect(Collectors.toList()); //remove all
								 * similar utirs.
								 * sameLabeledUitrs.removeAll(allSimilarUitrs);
								 */
								// first consideration is the confidence of the
								// prediction
								Comparator<WebElementTrainingRecord> cmp = (
										uitr1, uitr2) -> uitr1
										.getPioPredictConfidence()
										.compareTo(
												uitr2.getPioPredictConfidence());
								sameLabeledUitrs = sameLabeledUitrs.stream()
										.sorted(cmp.reversed())
										.collect(Collectors.toList());
								// find the most qualified candidate based on
								// confidence
								// few more considerations
								// 1. length of the corehtml (same as machine
								// learning html
								// 2. if the confidence smaller than the
								// threshold
								// 3. in the same labeled uitrs, should we
								// higher rate the html shorter one?
								// 4. if the corehtml has input or other high
								// rating html element.
								// 5. if the corehtml has embeded the other
								// labeled uitr corehtml
								// We need a interface to calculate the overall
								// probability based on conditions.
								/*
								 * draft of the algrithm uitr probability
								 * algrithm (Will be done on labels after
								 * initializeStepAdvice, which are almost sure
								 * that they are part of this screen elements)
								 * weight condition 10000 4.0 mlhtml has one
								 * input or other html element input -10000*#
								 * 5.0 mlHtml has embeded the other high
								 * confidence same labeled uitr mlhtml
								 * (confidence higher than threshold) -5000*#
								 * 5.1 mlHtml has embeded the other high
								 * confidence different labeled uitr mlhtml for
								 * this screen. ((confidence higher than
								 * threshold) (s.d ~
								 * length-avg<0?*-1:length-avg) * 10000 3.
								 * mlhtml length range (get from neo4j db for
								 * avg and standard diviation) for this label
								 * x10000 2.0 pio confidence (0-~1) 5000 4.1
								 * mlhtml has 2 inputs elements -10000* (#-2)
								 * 4.2 mlhtml has 3 or more inputs elements if
								 * userinput -10000 or clikable 0 4.3 mlhtml has
								 * no input 50000 2.1 pio confidence = 1
								 */
								WebElementTrainingRecord keyUitr = sameLabeledUitrs.get(0);
								if (keyUitr.getPioPredictConfidence()==1.0d) {

									List<WebElementTrainingRecord> actionTriggersInExistingDBRecords = sameLabeledUitrs
										.stream()
										.filter(uitr -> uitr.getPioPredictConfidence()==1.0d && uitr.isActionTrigger() && uitr.getInputLabelName().equalsIgnoreCase(keyUitr.getInputLabelName()))
										.collect(Collectors.toList());
									if (actionTriggersInExistingDBRecords.size()>0 && actionTriggersInExistingDBRecords.get(0) != keyUitr ) {
										sameLabeledUitrs.set(sameLabeledUitrs.indexOf(actionTriggersInExistingDBRecords.get(0)), keyUitr);
										sameLabeledUitrs.set(0, actionTriggersInExistingDBRecords.get(0));

									}
								}
								WebElementTrainingRecord key = sameLabeledUitrs
										.get(0);
								// sameLabeledUitrs.remove(key);
								retVal.put(key, sameLabeledUitrs);
								// allSimilarUitrs.forEach(uitr->{
								// List<WebElementTrainingRecord>
								// structureSimilarUitrList = new
								// ArrayList<WebElementTrainingRecord>();
								// structureSimilarUitrList.add(uitr);
								// retVal.put(uitr, structureSimilarUitrList);
								// });

							});
			retVal.values().stream().filter(uitrList->uitrList.get(0).getInputLabelName().equals(TebloudMachine.MONITORED_LABEL) || StringUtils.isEmpty(TebloudMachine.MONITORED_LABEL)).forEach(uitrs->{
				uitrs.forEach(uitr->{
				if (uitr.getElementCoreHtmlCode().contains(TebloudMachine.MONITORED_NODE_HTML_CODE) && uitr.getElementCoreHtmlCode().length()<TebloudMachine.MONITORED_NODE_LENGTH)
					_log.info(TebloudMachine.MONITORED_NODE_HTML_CODE + " node before returning call: " + uitr.getInputLabelName() + ":" +uitr.getPioPredictConfidence() + ": " +uitr.getScreenUitrProbability() +":" + uitr.getElementCoreHtmlCode()+ ":" + uitr.getInputMLHtmlWords());
				});

			});
			retVal.keySet().forEach(uitr->{
				if (uitr.getElementCoreHtmlCode().contains(TebloudMachine.MONITORED_NODE_HTML_CODE) && uitr.getElementCoreHtmlCode().length()<TebloudMachine.MONITORED_NODE_LENGTH)
					_log.info(TebloudMachine.MONITORED_NODE_HTML_CODE + " node in key before returning call: " + uitr.getInputLabelName() + ":" +uitr.getPioPredictConfidence() + ": "+uitr.getScreenUitrProbability() +":"  + uitr.getElementCoreHtmlCode());
				_log.info("every node in key before returning call: " + uitr.getInputLabelName() + ":" +uitr.getPioPredictConfidence() + ": " +uitr.getScreenUitrProbability() +":" + uitr.getElementCoreHtmlCode() + ":" + uitr.getInputMLHtmlWords());

			});
			return retVal;

			// TODO need to figure out if there is same elements appearing twice
			// (or multi times) on screen, for example two apply button, top and
			// bottom
			/*
			 * Map<String, Optional<WebElementTrainingRecord>>
			 * maxConfidentUniqueUitrsOptionals = allNodes.stream().parallel()
			 * .filter(uitr->!uitr.getPioPredictConfidence().equals(1.0d) &&
			 * !uitr.getInputLabelName().isEmpty())
			 * .collect(Collectors.groupingBy
			 * (WebElementTrainingRecord::getInputLabelName,
			 * Collectors.maxBy(Comparator
			 * .comparing(WebElementTrainingRecord::getPioPredictConfidence))));
			 * final Map<String, WebElementTrainingRecord>
			 * maxConfidentUniqueUitrs = new HashMap<String,
			 * WebElementTrainingRecord>();
			 * maxConfidentUniqueUitrsOptionals.forEach((label, maxUitr)->{ if
			 * (maxUitr.isPresent()) { maxConfidentUniqueUitrs.put(label,
			 * maxUitr.get()); } }); List<WebElementTrainingRecord>
			 * predictedUitrs = new ArrayList<WebElementTrainingRecord>();
			 * predictedUitrs.addAll(maxConfidentUniqueUitrs.values());
			 * predictedUitrs.addAll(uitrsInDb); predictedUitrs =
			 * predictedUitrs.stream().distinct().collect(Collectors.toList());
			 * final List<WebElementTrainingRecord> recordsToBeCleaned = new
			 * ArrayList<WebElementTrainingRecord>(); predictedUitrs .stream()
			 * .parallel() .collect( Collectors
			 * .groupingBy(WebElementTrainingRecord::getInputLabelName))
			 * .entrySet().stream().parallel().forEach(entry->{
			 * List<WebElementTrainingRecord> sameLabelUitrs = entry.getValue();
			 * final List<WebElementTrainingRecord> sortedSameLabelUitrs =
			 * sameLabelUitrs.stream().
			 * sorted(Comparator.comparingInt(uitr->uitr
			 * .getElementCoreHtmlCode()
			 * .length())).collect(Collectors.toList());
			 * sortedSameLabelUitrs.stream().forEach(sameLabelUitr->{ int
			 * sameLabelUitrIndexInList =
			 * sortedSameLabelUitrs.indexOf(sameLabelUitr); String
			 * sameLabelUitrCoreGuid = sameLabelUitr.getCoreGuid(); for (int
			 * aIndex= sameLabelUitrIndexInList + 1;
			 * aIndex<sortedSameLabelUitrs.size(); aIndex++) {
			 *
			 * if
			 * (sortedSameLabelUitrs.get(aIndex).getElementCoreHtmlCode().indexOf
			 * (sameLabelUitrCoreGuid)
			 * >=sameLabelUitr.getElementCoreHtmlCode().indexOf
			 * (sameLabelUitrCoreGuid)) { if
			 * (sortedSameLabelUitrs.get(aIndex).getGraphId() > 0 ) { if
			 * (!recordsToBeCleaned.contains(sameLabelUitr))
			 * recordsToBeCleaned.add(sameLabelUitr); break; } else { if
			 * (!recordsToBeCleaned.contains(sortedSameLabelUitrs.get(aIndex)))
			 * recordsToBeCleaned.add(sortedSameLabelUitrs.get(aIndex)); } } }
			 *
			 * }); });; predictedUitrs.removeAll(recordsToBeCleaned);
			 *
			 * return predictedUitrs;
			 */
//		} catch (ClassNotFoundException | IOException | ExecutionException
//				| InterruptedException e) {
//			// return new ArrayList<WebElementTrainingRecord>();
//			return retVal;
//		}

	}

	/*private ScreenStepsAdvice tuneStepAdvice(ScreenStepsAdvice stepAdvice,
			Map<String, List<HTMLSource>> sources) {
		if (stepAdvice.getJudgeThreshold() <= stepAdvice
				.getScreenNameConfidence()) {

			if (stepAdvice.getInScreenJumperUitrs().isEmpty()
					&& stepAdvice.getScreenJumperUitrs().isEmpty()
					&& stepAdvice.getScreenElementChangeUitrs().isEmpty()) {
				// check if the screenName is matching a test case end screen
				// name.
				if (isEndScreen(stepAdvice)) {
					stepAdvice.getUserInputUitrs().clear();
					stepAdvice.setLastScreenInTestCase(true);
				} else {

					// we will need to predict every single elements on web page
					// to see if we can match some clickables

					List<WebElementTrainingRecord> allNodes = getAllUitrNodes(sources, false);

					try {
						allNodes = pioPredict(allNodes, true);
						List<WebElementTrainingRecord> uitrsInDb = allNodes
								.stream()
								.parallel()
								.filter(uitr -> uitr.getPioPredictConfidence()
										.equals(1.0d))
								.collect(Collectors.toList());
						// TODO need to figure out if there is same elements
						// appearing twice (or multi times) on screen, for
						// example two apply button, top and bottom
						Map<String, Optional<WebElementTrainingRecord>> maxConfidentUniqueUitrsOptionals = allNodes
								.stream()
								.parallel()
								.filter(uitr -> !uitr.getPioPredictConfidence()
										.equals(1.0d)
										&& !uitr.getInputLabelName().isEmpty())
								.collect(
										Collectors
												.groupingBy(
														WebElementTrainingRecord::getInputLabelName,
														Collectors
																.maxBy(Comparator
																		.comparing(WebElementTrainingRecord::getPioPredictConfidence))));
						final Map<String, WebElementTrainingRecord> maxConfidentUniqueUitrs = new HashMap<String, WebElementTrainingRecord>();
						maxConfidentUniqueUitrsOptionals.forEach((label,
								maxUitr) -> {
							if (maxUitr.isPresent()) {
								maxConfidentUniqueUitrs.put(label,
										maxUitr.get());
							}
						});
						List<WebElementTrainingRecord> predictedUitrs = new ArrayList<WebElementTrainingRecord>();
						predictedUitrs.addAll(maxConfidentUniqueUitrs.values());
						predictedUitrs.addAll(uitrsInDb);
						predictedUitrs = predictedUitrs.stream().distinct()
								.collect(Collectors.toList());
						final List<WebElementTrainingRecord> recordsToBeCleaned = new ArrayList<WebElementTrainingRecord>();
						predictedUitrs
								.stream()
								.parallel()
								.collect(
										Collectors
												.groupingBy(WebElementTrainingRecord::getInputLabelName))
								.entrySet()
								.stream()
								.parallel()
								.forEach(
										entry -> {
											List<WebElementTrainingRecord> sameLabelUitrs = entry
													.getValue();
											final List<WebElementTrainingRecord> sortedSameLabelUitrs = sameLabelUitrs
													.stream()
													.sorted(Comparator
															.comparingInt(uitr -> uitr
																	.getElementCoreHtmlCode()
																	.length()))
													.collect(
															Collectors.toList());
											sortedSameLabelUitrs
													.stream()
													.forEach(
															sameLabelUitr -> {
																int sameLabelUitrIndexInList = sortedSameLabelUitrs
																		.indexOf(sameLabelUitr);
																String sameLabelUitrCoreGuid = sameLabelUitr
																		.getCoreGuid();
																for (int aIndex = sameLabelUitrIndexInList + 1; aIndex < sortedSameLabelUitrs
																		.size(); aIndex++) {

																	if (sortedSameLabelUitrs
																			.get(aIndex)
																			.getElementCoreHtmlCode()
																			.indexOf(
																					sameLabelUitrCoreGuid) >= sameLabelUitr
																			.getElementCoreHtmlCode()
																			.indexOf(
																					sameLabelUitrCoreGuid)) {
																		if (sortedSameLabelUitrs
																				.get(aIndex)
																				.getGraphId() != null
																				&& sortedSameLabelUitrs
																						.get(aIndex)
																						.getGraphId() > 0) {
																			if (!recordsToBeCleaned
																					.contains(sameLabelUitr))
																				recordsToBeCleaned
																						.add(sameLabelUitr);
																			break;
																		} else {
																			if (!recordsToBeCleaned
																					.contains(sortedSameLabelUitrs
																							.get(aIndex)))
																				recordsToBeCleaned
																						.add(sortedSameLabelUitrs
																								.get(aIndex));
																		}
																	}
																}

															});
										});
						;
						predictedUitrs.removeAll(recordsToBeCleaned);
						// Map<Long, Set<UserInputType>>
						// alternativeUserInputType =
						// findAlternativeUserInputType(predictedUitrs);
						ScreenStepsAdvice retVal = initializeStepAdvice(
								stepAdvice.getScreenNamePredictStrategy(),
								stepAdvice.getScreenName(),
								stepAdvice.getScreenNameConfidence(),
								predictedUitrs);

						return retVal;
					} catch (ClassNotFoundException | IOException
							| ExecutionException | InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					// TODO or this is a real end screen (false reported as non
					// end-screen
				}
			}
		} else {
			// TODO what to do if the screenName confidence is too low?
		}

		return stepAdvice;
	}*/

	/*public List<WebElementTrainingRecord> parseAllUitrsRawPredicted(Map<String, List<HTMLSource>> sources, boolean removeProcessedElement) throws ClassNotFoundException, IOException, ExecutionException, InterruptedException {
		List<WebElementTrainingRecord> allNodes = getAllUitrNodes(sources, removeProcessedElement);

		return pioPredict(allNodes, false);
	}*/

	@CrossOrigin
	@RequestMapping(value = "/{domainName}/{testSuiteName}/{testCaseName}/{previousScreenName}/{jumperUitrName}/{nextScreenName}", method = RequestMethod.GET)

	public Boolean validateTestStepPath(@PathVariable String testSuiteName, @PathVariable String domainName, @PathVariable String testCaseName, @PathVariable String previousScreenName,
			@PathVariable String jumperUitrName, @PathVariable String nextScreenName) {
		return this.getScreenNodeCrud().isValidScreenJumpingPath(testSuiteName, domainName, testCaseName, previousScreenName, jumperUitrName, nextScreenName);
	}

	/**
	 * Advise step uitrs.
	 *
	 * @param doms
	 *            the doms
	 * @return the step advice
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws ParserConfigurationException
	 *             the parser configuration exception
	 * @throws TransformerException
	 *             the transformer exception
	 * @throws Html2DomException
	 *             the html2 dom exception
	 * @throws ClassNotFoundException
	 *             the class not found exception
	 * @throws ExecutionException
	 *             the execution exception
	 * @throws InterruptedException
	 *             the interrupted exception
	 */
	@CrossOrigin
	@RequestMapping(value = "/{domainName}/{testSuiteName}/{testCaseName}/adviseStepUitrs", method = RequestMethod.POST)
	public Map<ScreenNamePredictStrategy, ScreenStepsAdvice> adviseStepUitrs(@PathVariable String testSuiteName, @PathVariable String domainName, @PathVariable String testCaseName,
			@RequestBody Map<String, List<HTMLSource>> doms)
			throws IOException, ParserConfigurationException,
			TransformerException, Html2DomException, ClassNotFoundException,
			ExecutionException, InterruptedException {
		//TODO add code to filter out anchor type screen jumper link or button
		Map<ScreenNamePredictStrategy, Map<String, ScreenNamePredictResult>> possibleScreenNamesPredicted = this.getPredictorService(domainName).pagePredict(testCaseName, domainName, doms,true);
		possibleScreenNamesPredicted = possibleScreenNamesPredicted
				.entrySet()
				.stream()
				.parallel()
				.filter(RunnerGlobalUtils.distinctByKey(mapEntry -> mapEntry
						.getValue().keySet().iterator().next()))
				.collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
		final Map<ScreenNamePredictStrategy, ScreenStepsAdvice> retVal = new ConcurrentHashMap<ScreenNamePredictStrategy, ScreenStepsAdvice>();


		for (Entry<ScreenNamePredictStrategy, Map<String, ScreenNamePredictResult>> entry : possibleScreenNamesPredicted
				.entrySet()) {
			ScreenNamePredictStrategy strategy = entry.getKey();
			Map<String, ScreenNamePredictResult> screenNamePredicted = entry
					.getValue();
			String predictedScreenNameStr = "";
			Double predictedScreenNameConfidence = 0D;
			if (!screenNamePredicted.isEmpty()) {
				predictedScreenNameStr = (String) screenNamePredicted.keySet()
						.toArray()[0];
				predictedScreenNameConfidence = screenNamePredicted.get(
						predictedScreenNameStr).getConfidence();
			}
			Map<WebElementTrainingRecord, List<WebElementTrainingRecord>> predictedUitrsWithSameLabeledsMap_ = this
					.parseAllPredictableUitrsWithSameLabeledUitrsForRunner(testCaseName, domainName, predictedScreenNameStr, doms);//slow level 2
			List<WebElementTrainingRecord> predictedUitrs_ = new ArrayList<WebElementTrainingRecord>(
					predictedUitrsWithSameLabeledsMap_.keySet());
			_log.info("predicted UItrs without stepAdvice init, size is :" + predictedUitrs_.size());

			predictedUitrs_
					.stream()
					.sorted(Comparator.comparing(uitr -> uitr.getInputLabelName()))
					.forEach(
							uitr -> {
								_log.info(uitr.getInputLabelName() + " :"
										+ uitr.getXpath());
							});


			// create a copy of predictedUITRSWithSameLabeledsMap

			Map<WebElementTrainingRecord, List<WebElementTrainingRecord>> predictedUitrsWithSameLabeledsMap = new ConcurrentHashMap<WebElementTrainingRecord, List<WebElementTrainingRecord>>(
					predictedUitrsWithSameLabeledsMap_);
			List<WebElementTrainingRecord> predictedUitrs = new ArrayList<WebElementTrainingRecord>(
					predictedUitrs_);

			// List<WebElementTrainingRecord> predictedUitrs =
			// parseAllPredictableUitrs(doms);
			// Map<Long, Set<UserInputType>> alternativeUserInputType = new
			// HashMap<Long, Set<UserInputType>>();
			// List<WebElementTrainingRecord> uitrs = preprocessing(doms);
			// List<WebElementTrainingRecord> predictedUitrs =
			// pioPredict(uitrs);
			// Map<Long, Set<UserInputType>> alternativeUserInputType =
			// findAlternativeUserInputType(predictedUitrs);//useless since each
			// uitr already has these values
			predictedUitrs.forEach(uitr -> {

				if (uitr.getElementCoreHtmlCode().contains(TebloudMachine.MONITORED_NODE_HTML_CODE) && uitr.getElementCoreHtmlCode().length()<TebloudMachine.MONITORED_NODE_LENGTH)
					_log.info(TebloudMachine.MONITORED_NODE_HTML_CODE +" node before stepadvice init " + uitr.getInputLabelName() + ": " + uitr.getElementCoreHtmlCode());


			});
			ScreenStepsAdvice stepAdvice = initializeStepAdvice(strategy,
					predictedScreenNameStr, testCaseName, predictedScreenNameConfidence,
					predictedUitrs);
			stepAdvice.setDevToolMessage(new DevToolMessage());
			stepAdvice.getDevToolMessage().setPages(doms.get("newDoms"));
			_log.info(stepAdvice.getScreenName()
					+ " stepAdvice1 init without probability calculation, size:"
					+ stepAdvice.getTestStepRunners().size());
			if (stepAdvice.getTestStepRunners().size() > 3
					&& stepAdvice.getTestStepRunners().size() < 6)
				_log.info(stepAdvice.getScreenName() + " ========== "
						+ stepAdvice.getTestStepRunners().size());
			stepAdvice.getTestStepRunners().forEach(uitr -> {

					if (uitr.getElementCoreHtmlCode().contains(TebloudMachine.MONITORED_NODE_HTML_CODE) && uitr.getElementCoreHtmlCode().length()<TebloudMachine.MONITORED_NODE_LENGTH)
						_log.info(TebloudMachine.MONITORED_NODE_HTML_CODE +" node after stepadvice init without probability calculation" + uitr.getInputLabelName() + ": " + uitr.getElementCoreHtmlCode());


			});
			List<String> runnerGuids = stepAdvice.getTestStepRunners().stream()
					.map(ITestStepRunner::getCoreGuid)
					.collect(Collectors.toList());
			Map<WebElementTrainingRecord, List<WebElementTrainingRecord>> probabilityReadyForCalculateUitrsForThisScreen = predictedUitrsWithSameLabeledsMap
					.entrySet()
					.stream()
					.filter(entryR -> runnerGuids.contains(entryR.getKey()
							.getCoreGuid()))
					.collect(
							Collectors.toMap(entryR -> entryR.getKey(),
									entryR -> entryR.getValue()));

			probabilityReadyForCalculateUitrsForThisScreen.entrySet().forEach(
					entryP -> {
						predictedUitrsWithSameLabeledsMap.remove(entryP
								.getKey());
					});
			Map<WebElementTrainingRecord, List<WebElementTrainingRecord>> probabilityCalculatedUitrsForThisScreen = new ConcurrentHashMap<WebElementTrainingRecord, List<WebElementTrainingRecord>>();
			if (this.getAppProperties().isEnableDecisionTreeInPrediction()) {
				createScreenProbabilityCalculator(domainName).caculateProbability(
					probabilityReadyForCalculateUitrsForThisScreen, stepAdvice,
					predictedUitrsWithSameLabeledsMap);



			probabilityReadyForCalculateUitrsForThisScreen
					.entrySet()
					.stream()
					.forEach(
							probEntry -> {
								Comparator<WebElementTrainingRecord> cmp = (a,
										b) -> Double
										.valueOf(a.getScreenUitrProbability())
										.compareTo(
												Double.valueOf(b
														.getScreenUitrProbability()));
								probEntry.getValue().sort(cmp.reversed());
								probabilityCalculatedUitrsForThisScreen.put(
										probEntry.getValue().get(0),
										probEntry.getValue());
							});
			} else {
					probabilityReadyForCalculateUitrsForThisScreen.entrySet().stream().forEach(pentry->{
					probabilityCalculatedUitrsForThisScreen.put(
							pentry.getKey(),
							pentry.getValue());
				});

			}

			// TODO remove the invisible and processed ones.
			//ll error, this remove could incorrectly remove valid uitr because it is removing by key.
//			probabilityCalculatedUitrsForThisScreen
//					.entrySet()
//					.removeIf(
//							entryAB -> {
//								Optional<Node> node = entryAB.getKey()
//										.getElementCoreHtmlNode();
//								if (!node.isPresent())
//									return true;
//								String invisible = Optional
//										.ofNullable(
//												((Element) node.get())
//														.getAttribute(WebFormUserInputsCollectorHtmlTerms.ATE_INVISIBLE_ATTR_NAME))
//										.orElse("");
//								String processed = Optional
//										.ofNullable(
//												((Element) node.get())
//														.getAttribute(WebFormUserInputsCollectorHtmlTerms.PREVIOUS_PROCESSED_ATTR_NAME))
//										.orElse("");
//								if (invisible
//										.equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE)
//										|| processed
//												.equalsIgnoreCase(WebFormUserInputsCollectorHtmlTerms.ATE_YES_ATTR_VALUE)) {
//									return true;
//								}
//								return false;
//							});
			// put the same label uitrs into archivedUitr field in stepAdvice
			// System.out.println("test");
			predictedUitrs = new ArrayList<WebElementTrainingRecord>(
					probabilityCalculatedUitrsForThisScreen.keySet());
			// alternativeUserInputType =
			// findAlternativeUserInputType(predictedUitrs);
			Map<String, List<WebElementTrainingRecord>> probabilityCalculatedUitrsStepRunnerTable = new ConcurrentHashMap<String, List<WebElementTrainingRecord>>();
			stepAdvice = initializeStepAdvice(strategy, predictedScreenNameStr,
					predictedScreenNameConfidence,
					probabilityCalculatedUitrsForThisScreen,
					probabilityCalculatedUitrsStepRunnerTable);
			stepAdvice
					.setProbabilitySortedTestRunnerSameLabeledUitrTable(probabilityCalculatedUitrsStepRunnerTable);
			_log.info(stepAdvice.getScreenName()
					+ " stepAdvice after initialization, uitr size:"
					+ stepAdvice.getTestStepRunners().size());
			stepAdvice.getProbabilitySortedTestRunnerSameLabeledUitrTable().values().forEach(uitrs->{
				uitrs.forEach(uitr->{
					_log.info("ranking of probability uitr: " + uitr.getInputLabelName() + ":" + uitr.getPioPredictConfidence() + ":" + uitr.getScreenUitrProbability() + ":" + uitr.getElementCoreHtmlCode());
				});
			});
			if (stepAdvice.getTestStepRunners().size() > 3
					&& stepAdvice.getTestStepRunners().size() < 6)
				_log.info(stepAdvice.getScreenName()
						+ " ====+======= "
						+ stepAdvice.getTestStepRunners().size());

			stepAdvice.getTestStepRunners().forEach(uitr -> {
				_log.info("stepAdvice after probability calculation, step uitr label " + uitr.getInputLabelName() + ":" + uitr.getElementCoreHtmlCode());
			});
			// tmVal = tuneStepAdvice(tmVal, doms);
			if (stepAdvice.isSuccessAdvice(this.getScreenNodeCrud())) {
				_log.info("success advice added with screen name: " + stepAdvice.getScreenName() + " with fields: "+ stepAdvice.getTestStepRunners().stream().map(uitr->uitr.getInputLabelName()).collect(Collectors.joining(",")));
				retVal.put(strategy, stepAdvice);
			}
		}
		// retVal.entrySet().stream().parallel().forEach(entry->{
		// final ScreenStepsAdvice stepAdvice = entry.getValue();
		// stepAdvice.getScreenElementChangeUitrs().forEach(uitr->{
		// stepAdvice.getTestStepRunners().add(uitr);
		// });
		// stepAdvice.getScreenJumperUitrs().forEach(jumper->{
		// stepAdvice.getTestStepRunners().add(jumper);
		// });
		// stepAdvice.getInScreenJumperUitrs().forEach(inJumper->{
		// stepAdvice.getTestStepRunners().add(inJumper);
		// });
		//
		// });
		_log.info("returning step advice:");
		return retVal;

	}

	@CrossOrigin
	@RequestMapping(value = "/{domainName}/{testSuiteName}/{testCaseName}/newAdvisedStepUitrs", method = RequestMethod.POST)
	public Map<ScreenNamePredictStrategy, ScreenStepsAdvice> newAdvisedStepUitrs(@PathVariable String testSuiteName, @PathVariable String domainName, @PathVariable String testCaseName,
			@RequestBody Map<String, List<HTMLSource>> doms)
			throws IOException, ParserConfigurationException,
			TransformerException, Html2DomException, ClassNotFoundException,
			ExecutionException, InterruptedException {
		return adviseStepUitrs(testSuiteName, testCaseName, domainName, doms);


	}


	/**
	 * @return the trainerQueque
	 */
	public TrainerQueueManager getTrainerQueque() {
		return trainerQueque;
	}

	/**
	 * @param trainerQueque the trainerQueque to set
	 */
	public void setTrainerQueque(TrainerQueueManager trainerQueque) {
		this.trainerQueque = trainerQueque;
	}


}
