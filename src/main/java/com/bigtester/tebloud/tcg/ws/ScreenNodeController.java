/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.tcg.ws;//NOPMD


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.xmlportletfactory.tebloud.tebloudhtmlsource.model.TebloudHTMLSource;
import org.xmlportletfactory.tebloud.tebloudhtmlsource.service.TebloudHTMLSourceLocalServiceUtil;

import com.bigtester.ate.tcg.model.IntermediateResult;
import com.bigtester.ate.tcg.model.ScreenNamePredictStrategy;
import com.bigtester.ate.tcg.model.domain.IndustryCategory;
import com.bigtester.ate.tcg.model.domain.Neo4jScreenNode;
import com.bigtester.ate.tcg.model.domain.TestSuite;
import com.bigtester.tebloud.tcg.config.RunningModeProperties;
import com.bigtester.tebloud.tcg.model.domain.TebloudUserZone;
import com.google.common.collect.Lists;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;


/**
 * The Class GreetingController.
 */
@RestController
//@RequestMapping(value = "/screenNode")
public class ScreenNodeController extends BaseWsController {








	@CrossOrigin
	@RequestMapping(value = "/screenNode/getScreenNodeTemplate", method = RequestMethod.GET)

	public Neo4jScreenNode newEmptyScreenNode(){
		return new Neo4jScreenNode();

	}

	@CrossOrigin
	@RequestMapping(value = "/screenNode/{graphId}/{testCaseName}", method = RequestMethod.GET)

	public IntermediateResult queryScreenNode(@PathVariable Long graphId, @PathVariable String testCaseName) throws SystemException{
		Neo4jScreenNode screen = this.getScreenNodeCrud().getScreenNodeRepo().findOne(graphId, 2);

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(TebloudHTMLSource.class, PortalClassLoaderUtil.getClassLoader());
		dynamicQuery.add(PropertyFactoryUtil.forName("TebloudHTMLSourceNeo4jId").eq(screen.getGraphId()));
		List<TebloudHTMLSource> tebloudSources = TebloudHTMLSourceLocalServiceUtil.dynamicQuery(dynamicQuery);
		if (tebloudSources.size()>0)
			screen.getSourcingDoms().forEach(sDom->{
				tebloudSources.stream().filter(ts->ts.getTebloudHTMLSourceGuid().equals(sDom.getDocGuid())).findFirst().ifPresent(tebSource->{
					sDom.setDomDoc(tebSource.getTebloudHTMLSourceContent());
				});

			});
		else
			_log.warn("Can't find the html source for screen: " + screen.getId() + ": " + screen.getName());
		screen.populateCorrectValueforFieldBelongsToTestCaseFromDBInfo(screen.getTestcases().iterator().next().getEndNode().getName());
		IntermediateResult retVal = new IntermediateResult();
		Iterable<TestSuite> testSuites = this.getTestSuiteCrud().getTestSuiteRepo().getTestSuitesByTestCaseName(testCaseName);
//		List<TestSuite> testSuites = new ArrayList<TestSuite>();
//		testSuites.add(new TestSuite("JobApplication"));
//		testSuites.add(new TestSuite("WebJobApplication"));
		retVal.setTestSuitesMap( Lists.newArrayList(testSuites));
		retVal.setScreenNode(screen);
		Iterable<Map<String,Object>> domainNodes = getTebloudUserZoneRepo().getTebloudUserZoneByScreenNodeId(screen.getGraphId());
		String domainName = domainNodes.iterator().hasNext()?((TebloudUserZone) domainNodes.iterator().next().values().iterator().next()).getZoneDomainName():"localhost";
		retVal.setDomainName(domainName);

		List<IndustryCategory> industryCategoriesMap = new ArrayList<IndustryCategory>();
		industryCategoriesMap.add(new IndustryCategory("HCM"));
		industryCategoriesMap.add(new IndustryCategory("Recruitment"));
		retVal.setIndustryCategoriesMap(industryCategoriesMap);

		retVal.setTestCaseName(testCaseName);

		retVal.setInScreenJump(false);
		return retVal;

	}
	@CrossOrigin
	@RequestMapping(value = "/screenNode/getIResultTemplate", method = RequestMethod.GET)

	public IntermediateResult getIResultTemplate(){
		IntermediateResult retVal = new IntermediateResult();
		List<TestSuite> testSuites = new ArrayList<TestSuite>();
		if (this.getRunningModeProperties().getRunningMode().equals(RunningModeProperties.RunningMode.USECASE_APP.name())) {
			testSuites.add(new TestSuite("JobApplication"));
			testSuites.add(new TestSuite("WebJobApplication"));
		} else {
			testSuites.add(new TestSuite(""));
			testSuites.add(new TestSuite(""));
		}
		retVal.setTestSuitesMap(testSuites);
		//$scope.screenType = "HTML"; //replaced by currentScreenNode.screenType;
		//$scope.screenTriggeredByPreviousScreenUitrId = 0;//replaced by querying variable mvcScreenNodes and mvcCurrentScreenNodePointer
		retVal.getScreenNode().setScreenNamePredictStrategy (ScreenNamePredictStrategy.PREDICTED_BY_VISIBLE_ELEMENT_TEXT);
		retVal.getScreenNode().setInputMLHtmlWords("");

		List<IndustryCategory> industryCategoriesMap = new ArrayList<IndustryCategory>();
		if (this.getRunningModeProperties().getRunningMode().equals(RunningModeProperties.RunningMode.USECASE_APP.name())) {
			industryCategoriesMap.add(new IndustryCategory("HCM"));
			industryCategoriesMap.add(new IndustryCategory("Recruitment"));
		} else {
			industryCategoriesMap.add(new IndustryCategory(""));
			industryCategoriesMap.add(new IndustryCategory(""));
		}
		retVal.setIndustryCategoriesMap(industryCategoriesMap);
		if (this.getRunningModeProperties().getRunningMode().equals(RunningModeProperties.RunningMode.USECASE_APP.name())) {
			retVal.setTestCaseName("QuickApply");
		}

		retVal.setInScreenJump(false);
		return retVal;
	}
	@CrossOrigin
	@RequestMapping(value = "/getNumberOfRowsInTable", method = RequestMethod.GET)

	public int getNumberOfRowsInTable() throws SystemException {

		return TebloudHTMLSourceLocalServiceUtil.getTebloudHTMLSourcesCount();
	}

}
