/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.tcg.config;

import java.io.File;
import java.util.Optional;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
//import org.springframework.web.servlet.ViewResolver;
//import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
//import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
//import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.bigtester.tebloud.machine.fasttext.config.FastTextProperties;
import com.bigtester.tebloud.tcg.config.RunningModeProperties.RunningMode;
//import com.bigtester.tebloud.tcg.model.domain.TebloudUser;
import com.bigtester.tebloud.tcg.model.domain.TebloudUserZone;
//import com.bigtester.tebloud.tcg.rest.interceptor.SecureRequestHandlerInterceptor;
import com.bigtester.tebloud.tcg.service.ml.TebloudPredictor;
import com.bigtester.tebloud.tcg.service.uicollection.DivInSpan;
import com.bigtester.tebloud.tcg.service.uicollection.IUiInvisibilityExceptionalPattern;
import com.bigtester.tebloud.tcg.service.uicollection.VisibleLabelForInvisibleInput;
import com.bigtester.tebloud.tcg.service.uicollection.matcher.IUitrOnScreenLabelMatcher;
import com.bigtester.tebloud.tcg.service.uicollection.matcher.IUserInputConsolidationMatcher;
import com.bigtester.tebloud.tcg.service.uicollection.matcher.OutOfFormInputListMatcher;
import com.bigtester.tebloud.tcg.service.uicollection.matcher.UserInputMatcher;
import com.bigtester.tebloud.tcg.service.uicollection.matcher.IUIGrouppedByAttributeMatcher.SupportedMatchingInputAttribute;
import com.bigtester.tebloud.tcg.service.uicollection.matcher.IUIGrouppedByParentTagMatcher.SupportedMatchingParentHtmlTag;
import com.bigtester.tebloud.tcg.service.uitr.IScreenProbabilityCalculator;
import com.bigtester.tebloud.tcg.service.uitr.ScreenProbabilityCalculator;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms.HtmlInputType;
import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms.UserCollectableHtmlTag;
//import com.bigtester.ate.tcg.model.ml.AteFastText;
//import com.bigtester.ate.tcg.model.ml.FastTextTrainer;
import com.bigtester.ate.tcg.model.ml.IAteFastText;
//import com.bigtester.ate.tcg.model.ml.IFastTextTrainer;
import com.bigtester.ate.tcg.model.ml.IMLPredictor;
import com.bigtester.ate.tcg.model.ml.IRemoteTebloudFastText;
import com.bigtester.ate.tcg.model.ml.ITrainingEntityPioRepo;
import com.bigtester.ate.tcg.model.ml.MLPredictor;
import com.bigtester.ate.tcg.model.ml.TrainingEntityPioRepo;
import com.bigtester.ate.tcg.model.ml.RemoteAteFastTextImpl;
import com.bigtester.ate.tcg.service.ITebloudPredictor;
import com.bigtester.ate.tcg.service.parallel.TrainerQueueManager;

// TODO: Auto-generated Javadoc
/**
 * The Class WebMvcConfig.
 */
@Configuration
@ComponentScan(basePackages = { "com.bigtester.ate.tcg", "com.bigtester.tebloud.tcg" })
@EnableAspectJAutoProxy
@EnableConfigurationProperties(TebloudApplicationProperties.class)

public class WebMvcConfig  extends WebMvcConfigurationSupport{ // extends WebMvcConfigurationSupport {
	private static Log _log = LogFactoryUtil.getLog(WebMvcConfig.class);
	@Inject
	private RunningModeProperties runningModeProperties;

	@Inject
	private TebloudApplicationProperties appProperties;

	@Value("${tebloud.fastTextServerHost:172.17.0.1}")
	private String fastTextServerHost;

	@Value("${tebloud.fasttextDockerContainerName:tebloud_fasttext_server}")
	private String fastTextDockerContainername;

	// /**
	// * {@inheritDoc}
	// */
	// @Override
	// public void configureAsyncSupport(@Nullable AsyncSupportConfigurer
	// configurer) {
	// if (null == configurer) return;
	// configurer.setDefaultTimeout(30*1000L);
	// }
	//
	// /**
	// * {@inheritDoc}
	// */
	// @Override
	// protected void configureMessageConverters(@Nullable
	// List<HttpMessageConverter<?>> converters) {
	// if (null == converters) return;
	// converters.clear();
	// super.addDefaultHttpMessageConverters(converters);
	// converters.removeIf(cov->cov.getClass()==MappingJackson2HttpMessageConverter.class);
	//// converters.stream().filter(cov->cov.getClass()==MappingJackson2HttpMessageConverter.class).forEach(conv-{
	//// conv.
	//// });
	// ObjectMapper mapper =
	// Jackson2ObjectMapperBuilder.json().applicationContext(getApplicationContext()).modules(new
	// MapMixinModule()).build();
	// MappingJackson2HttpMessageConverter cov = new
	// MappingJackson2HttpMessageConverter(mapper);
	//// cov.getObjectMapper().getDeserializationConfig().with();
	// converters.add(cov);
	// }
	//
	// /**
	// * {@inheritDoc}
	// */
	// public void addViewControllers(@Nullable ViewControllerRegistry registry) {
	// if (null == registry) return;
	// registry.addViewController("/").setViewName("chat");
	// }
	//
	// /**
	// * {@inheritDoc}
	// */
	// @Override
	// public void addResourceHandlers(@Nullable ResourceHandlerRegistry registry) {
	// if (null == registry) return;
	// registry.addResourceHandler("/resources/**").addResourceLocations("resources/");
	// }
	//
	// /**
	// * View resolver.
	// *
	// * @return the view resolver
	// */
	// @Bean
	// public ViewResolver viewResolver() {
	// ThymeleafViewResolver resolver = new ThymeleafViewResolver();
	// resolver.setTemplateEngine(templateEngine());
	// return resolver;
	// }
	//
	// /**
	// * Template engine.
	// *
	// * @return the spring template engine
	// */
	// @Bean
	// public SpringTemplateEngine templateEngine() {
	// SpringTemplateEngine engine = new SpringTemplateEngine();
	// engine.setTemplateResolver(templateResolver());
	// return engine;
	// }
	//
	// /**
	// * Template resolver.
	// *
	// * @return the template resolver
	// */
	// @Bean
	// public TemplateResolver templateResolver() {
	// ServletContextTemplateResolver resolver = new
	// ServletContextTemplateResolver();
	// resolver.setPrefix("/WEB-INF/templates/");
	// resolver.setSuffix(".html");
	// resolver.setTemplateMode("HTML5");
	// resolver.setCacheable(false);
	// return resolver;
	// }

	  @Override
	  public void configurePathMatch(PathMatchConfigurer configurer) {
	    configurer.setUseSuffixPatternMatch(false);
	  }
	/**
	 * Div in span.
	 *
	 * @return the i ui invisibility exceptional pattern
	 */
	@Bean
	// @Scope("prototype")
	public IUiInvisibilityExceptionalPattern divInSpan() {
		// List<IUiInvisibilityExceptionalPattern> retVal = new
		// ArrayList<IUiInvisibilityExceptionalPattern>();
		// retVal.add(new DivInSpan());
		return new DivInSpan();
	}


	/**
	 * Ml trainer.
	 *
	 * @param autName
	 *            the aut name
	 * @param fastText
	 *            the fast text
	 * @return the ML trainer
	 */
	@Bean
	//@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.NO)
	@Lazy(value = true)
	public ITrainingEntityPioRepo trainingEntityPioRepo(String autName, IAteFastText fastText) {
		if (this.getRunningModeProperties().getRunningMode().equals(RunningMode.USECASE_APP.name())) {
			autName = this.getRunningModeProperties().getUsecaseAPPTrainingEventName();
		}
		return new TrainingEntityPioRepo(autName, fastText);
	}

	@Bean
	//@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.NO)
	@Lazy(value = true)
	public IMLPredictor mlPredictor(String autName, IAteFastText fastText) {
		if (this.getRunningModeProperties().getRunningMode().equals(RunningMode.USECASE_APP.name())) {
			autName = this.getRunningModeProperties().getUsecaseAPPTrainingEventName();
		}
		return new MLPredictor(autName, fastText);
	}

//	@Bean
//	@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
//	@Lazy(value = true)
//	public TebloudUser requestUser() {
//		return new TebloudUser();
//	}

	@Bean
	@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
	@Lazy(value = true)
	public TebloudUserZone requestUserWorkingZone() {
		return new TebloudUserZone();
	}

	/**
	 * Trainer quebe manager.
	 *
	 * @return the trainer queue manager
	 */
	@Bean
	public TrainerQueueManager trainerQuebeManager() {
		//_log.info("test log4j loggin");
		return new TrainerQueueManager();
	}

	/**
	 * Pio predictor.
	 *
	 * @return the pio predictor
	 */
	@Bean
	//@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.NO)
	@Lazy(value = true)
	public ITebloudPredictor pioPredictor(IMLPredictor appPredictor) {
		return new TebloudPredictor(appPredictor);
	}

	/**
	 * Visible label for invisible input.
	 *
	 * @return the i ui invisibility exceptional pattern
	 */
	@Bean
	// @Scope("prototype")
	public IUiInvisibilityExceptionalPattern visibleLabelForInvisibleInput() {
		// List<IUiInvisibilityExceptionalPattern> retVal = new
		// ArrayList<IUiInvisibilityExceptionalPattern>();
		// retVal.add(new DivInSpan());
		return new VisibleLabelForInvisibleInput();
	}

	@Bean
	// @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode =
	// ScopedProxyMode.NO)
	// @Scope(value = "prototype")
	@Lazy(value = true)
	public IRemoteTebloudFastText remoteFastTextPredictor(String autName) {
//		if (this.getRunningModeProperties().getRunningMode().equals(RunningMode.USECASE_APP.name())) {
//			autName = this.getRunningModeProperties().getUsecaseAPPTrainingEventName();
//		}
//		AteFastText jft = new AteFastText();
//		// Train supervised model
//		System.out.println(IFastTextTrainer.CURRENTDIR);
//		String newFilePathName = IFastTextTrainer.CURRENTDIR + "src/test/resources/models/" + autName + "/supervised.model.bin";
//		String defaultFilePathName = IFastTextTrainer.CURRENTDIR + "src/test/resources/models/supervised.model.bin";
//		// Load model from file
//		File modelFile = new File(newFilePathName);
//		if (modelFile.exists())
//			jft.loadModel(newFilePathName);
//		else
//			jft.loadModel(defaultFilePathName);
//		return jft;
		IRemoteTebloudFastText fastTextClient = new RemoteAteFastTextImpl(fastTextServerHost, fastTextDockerContainername);
		fastTextClient.unloadModel();
		fastTextClient.loadModel(autName, runningModeProperties, this.appProperties);
		return fastTextClient;
	}

//	@Bean
//	//@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.NO)
//	@Lazy(value = true)
//	public IFastTextTrainer fastTextTrainer(String autName) {
//		if (this.getRunningModeProperties().getRunningMode().equals(RunningMode.USECASE_APP.name())) {
//			autName = this.getRunningModeProperties().getUsecaseAPPTrainingEventName();
//		}
//		return new FastTextTrainer(autName);
//	}

	@Bean
	//@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.NO)
	@Lazy(value = true)
	public IScreenProbabilityCalculator screenProbabilityCalculator(ITebloudPredictor pioP) {
		return new ScreenProbabilityCalculator(pioP);
	}

	/**
	 * Out of form input list matcher.
	 *
	 * @return the i uitr on screen label matcher
	 */
	@Bean
	public IUitrOnScreenLabelMatcher outOfFormInputListMatcher() {

		return new OutOfFormInputListMatcher();
	}

	/**
	 * Radio groupped by field set matcher.
	 *
	 * @return the i user input consolidation matcher
	 */
	@Bean
	public IUserInputConsolidationMatcher radioGrouppedByFieldSetMatcher() {

		return new UserInputMatcher(SupportedMatchingParentHtmlTag.FIELDSET, UserCollectableHtmlTag.INPUT,
				Optional.of(HtmlInputType.RADIO));
	}

	@Bean
	public IUserInputConsolidationMatcher checkboxGrouppedByFieldSetMatcher() {

		return new UserInputMatcher(SupportedMatchingParentHtmlTag.FIELDSET, UserCollectableHtmlTag.INPUT,
				Optional.of(HtmlInputType.CHECKBOX));
	}

	@Bean
	public IUserInputConsolidationMatcher radioInputGrouppedByNameAttrMatcher() {

		return new UserInputMatcher(SupportedMatchingInputAttribute.NAME, UserCollectableHtmlTag.INPUT,
				Optional.of(HtmlInputType.RADIO));
	}

	// @Bean
	// public IUserInputConsolidationMatcher
	// inputAndSelectGrouppedByIdPrefixMatcher() {
	//
	// return new UserInputMatcher(SupportedMatchingInputAttribute.ID,
	// new HashSet<UserCollectableHtmlTag>(Arrays.asList(
	// UserCollectableHtmlTag.SELECT,
	// UserCollectableHtmlTag.INPUT)),
	// true);
	// }

	/**
	 * Select groupped by field set matcher.
	 *
	 * @return the i user input consolidation matcher
	 */
	@Bean
	public IUserInputConsolidationMatcher selectGrouppedByFieldSetMatcher() {

		return new UserInputMatcher(SupportedMatchingParentHtmlTag.FIELDSET, UserCollectableHtmlTag.SELECT,
				Optional.empty());
	}

	public RunningModeProperties getRunningModeProperties() {
		return runningModeProperties;
	}

	public void setRunningModeProperties(RunningModeProperties runningModeProperties) {
		this.runningModeProperties = runningModeProperties;
	}
	/**
	 * @return the appProperties
	 */
	public TebloudApplicationProperties getAppProperties() {
		return appProperties;
	}
	/**
	 * @param appProperties the appProperties to set
	 */
	public void setAppProperties(TebloudApplicationProperties appProperties) {
		this.appProperties = appProperties;
	}

	// @Bean
	// //@ConditionalOnClass({ Jackson2ObjectMapperBuilder.class})
	// public MapMixinModule mapMixin() {
	//
	//
	//
	//
	// return new MapMixinModule();
	// }
	// @Bean
	// public MappingJackson2HttpMessageConverter
	// mappingJackson2HttpMessageConverter(MapMixinModule mapMixin) {
	//
	// MappingJackson2HttpMessageConverter jsonConverter = new
	// MappingJackson2HttpMessageConverter(Jackson2ObjectMapperBuilder.json().modules(mapMixin).build());
	//
	// jsonConverter.getObjectMapper().addMixIn(Map.class, MapMixIn.class);
	// jsonConverter.getObjectMapper().addMixIn(List.class, MapMixIn.class);
	// jsonConverter.getObjectMapper().addMixIn(LinkedHashMap.class,
	// LinkedHashMapMixin.class);
	// jsonConverter.getObjectMapper().addMixIn(ArrayList.class,
	// LinkedHashMapMixin.class);
	//
	//
	// return jsonConverter;
	// }

	// @Bean
	// public ObjectMapper mapper() {
	// ObjectMapper objectMapper = new ObjectMapper();
	//// objectMapper.addMixIn(LinkedHashMap.class, MapAnnotationMixIn.class);
	//// objectMapper.addMixIn(LinkedHashMap.class, MapAnnotationMixIn.class);
	//// objectMapper.addMixIn(Map.class, MapAnnotationMixIn.class);
	//// objectMapper.addMixIn(Map.class, MapAnnotationMixIn.class);
	// return objectMapper;
	// }

	// @Bean
	// public Jackson2ObjectMapperBuilder objectMapperBuilder() {
	// Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
	//
	// builder.mixIn(Map.class, MapAnnotationMixIn.class);
	//
	// return builder;
	// }

	// @Bean
	// @Scope("prototype")
	// public WebFormUserInputsCollector webFormUserInputsCollector() {
	// return new WebFormUserInputsCollector();
	// }

}
