/**
 *
 */
package com.bigtester.tebloud.tcg.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

import com.bigtester.tebloud.tcg.rest.interceptor.SecureRequestHandlerInterceptor;

/**
 * @author peidong
 *
 */
@Configuration
@ComponentScan(basePackages = { "com.bigtester.ate.tcg", "com.bigtester.tebloud.tcg" })
public class RestConfig extends org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter {

	@Bean
	public SecureRequestHandlerInterceptor secureRequestHandlerInterceptor() {
		return new SecureRequestHandlerInterceptor();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(secureRequestHandlerInterceptor()).addPathPatterns("/users");
	}

}
