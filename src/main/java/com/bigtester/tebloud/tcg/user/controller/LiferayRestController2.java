package com.bigtester.tebloud.tcg.user.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bigtester.tebloud.tcg.beans.UserBean;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;


/**
 * Rest Controller
 *
 * @author Vianney FAIVRE
 */
@RestController
public class LiferayRestController2 {
	@Autowired
	private org.neo4j.ogm.config.Configuration configNeo4j;
	/**
	 * URL to use: http://localhost:8080/tebloud-tcg-liferay-portlet/services/users
	 *
	 * @return list of users as JSON
	 */
	@RequestMapping(value = "/users2", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<UserBean> getUsers(HttpServletRequest request) throws SystemException {

		List<UserBean> userBeans = new ArrayList<UserBean>();

		for (User user : UserLocalServiceUtil.getUsers(0, 10)) {
			userBeans.add(new UserBean(user.getFirstName(), user.getLastName(), user.getEmailAddress()));
		}

		return userBeans;
	}
	@RequestMapping(value = "/neo4jurl", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public String getNeo4jUrl(HttpServletRequest request) throws SystemException {


		return configNeo4j.driverConfiguration().getURI();
	}

 }
