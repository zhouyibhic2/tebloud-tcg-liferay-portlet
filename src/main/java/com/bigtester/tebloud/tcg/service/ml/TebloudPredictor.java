/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2017, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.tcg.service.ml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.eclipse.jdt.annotation.Nullable;
import org.neo4j.ogm.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.w3c.dom.Document;

import com.bigtester.RunnerGlobalUtils;
//import com.bigtester.ate.tcg.controller.PredictionIOTrainer;
import com.bigtester.ate.tcg.model.ScreenNamePredictResult;
import com.bigtester.ate.tcg.model.ScreenNamePredictStrategy;
import com.bigtester.ate.tcg.model.domain.HTMLSource;
import com.bigtester.ate.tcg.model.domain.PredictedFieldName;
import com.bigtester.ate.tcg.model.domain.UserInputTrainingRecord;
import com.bigtester.ate.tcg.model.domain.UserInputValue;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord.UserInputType;
import com.bigtester.ate.tcg.model.ml.IMLPredictor;
import com.bigtester.ate.tcg.model.ml.MLPredictor;
import com.bigtester.ate.tcg.model.ml.TebloudMachine;
import com.bigtester.ate.tcg.model.ml.TrainingEntityPioRepo;
import com.bigtester.ate.tcg.service.ITebloudPredictor;
import com.bigtester.ate.tcg.service.UitrParser;
import com.bigtester.ate.tcg.service.repository.PredictedFieldNameRepo;
import com.bigtester.ate.tcg.service.repository.ScreenJumperTrainingRecordRepo;
import com.bigtester.ate.tcg.service.repository.ScreenNodeRepo;
import com.bigtester.ate.tcg.service.repository.TestCaseRepo;
import com.bigtester.ate.tcg.service.repository.TestSuiteRepo;
import com.bigtester.ate.tcg.service.repository.UserInputTrainingRecordRepo;
import com.bigtester.ate.tcg.service.repository.UserInputValueRepo;
import com.bigtester.ate.tcg.service.repository.WebDomainRepo;
import com.bigtester.ate.tcg.utils.GlobalUtils;
import com.bigtester.tebloud.tcg.service.ScreenNodeCrud;
import com.bigtester.tebloud.tcg.service.TestCaseNodeCrud;
import com.bigtester.tebloud.tcg.service.TestSuiteNodeCrud;
import com.bigtester.tebloud.tcg.service.UitrCrud;
import com.bigtester.tebloud.tcg.service.WebDomainCrud;
import com.bigtester.tebloud.tcg.service.WindowsSystemFilePickerScreenNodeCrud;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import com.google.common.collect.Sets;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

// TODO: Auto-generated Javadoc
/**
 * This class PioPredictor defines ....
 * @author Peidong Hu
 *
 */
public class TebloudPredictor implements ITebloudPredictor{
	protected static Log _log = LogFactoryUtil.getLog(TebloudPredictor.class);
	/** The template. */
//	@Autowired
//	@Nullable
//	private transient Neo4jOperations template;

	/** The screen node repo. */
	@Autowired
	@Nullable
	private ScreenNodeRepo screenNodeRepo;

	/** The user input value repo. */
	@Autowired
	@Nullable
	private UserInputValueRepo userInputValueRepo;

	/** The predicted field name repo. */
	@Autowired
	@Nullable
	private PredictedFieldNameRepo predictedFieldNameRepo;

	/** The user input training record repo. */
	@Autowired
	@Nullable
	private UserInputTrainingRecordRepo userInputTrainingRecordRepo;

	/** The user click input training record repo. */
	@Autowired
	@Nullable
	private UserInputTrainingRecordRepo userClickInputTrainingRecordRepo;

	@Autowired
	@Nullable
	/** The action element training record repo. */
	private ScreenJumperTrainingRecordRepo actionElementTrainingRecordRepo;

	/** The web domain repo. */
	@Autowired
	@Nullable
	private WebDomainRepo webDomainRepo;

	/** The test case repo. */
	@Autowired
	@Nullable
	private TestCaseRepo testCaseRepo;

	/** The screen node crud. */
	@Autowired
	@Nullable
	private ScreenNodeCrud screenNodeCrud;

	/** The windows system file picker screen node crud. */
	@Autowired
	@Nullable
	private WindowsSystemFilePickerScreenNodeCrud windowsSystemFilePickerScreenNodeCrud; //NOPMD


	/** The web domain crud. */
	@Autowired
	@Nullable
	private WebDomainCrud webDomainCrud;

	/** The test case crud. */
	@Autowired
	@Nullable
	private TestCaseNodeCrud testCaseCrud;
	/** The test suite repo. */
	@Autowired
	@Nullable
	private TestSuiteRepo testSuiteRepo;

	@Autowired
	@Nullable
	private UitrCrud uitrCrud;

	/** The test suite crud. */
	@Autowired
	@Nullable
	private TestSuiteNodeCrud testSuiteCrud;

	/** The neo4j session. */
	@Autowired
	@Nullable
	private transient Session neo4jSession;


	private final IMLPredictor tebloudPredictorMachine;
	/**
	 *
	 */
	public TebloudPredictor(IMLPredictor appPredictor) {
		this.tebloudPredictorMachine = appPredictor;
	}

	public double probabilityPredict(WebElementTrainingRecord uitr, String screenName, String testSuiteName, String subSuiteName, String testCaseName) {
		try {
			return getTebloudPredictorMachine().predictUitrRunDecision(uitr, screenName, testSuiteName, subSuiteName, testCaseName);
		} catch (ExecutionException | InterruptedException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//System.out.println("e");
			return 0d;
		}
	}
	@Deprecated
	public List<WebElementTrainingRecord> pioPredict(
			@RequestBody List<WebElementTrainingRecord> records,
			boolean forRunner)  {

		// for (int i = 0; i < records.size(); i++) {
		records.stream().parallel()

				.forEach(record -> {

//
//						record.setInputMLHtmlCode(record.getInputMLHtmlCode()
//								.replaceAll("\r", "").replaceAll("\n", ""));
//						record.setInputMLHtmlWords(GlobalUtils
//								.removeTagAndAttributeNames(record
//										.getInputMLHtmlCode()));
//						record.setElementCoreHtmlCode(record
//								.getElementCoreHtmlCode().replaceAll("\r", "")
//								.replaceAll("\n", ""));
//						record.setElementCoreHtmlCodeWithoutGuidValue(GlobalUtils
//								.convertToComparableString(record
//										.getElementCoreHtmlCode()));


						if (null != record) {
							GlobalUtils.nomalizeUitr(record);
							try {
								getTebloudPredictorMachine().predictEntity(record, "");

							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							Iterable<? extends WebElementTrainingRecord> existingRecords;
							// TODO need to rewrite the query to avoid the
							// ate-guid in the query.
							// System.out.println("abcd" +
							// record.getElementCoreHtmlCodeWithoutGuidValue());
							existingRecords = getUserInputTrainingRecordRepo()
									.findByElementCoreHtmlCodeWithoutGuidValue(
											record.getElementCoreHtmlCodeWithoutGuidValue());
							int sameLabeledRecordCount = StreamSupport
									.stream(existingRecords.spliterator(), false)
									.collect(
											Collectors.groupingBy(uitr -> uitr
													.getInputLabelName()))
									.size();
							// if
							// (record.getUserInputType().equals(UserInputType.USERINPUT))
							// {
							// existingRecord = getUserInputTrainingRecordRepo()
							// .findByInputMLHtmlCode(tmpMLHtmlCode);
							// } else if
							// (record.getUserInputType().equals(UserInputType.INSCREENJUMPER))
							// {
							// existingRecord =
							// getUserClickInputTrainingRecordRepo()
							// .findByInputMLHtmlCode(tmpMLHtmlCode);
							// }
							// else {
							// existingRecord =
							// getActionElementTrainingRecordRepo()
							// .findByInputMLHtmlCode(tmpMLHtmlCode);
							//
							//
							// }
							// Double confidencetmp =
							// record.getPioPredictConfidence();
							if (sameLabeledRecordCount == 1
									&& existingRecords.iterator().hasNext()) {
								WebElementTrainingRecord tmpWetr = existingRecords
										.iterator().next();
								//
								// TODO for existing record, need to reset the
								// belongToTestCase flag by query the
								// relationship of in_testcase.
								// defautl value of belongToTestCase is true and
								// it is not preserved into db.
								if (tmpWetr.getTestcases().isEmpty())
									tmpWetr.setBelongToCurrentTestCase(false);
								if (forRunner == false) {
									record.setPioPredictConfidence(1.0d);
									record.setUserInputType(tmpWetr
											.getUserInputType());
									record.setActionTrigger(tmpWetr
											.isActionTrigger());
									record.setBelongToCurrentTestCase(tmpWetr
											.isBelongToCurrentTestCase());
									// fix for some nodes in db has no
									// inputLabelName set
									record.setInputLabelName(tmpWetr
											.getInputLabelName() == null ? tmpWetr
											.getPioPredictLabelResult()
											.getValue() : tmpWetr
											.getInputLabelName());
									record.getPioPredictLabelResult()
											.setValue(
													tmpWetr.getInputLabelName() == null ? tmpWetr
															.getPioPredictLabelResult()
															.getValue()
															: tmpWetr
																	.getInputLabelName());
									record.getPioPredictLabelResult().setName(
											record.getPioPredictLabelResult()
													.getValue());
									record.getUserValues().addAll(
											tmpWetr.getUserValues());
								} else {
									tmpWetr.setPioPredictConfidence(1.0d);

									tmpWetr.setElementCoreHtmlCode(record
											.getElementCoreHtmlCode());
									tmpWetr.setElementCoreHtmlCodeWithoutGuidValue(record
											.getElementCoreHtmlCodeWithoutGuidValue());
									tmpWetr.setElementCoreHtmlNode(record
											.getElementCoreHtmlNode());
									tmpWetr.setInputMLHtmlCode(record
											.getInputMLHtmlCode());
									tmpWetr.setInputMLHtmlWords(record
											.getInputMLHtmlWords());
									// fix for some nodes in db has no
									// inputLabelName set
									tmpWetr.setInputLabelName(tmpWetr
											.getPioPredictLabelResult()
											.getValue());

									records.set(records.indexOf(record),
											tmpWetr);

								}
								// records.set(records.indexOf(record),
								// tmpWetr);
								// records.get(i).setPioPredictConfidence(confidencetmp);
								// records.get(i).getPioPredictLabelResult()
								// .setValue(tmpLabel);
							} else {
								if (!forRunner) {
									//for training, we need to see how confident about the predict,
									//if lower than 0.009, we will use the default label to replace the predicted one,
									if (record.getPioPredictConfidence() < 0.008) {
										//System.out.println("defaultLabel = begin");
										String defaultLabel = record.parseDefaultLabelFromTextContent();
										//System.out.println("defaultLabel = parsed");
										if (!defaultLabel.isEmpty()) {
											//System.out.println("defaultLabel = " + defaultLabel);
											Map<String, String> neo4jQueriedLabelValue = record.queryPossibleLabelFromNeo4j(getPredictedFieldNameRepo());
											//System.out.println("defaultLabel = quried");
											if (neo4jQueriedLabelValue.isEmpty()) {
												record.setInputLabelName(defaultLabel);
												record.getPioPredictLabelResult().setValue(defaultLabel);
											} else {
												Iterator<Entry<String,String>> itrEntry = neo4jQueriedLabelValue.entrySet().iterator();
												Entry<String, String> firstEntry = itrEntry.next();
												record.setInputLabelName(firstEntry.getValue());
												record.getPioPredictLabelResult().setValue(firstEntry.getValue());
												Entry<String, String> secondEntry = itrEntry.next();
												if (secondEntry.getValue()!=null) {
													record.getUserValues().add(new UserInputValue(secondEntry.getValue()));
												}

											}
										}
									}

								}


								// TODO performance improvement
								// TODO for any record has no history data in
								// db, need the input type to determine.
								String tmpLabel = record
										.getPioPredictLabelResult().getValue();

								Iterable<UserInputTrainingRecord> allSameFieldUitrs1 = getUserInputTrainingRecordRepo()
										.findByPioPredictLabelResultValue(
												tmpLabel);

								Iterable<UserInputTrainingRecord> allSameFieldUitrs2 = getUserInputTrainingRecordRepo()
										.findByInputLabelName(tmpLabel);
								Set<UserInputTrainingRecord> allSameFieldUitrs = Sets
										.newHashSet(allSameFieldUitrs1);
								allSameFieldUitrs.addAll(Sets
										.newHashSet(allSameFieldUitrs2));
								//System.out.println("tmpLabel = " + tmpLabel);
								//System.out.println("samefieldUitrs.size = " + allSameFieldUitrs.size());
								if (allSameFieldUitrs.iterator().hasNext()) {
									//System.out.println("tmpLabel = " + tmpLabel + " 1");
									UserInputType inputType = StreamSupport
											.stream(allSameFieldUitrs
													.spliterator(), true)
											.collect(
													Collectors
															.groupingBy(
																	WebElementTrainingRecord::getUserInputType,
																	Collectors
																			.counting()))
											.entrySet()
											.stream()
											.sorted(Map.Entry
													.comparingByValue(Comparator
															.reverseOrder()))
											.iterator().next().getKey();
									//System.out.println("tmpLabel = " + tmpLabel + " 2");
									record.setUserInputType(inputType);
									Set<UserInputType> alternativeInputTypes = StreamSupport
											.stream(allSameFieldUitrs
													.spliterator(), true)
											.filter(uitr -> !uitr
													.getUserInputType().equals(
															inputType))
											.collect(Collectors.toSet())
											.stream()
											.map(uitr -> uitr
													.getUserInputType())
											.collect(Collectors.toSet());
									//System.out.println("tmpLabel = " + tmpLabel + " 3");
									record.setAlternativeUserInputTypes(alternativeInputTypes);

									// for (java.util.Iterator<? extends
									// WebElementTrainingRecord> itr =
									// allSameFieldUitrs
									// .iterator(); itr.hasNext();) {
									allSameFieldUitrs
											.stream()
											.forEach(
													itr -> {
														record.getUserValues()
																.addAll(itr
																		.getUserValues());
													});
									//System.out.println("tmpLabel = " + tmpLabel + " 4");
									// }

									// Iterable<? extends
									// WebElementTrainingRecord>
									// allSameFieldActionUitrs = this
									// .getActionElementTrainingRecordRepo()
									// .findByPioPredictLabelResultValue(tmpLabel);
									// for (java.util.Iterator<? extends
									// WebElementTrainingRecord> itr =
									// allSameFieldActionUitrs
									// .iterator(); itr.hasNext();) {
									// record.getUserValues().addAll(
									// itr.next().getUserValues());
									// }
								}
							}
						}
					});
		//System.out.println("tmpLabel = end1");
		return records;
	}

	public Map<ScreenNamePredictStrategy, Map<String, ScreenNamePredictResult>> pagePredict(String testCaseName, String domainName,
			Map<String, List<HTMLSource>> doms, boolean forRunner)
			throws IOException, ClassNotFoundException, ExecutionException,
			InterruptedException {

		Map<ScreenNamePredictStrategy, Map<String, ScreenNamePredictResult>> retVal = new ConcurrentHashMap<ScreenNamePredictStrategy, Map<String, ScreenNamePredictResult>>();// NOPMD
		UitrParser uitrParser = new UitrParser(testCaseName, domainName, null, doms, this);

		//List<HTMLSource> previousDoms = uitrParser.getPreviousHtmlSource().orElse(null);//doms.get("previousDoms");
		//List<HTMLSource> newDoms = uitrParser.getNewHtmlSource();// doms.get("newDoms");
		List<Document> newDoms = uitrParser.getNewDomDocuments();// doms.get("newDoms");
		if (uitrParser.getNewHtmlSource().isEmpty()) {
			Map<String, ScreenNamePredictResult> tmpMap = new ConcurrentHashMap<String, ScreenNamePredictResult>();
			tmpMap.put("", new ScreenNamePredictResult());
			retVal.put(
					ScreenNamePredictStrategy.PREDICTED_BY_VISIBLE_AND_UNPROCESSED_ELEMENT_TEXT,
					tmpMap);
		} else {
			// removeIllegalXmlChars(previousDoms);
			// removeIllegalXmlChars(newDoms);
			//uitrParser has done this in the construction.
			//markPreviousScreenDomProcessedElements(previousDoms, newDoms);
			ScreenNamePredictStrategy[] possibleValues = ScreenNamePredictStrategy
					.values();
			List<String> screenWords = new ArrayList<String>();
			//List<String> screenNames = new ArrayList<String>();
			for (int index = 0; index < possibleValues.length; index++) {
				Map<String, ScreenNamePredictResult> tmpVal = getTebloudPredictorMachine()
						.predictScreenName(newDoms,
								possibleValues[index]);
				if (!screenWords.contains(tmpVal.values().iterator().next()
						.getScreenTrainingWords())){
						//&& !screenNames.contains(tmpVal.keySet().iterator()
								//.next())) {
					retVal.put(possibleValues[index], tmpVal);
					screenWords.add(tmpVal.values().iterator().next()
							.getScreenTrainingWords());
					//screenNames.add(tmpVal.keySet().iterator().next());
				}
			}

		}
		if (forRunner) {
			retVal = retVal
				.entrySet()
				.stream()

				.filter(RunnerGlobalUtils.distinctByKey(mapEntry -> mapEntry
						.getValue().keySet().iterator().next()))
				.collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
		}
		return retVal;
	}
	private void nomalizeUserInputValue(WebElementTrainingRecord record) {
		if (!record.getUserValues().isEmpty()){
			Set<String> temp = record.getUserValues().stream().map(UserInputValue::getValue).collect(Collectors.toSet());
			record.getUserValues().removeIf(uv->{
				//System.out.println("connected ids: " + uv.getGraphId());
				if (uv.getGraphId()!=null) {
					Iterable<Long> uitrs = getUserInputValueRepo().getConnectedUitrIds(uv.getGraphId());
					return Iterators.size(uitrs.iterator())>1 || Iterators.size(uitrs.iterator())==1 && !uitrs.iterator().next().equals(record.getGraphId());
				} else {
					return false;
				}
			});
			if (record.getUserValues().isEmpty()){
				UserInputValue uiv = new UserInputValue(temp.iterator().next());
				record.getUserValues().add(uiv);
			}
		}
	}

	public List<WebElementTrainingRecord> uitrPredict(String domainName, String testCaseName, String screenName,
			List<WebElementTrainingRecord> records, boolean forRunner, String runningMode) {


		// for (int i = 0; i < records.size(); i++) {
		records.stream()
				.filter(rec -> rec.isInputMLHtmlCodeChangedByUI() || forRunner)
				.forEach(record -> {

					if (null != record) {
						GlobalUtils.nomalizeUitr(record);
						try {
							getTebloudPredictorMachine().predictEntity(record, runningMode);
							if (record.getElementCoreHtmlCode().length()<TebloudMachine.MONITORED_NODE_LENGTH && record.getElementCoreHtmlCode().contains(TebloudMachine.MONITORED_NODE_HTML_CODE))
								_log.info(TebloudMachine.MONITORED_NODE_HTML_CODE +" link predicted: " + record.getElementCoreHtmlCode() + ": " + record.getPioPredictConfidence() + ": " + record.getInputMLHtmlWords());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						record.setPioPredictConfidence(-1d);
					}
					}
				});

		if (forRunner) {
			Iterable<String> labelsExpectedOnThisScreen = this.getScreenNodeRepo().getDistinctUserInputLabelNamesInTestcaseOfDomain(domainName, testCaseName, screenName);
			_log.info("labels expected on this screen: " + labelsExpectedOnThisScreen);
//			records.forEach(uitr->{
//				if (uitr.getElementCoreHtmlCode().contains("my first wp post"))
//					_log.info("my first wp post title link node in predictor before filter: " + uitr.getInputLabelName());
//
//				});
			records = records.stream().filter(rec->Iterables.contains(labelsExpectedOnThisScreen, rec.getInputLabelName())).collect(Collectors.toList());
//			records.forEach(uitr->{
//			if (uitr.getElementCoreHtmlCode().contains("my first wp post"))
//				_log.info("my first wp post title link node in predictor: " + uitr.getInputLabelName());
//
//			});
		}
		List<WebElementTrainingRecord> retRecords = records;
		retRecords.stream().forEach(record->{
			if (null != record) {
					Iterable<? extends WebElementTrainingRecord> existingRecords;
					// TODO need to rewrite the query to avoid the
					// ate-guid in the query.
					// System.out.println("abcd" +
					// record.getElementCoreHtmlCodeWithoutGuidValue());
					existingRecords = getUserInputTrainingRecordRepo()
							.findByElementCoreHtmlCodeWithoutGuidValueaInTestCase(
									record.getElementCoreHtmlCodeWithoutGuidValue(),
									testCaseName);
					if (!existingRecords.iterator().hasNext()) {
						existingRecords = getUserInputTrainingRecordRepo()
								.findByElementCoreHtmlCodeWithoutGuidValue(
										record.getElementCoreHtmlCodeWithoutGuidValue());
					}
					int sameLabeledRecordCount = StreamSupport
							.stream(existingRecords.spliterator(), false)
							.collect(
									Collectors.groupingBy(uitr -> uitr
											.getInputLabelName())).size();

					if (sameLabeledRecordCount == 1
							&& existingRecords.iterator().hasNext()) {
						WebElementTrainingRecord tmpExistinWetr = existingRecords
								.iterator().next();
						//
						// TODO for existing record, need to reset the
						// belongToTestCase flag by query the
						// relationship of in_testcase.
						// defautl value of belongToTestCase is true and
						// it is not preserved into db.
						if (tmpExistinWetr
								.getTestcases()
								.stream()
								.filter(tc -> tc.getEndNode() != null
										&& tc.getEndNode().getName()
												.equals(testCaseName)).count() == 0)
							tmpExistinWetr.setBelongToCurrentTestCase(false);
						if (forRunner == false) {
							record.setPioPredictConfidence(1.0d);
							record.setUserInputType(tmpExistinWetr
									.getUserInputType());
							record.setActionTrigger(tmpExistinWetr
									.isActionTrigger());
							record.setBelongToCurrentTestCase(tmpExistinWetr
									.isBelongToCurrentTestCase());
							// fix for some nodes in db has no
							// inputLabelName set
							record.setInputLabelName(tmpExistinWetr
									.getInputLabelName() == null ? tmpExistinWetr
									.getPioPredictLabelResult().getValue()
									: tmpExistinWetr.getInputLabelName());
							record.getPioPredictLabelResult()
									.setValue(
											tmpExistinWetr.getInputLabelName() == null ? tmpExistinWetr
													.getPioPredictLabelResult()
													.getValue()
													: tmpExistinWetr
															.getInputLabelName());
							record.getPioPredictLabelResult()
									.setName(
											tmpExistinWetr.getInputLabelName() == null ? tmpExistinWetr
													.getPioPredictLabelResult()
													.getValue()
													: tmpExistinWetr
															.getInputLabelName());
							if (tmpExistinWetr.getUserValues().size() > 0)
								record.getUserValues().add(
										new UserInputValue(tmpExistinWetr
												.getUserValues().iterator()
												.next().getValue()));
							// nomalizeUserInputValue(record);

						} else {

							tmpExistinWetr.setPioPredictConfidence(1.0d);

							tmpExistinWetr.setElementCoreHtmlCode(record
									.getElementCoreHtmlCode());
							tmpExistinWetr
									.setElementCoreHtmlCodeWithoutGuidValue(record
											.getElementCoreHtmlCodeWithoutGuidValue());
							tmpExistinWetr.setElementCoreHtmlNode(record
									.getElementCoreHtmlNode());
							tmpExistinWetr.setInputMLHtmlCode(record
									.getInputMLHtmlCode());
							tmpExistinWetr.setInputMLHtmlWords(record
									.getInputMLHtmlWords());
							// fix for some nodes in db has no
							// inputLabelName set
							tmpExistinWetr.setInputLabelName(tmpExistinWetr
									.getPioPredictLabelResult().getValue());

							retRecords.set(retRecords.indexOf(record), tmpExistinWetr);

						}
						// records.set(records.indexOf(record),
						// tmpWetr);
						// records.get(i).setPioPredictConfidence(confidencetmp);
						// records.get(i).getPioPredictLabelResult()
						// .setValue(tmpLabel);
					} else {
						// System.out.println("inner2: " +
						// record.getInputLabelName());
						if (!forRunner) {
							// for training, we need to see how confident about
							// the predict,
							// if lower than 0.009, we will use the default
							// label to replace the predicted one,
							if (record.getPioPredictConfidence() < 0.008) {
								// System.out.println("defaultLabel = begin");
								//_log.info("predicted confidence is too low: " + record.getPioPredictConfidence());
								String defaultLabel = record
										.parseDefaultLabelFromTextContent();
								// System.out.println("defaultLabel = parsed");
								if (!defaultLabel.isEmpty()) {
									// System.out.println("defaultLabel = " +
									// defaultLabel);
									Map<String, String> neo4jQueriedLabelValue = record
											.queryPossibleLabelFromNeo4j(getPredictedFieldNameRepo());
									if (record.getPioPredictLabelResult() == null) {
										record.setPioPredictLabelResult(new PredictedFieldName());
									}
									// System.out.println("defaultLabel = quried");
									if (neo4jQueriedLabelValue.isEmpty()) {
										record.setInputLabelName(defaultLabel);
										record.getPioPredictLabelResult()
												.setValue(defaultLabel);
									} else {
										Iterator<Entry<String, String>> itrEntry = neo4jQueriedLabelValue
												.entrySet().iterator();
										Entry<String, String> firstEntry = itrEntry
												.next();
										record.setInputLabelName(firstEntry
												.getValue());
										record.getPioPredictLabelResult()
												.setValue(firstEntry.getValue());
										Entry<String, String> secondEntry = itrEntry
												.next();
										if (secondEntry.getValue() != null) {
											record.getUserValues()
													.add(new UserInputValue(
															secondEntry
																	.getValue()));
										}

									}
								}
							}

						}

						// TODO performance improvement
						// TODO for any record has no history data in
						// db, need the input type to determine.
						String tmpLabel = record.getPioPredictLabelResult()
								.getValue();


						Set<UserInputTrainingRecord> allSameFieldUitrs = Sets
								.newHashSet(getUitrCrud().getAllSaveFieldLabeledUitrs(domainName, testCaseName, tmpLabel));
						// System.out.println("tmpLabel = " + tmpLabel);
						// System.out.println("samefieldUitrs.size = " +
						// allSameFieldUitrs.size());
						// System.out.println("inner3: " +
						// record.getInputLabelName());
						if (allSameFieldUitrs.iterator().hasNext()) {
							// System.out.println("tmpLabel = " + tmpLabel +
							// " 1");
							UserInputType inputType = StreamSupport
									.stream(allSameFieldUitrs.spliterator(),
											true)
									.collect(
											Collectors
													.groupingBy(
															WebElementTrainingRecord::getUserInputType,
															Collectors
																	.counting()))
									.entrySet()
									.stream()
									.sorted(Map.Entry
											.comparingByValue(Comparator
													.reverseOrder()))
									.iterator().next().getKey();
							// System.out.println("tmpLabel = " + tmpLabel +
							// " 2");
							record.setUserInputType(inputType);
							Set<UserInputType> alternativeInputTypes = StreamSupport
									.stream(allSameFieldUitrs.spliterator(),
											true)
									.filter(uitr -> !uitr.getUserInputType()
											.equals(inputType))
									.collect(Collectors.toSet()).stream()
									.map(uitr -> uitr.getUserInputType())
									.collect(Collectors.toSet());
							// System.out.println("tmpLabel = " + tmpLabel +
							// " 3");
							record.setAlternativeUserInputTypes(alternativeInputTypes);

							// for (java.util.Iterator<? extends
							// WebElementTrainingRecord> itr =
							// allSameFieldUitrs
							// .iterator(); itr.hasNext();) {
							if (record.getUserValues().isEmpty()) {
								Iterator<UserInputTrainingRecord> itr = allSameFieldUitrs
										.iterator();
								while (itr.hasNext()) {
									UserInputTrainingRecord utr = itr.next();
									if (!utr.getUserValues().isEmpty()) {
										record.getUserValues().add(
												new UserInputValue(utr
														.getUserValues()
														.iterator().next()
														.getValue()));
										break;
									}
								}
								// .stream()
								// .forEach(
								// itr -> {
								// Set<UserInputValue> itrValues =
								// itr.getUserValues();
								// Set<String> userValueStrs =
								// record.getUserValues().stream().map(UserInputValue::getValue).collect(Collectors.toSet());
								// itrValues.removeIf(val->userValueStrs.contains(val.getValue()));
								//
								// record.getUserValues()
								// .addAll(itrValues);
								// });
							}
							// nomalizeUserInputValue(record);

							// System.out.println("tmpLabel = " + tmpLabel +
							// " 4");
							// }

							// Iterable<? extends
							// WebElementTrainingRecord>
							// allSameFieldActionUitrs = this
							// .getActionElementTrainingRecordRepo()
							// .findByPioPredictLabelResultValue(tmpLabel);
							// for (java.util.Iterator<? extends
							// WebElementTrainingRecord> itr =
							// allSameFieldActionUitrs
							// .iterator(); itr.hasNext();) {
							// record.getUserValues().addAll(
							// itr.next().getUserValues());
							// }
						}
						// System.out.println("inner4: " +
						// record.getInputLabelName());
					}
				}
			}	);
		//retRecords.stream().map(rec -> rec.getInputLabelName())
		//		.forEach(System.out::println);
		return retRecords;
	}

//	/**
//	 * @return the template
//	 */
//	public Neo4jOperations getTemplate() {
//		return template;
//	}
//
//	/**
//	 * @param template the template to set
//	 */
//	public void setTemplate(Neo4jOperations template) {
//		this.template = template;
//	}

	/**
	 * @return the screenNodeRepo
	 */
	public ScreenNodeRepo getScreenNodeRepo() {
		return screenNodeRepo;
	}

	/**
	 * @param screenNodeRepo the screenNodeRepo to set
	 */
	public void setScreenNodeRepo(ScreenNodeRepo screenNodeRepo) {
		this.screenNodeRepo = screenNodeRepo;
	}

	/**
	 * @return the userInputValueRepo
	 */
	public UserInputValueRepo getUserInputValueRepo() {
		return userInputValueRepo;
	}

	/**
	 * @param userInputValueRepo the userInputValueRepo to set
	 */
	public void setUserInputValueRepo(UserInputValueRepo userInputValueRepo) {
		this.userInputValueRepo = userInputValueRepo;
	}

	/**
	 * @return the predictedFieldNameRepo
	 */
	public PredictedFieldNameRepo getPredictedFieldNameRepo() {
		return predictedFieldNameRepo;
	}

	/**
	 * @param predictedFieldNameRepo the predictedFieldNameRepo to set
	 */
	public void setPredictedFieldNameRepo(
			PredictedFieldNameRepo predictedFieldNameRepo) {
		this.predictedFieldNameRepo = predictedFieldNameRepo;
	}

	/**
	 * @return the userInputTrainingRecordRepo
	 */
	public UserInputTrainingRecordRepo getUserInputTrainingRecordRepo() {
		return userInputTrainingRecordRepo;
	}

	/**
	 * @param userInputTrainingRecordRepo the userInputTrainingRecordRepo to set
	 */
	public void setUserInputTrainingRecordRepo(
			UserInputTrainingRecordRepo userInputTrainingRecordRepo) {
		this.userInputTrainingRecordRepo = userInputTrainingRecordRepo;
	}

	/**
	 * @return the userClickInputTrainingRecordRepo
	 */
	public UserInputTrainingRecordRepo getUserClickInputTrainingRecordRepo() {
		return userClickInputTrainingRecordRepo;
	}

	/**
	 * @param userClickInputTrainingRecordRepo the userClickInputTrainingRecordRepo to set
	 */
	public void setUserClickInputTrainingRecordRepo(
			UserInputTrainingRecordRepo userClickInputTrainingRecordRepo) {
		this.userClickInputTrainingRecordRepo = userClickInputTrainingRecordRepo;
	}

	/**
	 * @return the actionElementTrainingRecordRepo
	 */
	public ScreenJumperTrainingRecordRepo getActionElementTrainingRecordRepo() {
		return actionElementTrainingRecordRepo;
	}

	/**
	 * @param actionElementTrainingRecordRepo the actionElementTrainingRecordRepo to set
	 */
	public void setActionElementTrainingRecordRepo(
			ScreenJumperTrainingRecordRepo actionElementTrainingRecordRepo) {
		this.actionElementTrainingRecordRepo = actionElementTrainingRecordRepo;
	}

	/**
	 * @return the webDomainRepo
	 */
	public WebDomainRepo getWebDomainRepo() {
		return webDomainRepo;
	}

	/**
	 * @param webDomainRepo the webDomainRepo to set
	 */
	public void setWebDomainRepo(WebDomainRepo webDomainRepo) {
		this.webDomainRepo = webDomainRepo;
	}

	/**
	 * @return the testCaseRepo
	 */
	public TestCaseRepo getTestCaseRepo() {
		return testCaseRepo;
	}

	/**
	 * @param testCaseRepo the testCaseRepo to set
	 */
	public void setTestCaseRepo(TestCaseRepo testCaseRepo) {
		this.testCaseRepo = testCaseRepo;
	}

	/**
	 * @return the screenNodeCrud
	 */
	public ScreenNodeCrud getScreenNodeCrud() {
		return screenNodeCrud;
	}

	/**
	 * @param screenNodeCrud the screenNodeCrud to set
	 */
	public void setScreenNodeCrud(ScreenNodeCrud screenNodeCrud) {
		this.screenNodeCrud = screenNodeCrud;
	}

	/**
	 * @return the windowsSystemFilePickerScreenNodeCrud
	 */
	public WindowsSystemFilePickerScreenNodeCrud getWindowsSystemFilePickerScreenNodeCrud() {
		return windowsSystemFilePickerScreenNodeCrud;
	}

	/**
	 * @param windowsSystemFilePickerScreenNodeCrud the windowsSystemFilePickerScreenNodeCrud to set
	 */
	public void setWindowsSystemFilePickerScreenNodeCrud(
			WindowsSystemFilePickerScreenNodeCrud windowsSystemFilePickerScreenNodeCrud) {
		this.windowsSystemFilePickerScreenNodeCrud = windowsSystemFilePickerScreenNodeCrud;
	}

	/**
	 * @return the webDomainCrud
	 */
	public WebDomainCrud getWebDomainCrud() {
		return webDomainCrud;
	}

	/**
	 * @param webDomainCrud the webDomainCrud to set
	 */
	public void setWebDomainCrud(WebDomainCrud webDomainCrud) {
		this.webDomainCrud = webDomainCrud;
	}

	/**
	 * @return the testCaseCrud
	 */
	public TestCaseNodeCrud getTestCaseCrud() {
		return testCaseCrud;
	}

	/**
	 * @param testCaseCrud the testCaseCrud to set
	 */
	public void setTestCaseCrud(TestCaseNodeCrud testCaseCrud) {
		this.testCaseCrud = testCaseCrud;
	}

	/**
	 * @return the testSuiteRepo
	 */
	public TestSuiteRepo getTestSuiteRepo() {
		return testSuiteRepo;
	}

	/**
	 * @param testSuiteRepo the testSuiteRepo to set
	 */
	public void setTestSuiteRepo(TestSuiteRepo testSuiteRepo) {
		this.testSuiteRepo = testSuiteRepo;
	}

	/**
	 * @return the testSuiteCrud
	 */
	public TestSuiteNodeCrud getTestSuiteCrud() {
		return testSuiteCrud;
	}

	/**
	 * @param testSuiteCrud the testSuiteCrud to set
	 */
	public void setTestSuiteCrud(TestSuiteNodeCrud testSuiteCrud) {
		this.testSuiteCrud = testSuiteCrud;
	}

	/**
	 * @return the neo4jSession
	 */
	public Session getNeo4jSession() {
		return neo4jSession;
	}

	/**
	 * @param neo4jSession the neo4jSession to set
	 */
	public void setNeo4jSession(Session neo4jSession) {
		this.neo4jSession = neo4jSession;
	}

	/**
	 * @return the appPredictor
	 */
	public IMLPredictor getTebloudPredictorMachine() {
		return tebloudPredictorMachine;
	}

	/**
	 * @return the uitrCrud
	 */
	public UitrCrud getUitrCrud() {
		return uitrCrud;
	}

	/**
	 * @param uitrCrud the uitrCrud to set
	 */
	public void setUitrCrud(UitrCrud uitrCrud) {
		this.uitrCrud = uitrCrud;
	}
}
