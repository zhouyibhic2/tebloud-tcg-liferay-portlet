/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.tcg.service.repository;

import java.util.Map;

import org.eclipse.jdt.annotation.Nullable;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bigtester.ate.tcg.model.domain.WebDomain;
import com.bigtester.tebloud.tcg.model.domain.TebloudUserZone;

// TODO: Auto-generated Javadoc
/**
 * This class ScreenNodeRepo defines ....
 * @author Peidong Hu
 *
 */
@Repository
public interface TebloudUserZoneRepo extends Neo4jRepository<TebloudUserZone, Long> {


	@Query("match (n:Neo4jScreenNode)-[r]-(m:TebloudUserZone) where id(n)={screenNodeId} return m")
	Iterable<Map<String,Object>> getTebloudUserZoneByScreenNodeId(@Param("screenNodeId") Long screenNodeId);

	@Query("match (n:TebloudUserZone) where HAS(n.userIds) and {userId} IN n.userIds and n.zoneDomainName={domainName} return n")
	TebloudUserZone getTebloudUserZoneByDomainNameAndUserId(@Param("domainName") String doaminName, @Param("userId") Long userId);
}
