/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.tcg.service.uitr;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms;
import com.bigtester.ate.tcg.model.domain.HTMLSource;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord.UserInputType;
import com.bigtester.ate.tcg.model.ws.api.ScreenStepsAdvice;
import com.bigtester.ate.tcg.service.ITebloudPredictor;
import com.bigtester.ate.tcg.service.IScreenNodeCrud;

// TODO: Auto-generated Javadoc
/**
 * This class ScreenProbabilityCalculator defines ....
 * @author Peidong Hu
 *
 */

public class ScreenProbabilityCalculator implements IScreenProbabilityCalculator{

	private ITebloudPredictor predictor;
	public static Double PROB_WEIGHT_UNIT = 10000.0;
	//we use .009 as the lowest judgeThreshold
	public static Double PIO_CONFIDENCE_WEIGHT_MULTIPLIER = (ScreenStepsAdvice.judgeThreshold/0.009) * 10;

	public static Double PIO_CONFIDENCE_WEIGHT_UNIT = PROB_WEIGHT_UNIT * PIO_CONFIDENCE_WEIGHT_MULTIPLIER;

	//public static Double ELEMENT_OVERSIZE_WEIGHT_UNIT = PIO_CONFIDENCE_WEIGHT_UNIT;

	//public static Double NUMBER_OF_INPUT_TAG_WEIGHT_UNIT = PROB_WEIGHT_UNIT * 10/3;

	@Autowired
	private IScreenNodeCrud screenNodeCrud;
	/**
	 *
	 */
	public ScreenProbabilityCalculator(ITebloudPredictor pioPredictor) {
		this.predictor = pioPredictor;
	}

	private void addNumberOfVisibleInputProbValue(WebElementTrainingRecord uitr, List<String> userCollectableTags) {
		/* draft of the algrithm
* uitr probability algrithm (Will be done on labels after initializeStepAdvice, which are almost sure that they are part of this screen elements)
weight                 					 			condition
10000											4.0 mlhtml has one visible input or other html element input
5000											4.1 mlhtml has 2 visible inputs elements
-10000* (#-2)									4.2 mlhtml has 3 or more visible inputs elements
if userinput -10000 or clikable 0				4.3 mlhtml has no visible input
*/

					long inputCount=uitr.calculateAteCollectableTagsCount(userCollectableTags);

					Double probValue = 0.0;
					/*10000											4.0 mlhtml has one visible input or other html element input
					5000											4.1 mlhtml has 2 visible inputs elements
					-10000* (#-2)									4.2 mlhtml has 3 or more visible inputs elements
					if userinput -10000 or clikable 0				4.3 mlhtml has no visible input
					*/
					if (inputCount ==0) {
						if(uitr.getUserInputType().equals(UserInputType.USERINPUT))
							probValue = -1.0 * uitr.getPioPredictConfidence() * PIO_CONFIDENCE_WEIGHT_UNIT;
						else
							probValue = 0d;
					} else if (inputCount==1) {
						probValue = uitr.getPioPredictConfidence() * PIO_CONFIDENCE_WEIGHT_UNIT;
					} else if(inputCount ==2) {
						probValue = uitr.getPioPredictConfidence() * PIO_CONFIDENCE_WEIGHT_UNIT/2;
					} else if (inputCount>2) {
						probValue = -1.0* uitr.getPioPredictConfidence() * PIO_CONFIDENCE_WEIGHT_UNIT *(inputCount-2);
					}
					uitr.setScreenUitrProbability(uitr.getScreenUitrProbability() + probValue.longValue());

	}

	private void addPioConfidenceProbValue(WebElementTrainingRecord uitr) {
		/*
		x10000											2.0 pio confidence (0-~1)
		50000											2.1 pio confidence = 1
		*/
		Double probVal = 0.0;
		if (uitr.getPioPredictConfidence()==1.0) {
			probVal = 10000 * PIO_CONFIDENCE_WEIGHT_UNIT;
		} else {
			probVal = uitr.getPioPredictConfidence() * PIO_CONFIDENCE_WEIGHT_UNIT;
		}
		uitr.setScreenUitrProbability(uitr.getScreenUitrProbability() + probVal.longValue());
	}

	private void addLengthProbValue(WebElementTrainingRecord uitr, ScreenStepsAdvice stepAdvice) {
		/*
//(s.d ~ length-avg<0?*-1:length-avg) * 10000		3. mlhtml length range  (get from neo4j db for avg and standard diviation) for this label
if < min(   length(n.inputMLHtmlCode)*1.0/length(dom.domDoc)), -5000
if > min and <max, 10000*percentileDisc(0.1-0.9)
if > max, -5000
		*/
		List<Double> percentileDiscLengthRate = new ArrayList<Double>();
		percentileDiscLengthRate.add(0, 0.0);

		for (int index = 1; index <10; index++) {
			percentileDiscLengthRate.add(index, screenNodeCrud.getMlHtmlCodeVsDomDocPercentileLengthRate(index*1.0/10));
		}
		percentileDiscLengthRate.add(10, 1.0);
		Double minLengthRate = screenNodeCrud.getMinMlHtmlCodeVsDomDocPercentileLengthRate();
		Double maxLengthRate = screenNodeCrud.getMaxMlHtmlCodeVsDomDocPercentileLengthRate();
		Double uitrHtmlLengthRate = uitr.getInputMLHtmlCode().length()
				* 1.0
				/ stepAdvice.getDevToolMessage().getPages().stream()
						.map(HTMLSource::getDomDoc)
						.collect(Collectors.toList()).stream()
						.mapToLong(String::length).sum();
		Double probVal = 0.0;
		if (uitrHtmlLengthRate<minLengthRate) {
			probVal = -1.0 * uitr.getPioPredictConfidence() * PIO_CONFIDENCE_WEIGHT_UNIT/2;
		} else if (uitrHtmlLengthRate>maxLengthRate) {
			probVal = -1.0 * uitr.getPioPredictConfidence() * PIO_CONFIDENCE_WEIGHT_UNIT/2;;
		} else {
			int probValPercentileRange = percentileDiscLengthRate.indexOf(percentileDiscLengthRate
					.stream()
					.filter(rate -> uitrHtmlLengthRate <= rate
							&& uitrHtmlLengthRate > percentileDiscLengthRate
									.get(percentileDiscLengthRate.indexOf(rate) - 1))
					.collect(Collectors.toList()).get(0));
			if (probValPercentileRange <=5) {
				probVal = uitr.getPioPredictConfidence() * PIO_CONFIDENCE_WEIGHT_UNIT * probValPercentileRange*1.0/5;
			} else {
				probVal = uitr.getPioPredictConfidence() * PIO_CONFIDENCE_WEIGHT_UNIT * (10 - probValPercentileRange)*1.0/5;
			}
		}
		uitr.setScreenUitrProbability(uitr.getScreenUitrProbability() + probVal.longValue());
	}
	private void addElementSizeProbValue(WebElementTrainingRecord uitr) {
		/*
//(s.d ~ length-avg<0?*-1:length-avg) * 10000		3. mlhtml length range  (get from neo4j db for avg and standard diviation) for this label
if < min(   length(n.inputMLHtmlCode)*1.0/length(dom.domDoc)), -5000
if > min and <max, 10000*percentileDisc(0.1-0.9)
if > max, -5000
		*/
		Double minSize = screenNodeCrud.getMinElementSize(uitr.getInputLabelName());

		Double uitrSize = uitr.getComputedCssSizes().iterator().next().getUitrSize()
				* 1.0;
		Double probVal = 0.0;
		List<Double> percentileSizeRate = new ArrayList<Double>();

		percentileSizeRate.add(0, minSize);

		for (int index = 1; index <10; index++) {
			percentileSizeRate.add(index, screenNodeCrud.getElementSizePercentile(uitr.getInputLabelName(),(index*1.0/10)));
		}
		//percentileSizeRate.add(10, maxSize);
		//Double maxSize = screenNodeCrud.getMaxElementSize(uitr.getInputLabelName());
		Double maxSize = percentileSizeRate.get(percentileSizeRate.size()-1);
		if (uitrSize<=minSize) {
			probVal = -1.0 * uitr.getPioPredictConfidence() * PIO_CONFIDENCE_WEIGHT_UNIT * (minSize/uitrSize);
		} else if (uitrSize>=maxSize) {
			probVal = -1.0 * uitr.getPioPredictConfidence() * PIO_CONFIDENCE_WEIGHT_UNIT * (uitrSize/maxSize);
		} else {
			int probValPercentileRange = percentileSizeRate.indexOf(percentileSizeRate
					.stream()
					.filter(rate -> uitrSize < rate
							&& uitrSize >= percentileSizeRate
									.get(percentileSizeRate.indexOf(rate) - 1))
					.collect(Collectors.toList()).get(0));
			if (probValPercentileRange <=5) {
				probVal = uitr.getPioPredictConfidence() * PIO_CONFIDENCE_WEIGHT_UNIT * probValPercentileRange*1.0/5;
			} else {
				probVal = uitr.getPioPredictConfidence() * PIO_CONFIDENCE_WEIGHT_UNIT * (10 - probValPercentileRange)*1.0/5;
			}
		}
		uitr.setScreenUitrProbability(uitr.getScreenUitrProbability() + probVal.longValue());
	}
	private void addType5ProbValue(WebElementTrainingRecord uitr,
			Map<WebElementTrainingRecord, List<WebElementTrainingRecord>> probabilityReadyForCalculateUitrsForThisScreen, List<WebElementTrainingRecord> sameLabeledUitrs,
			ScreenStepsAdvice advice,
			Map<WebElementTrainingRecord, List<WebElementTrainingRecord>> predictedUitrsWithSameLabeleds
			) {
		/*
		-10000*#			5.0 mlHtml has embeded the other high confidence same labeled uitr mlhtml (confidence higher than threshold)
		-3000*#				5.1 mlHtml has embeded the other high confidence different labeled uitr mlhtml for this screen. ((confidence higher than threshold)
		-10000*#			5.1 mlHtml has embeded the other high confidence different labeled uitr mlhtml for this screen. ((confidence higher than threshold), and in this label row, confidence is lower than others.
		*/
		//TODO need to twick this core. it will be common to have same labeled children as independent uitr. how we are going to decide about the score?
		int includeSameLabeledHighConfidenceUitrSize = sameLabeledUitrs
				.stream()
				.filter(sameLabeledUitr -> uitr.getInputMLHtmlCode().indexOf(
						sameLabeledUitr.getCoreGuid()) > uitr.getInputMLHtmlCode()
						.indexOf(uitr.getCoreGuid()) && sameLabeledUitr.getPioPredictConfidence()>advice.getJudgeThreshold())
				.collect(Collectors.toList()).size();
		int includeOtherLabeledHighConfidenceUitrSize = predictedUitrsWithSameLabeleds
				.entrySet()
				.stream()
				.map(Map.Entry::getValue)
				.collect(Collectors.toList())
				.stream()
				.flatMap(Collection::stream)
				.collect(Collectors.toList())
				.stream()
				.filter(otherLabeledUitr -> uitr.getInputMLHtmlCode().indexOf(
						otherLabeledUitr.getCoreGuid()) > uitr
						.getInputMLHtmlCode().indexOf(uitr.getCoreGuid())
						&& otherLabeledUitr.getPioPredictConfidence() > advice
								.getJudgeThreshold())
				.collect(Collectors.toList()).size();

		int includeOtherLabeledHighConfidenceUitrForThisScreenSize =	probabilityReadyForCalculateUitrsForThisScreen
				.entrySet()
				.stream()
				.map(Map.Entry::getValue)
				.collect(Collectors.toList())
				.stream()
				.flatMap(Collection::stream)
				.collect(Collectors.toList())
				.stream()
				.filter(otherLabeledUitr -> uitr.getInputMLHtmlCode().indexOf(
						otherLabeledUitr.getCoreGuid()) > uitr
						.getInputMLHtmlCode().indexOf(uitr.getCoreGuid())
						&& otherLabeledUitr.getPioPredictConfidence() > advice
								.getJudgeThreshold())
				.collect(Collectors.toList()).size() ;



		long probVal = 0;
		if (includeSameLabeledHighConfidenceUitrSize>0 && (includeOtherLabeledHighConfidenceUitrForThisScreenSize + includeOtherLabeledHighConfidenceUitrSize) > 0) {
			probVal = -10000 * includeSameLabeledHighConfidenceUitrSize;
		}
		if (includeOtherLabeledHighConfidenceUitrForThisScreenSize>0) {
			probVal = -10000 * includeOtherLabeledHighConfidenceUitrForThisScreenSize;
		}
		if (includeOtherLabeledHighConfidenceUitrSize>0){
			List<WebElementTrainingRecord> sameLabeledUitrsSortedByConfidence = sameLabeledUitrs.stream().sorted(Comparator.comparing(uitr2->uitr2.getPioPredictConfidence())).collect(Collectors.toList());
			if (sameLabeledUitrsSortedByConfidence.indexOf(uitr)<sameLabeledUitrsSortedByConfidence.size()-1) {
				probVal = probVal + -10000 * includeOtherLabeledHighConfidenceUitrSize;
			} else {
				probVal = probVal + -3000 * includeOtherLabeledHighConfidenceUitrSize;
			}
		}
		uitr.setScreenUitrProbability(uitr.getScreenUitrProbability() + probVal);
	}

	public void caculateProbability(
			Map<WebElementTrainingRecord, List<WebElementTrainingRecord>> probabilityReadyForCalculateUitrsForThisScreen,
			ScreenStepsAdvice stepAdvice,
			Map<WebElementTrainingRecord, List<WebElementTrainingRecord>> predictedUitrsWithSameLabeledsMap) {
		/* draft of the algrithm
* uitr probability algrithm (Will be done on labels after initializeStepAdvice, which are almost sure that they are part of this screen elements)
weight                 					 			condition
10000											4.0 mlhtml has one visible input or other html element input
-10000*#										5.0 mlHtml has embeded the other high confidence same labeled uitr mlhtml (confidence higher than threshold)
-5000*#											5.1 mlHtml has embeded the other high confidence different labeled uitr mlhtml for this screen. ((confidence higher than threshold)
(s.d ~ length-avg<0?*-1:length-avg) * 10000		3. mlhtml length range  (get from neo4j db for avg and standard diviation) for this label
x10000											2.0 pio confidence (0-~1)
5000											4.1 mlhtml has 2 visible inputs elements
-10000* (#-2)									4.2 mlhtml has 3 or more visible inputs elements
if userinput -10000 or clikable 0				4.3 mlhtml has no visible input
50000											2.1 pio confidence = 1
*/


		List<String> userCollectableTags = Arrays.asList(WebFormUserInputsCollectorHtmlTerms.USER_COLLECTABLE_TAGS_EXCEPT_ALINK);

		probabilityReadyForCalculateUitrsForThisScreen.entrySet().parallelStream().parallel().forEach(entryP->{

			if (entryP.getValue().size()>1) {
			//there are multiple same labeled canadidates, need to calculate prob to sort them.
			//TODO, need to rebalance the algrithm of the probability calculation.
			//Email on uploadrusume and continue page is a good example,
			//Email predicted label html could have Subject code (with lower confidence), but get higher prob score, need to debug the reason.
			//Email predicted correctly, with higher confidence, but get lower prob score, the reason may be because we give the weight of have same label child too high?


				entryP.getValue().parallelStream().parallel().forEach(uitr->{
					uitr.setScreenUitrProbability((long) (10 * 10000
							* this.getPredictor().probabilityPredict(uitr, stepAdvice.getScreenName(),
									stepAdvice.getTestSuiteMap().get(0).getName(),
									stepAdvice.getTestSuiteMap().get(1).getName(), stepAdvice.getTestCaseName())
							* uitr.getPioPredictConfidence()));
//					if (uitr.getScreenUitrProbability()==0) {
//						addPioConfidenceProbValue(uitr);
//						addLengthProbValue(uitr, stepAdvice);
//						addNumberOfVisibleInputProbValue(uitr, userCollectableTags);
//						addElementSizeProbValue(uitr);
//					//addType5ProbValue(uitr, probabilityReadyForCalculateUitrsForThisScreen, entryP.getValue(), stepAdvice, predictedUitrsWithSameLabeledsMap);
//					}
				});
			}
		});
	}

	/**
	 * @return the screenNodeCrud
	 */
	public IScreenNodeCrud getScreenNodeCrud() {
		return screenNodeCrud;
	}

	/**
	 * @param screenNodeCrud the screenNodeCrud to set
	 */
	public void setScreenNodeCrud(IScreenNodeCrud screenNodeCrud) {
		this.screenNodeCrud = screenNodeCrud;
	}

	/**
	 * @return the predictor
	 */
	public ITebloudPredictor getPredictor() {
		return predictor;
	}

	/**
	 * @param predictor the predictor to set
	 */
	public void setPredictor(ITebloudPredictor predictor) {
		this.predictor = predictor;
	}

}
