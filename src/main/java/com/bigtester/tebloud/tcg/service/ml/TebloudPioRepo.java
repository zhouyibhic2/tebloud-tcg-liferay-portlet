/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.tcg.service.ml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Node;




//import com.bigtester.ate.tcg.controller.PredictionIOTrainer;
//import com.bigtester.ate.tcg.controller.PredictionIOTrainer.MyPair;
import com.bigtester.ate.tcg.model.IntermediateResult;
import com.bigtester.ate.tcg.model.domain.ComputedCssSize;
import com.bigtester.ate.tcg.model.domain.InScreenJumperTrainingRecord;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord;
import com.bigtester.ate.tcg.model.ml.IRemoteTebloudFastText;
import com.bigtester.ate.tcg.model.ml.ITrainingEntityPioRepo;
import com.bigtester.ate.tcg.model.ml.TrainingEntityPioRepo;
import com.bigtester.ate.tcg.service.ITebloudPredictor;
import com.bigtester.ate.tcg.service.UitrParser;
import com.bigtester.ate.tcg.utils.GlobalUtils;
import com.bigtester.tebloud.tcg.config.RunningModeProperties;
import com.bigtester.tebloud.tcg.config.RunningModeProperties.RunningMode;
import com.bigtester.tebloud.tcg.config.TebloudApplicationProperties;
import com.bigtester.tebloud.tcg.ws.BaseWsController;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

// TODO: Auto-generated Javadoc
/**
 * This class PioUitrTrainer defines ....
 * @author Peidong Hu
 *
 */
final public class TebloudPioRepo implements Supplier<IntermediateResult>{
	protected static Log _log = LogFactoryUtil.getLog(BaseWsController.class);


	private RunningModeProperties trainingMode;

	private TebloudApplicationProperties appProperties;

	final IntermediateResult iResult;
	final ITebloudPredictor pioPredictor;
	final ITrainingEntityPioRepo appTrainer;
	/**
	 *
	 */
	public TebloudPioRepo(IntermediateResult iResult, ITebloudPredictor pioPredictor, ITrainingEntityPioRepo appTrainer, RunningModeProperties trainingMode, TebloudApplicationProperties appProperties) {
		this.iResult = iResult;
		this.pioPredictor = pioPredictor;
		this.appTrainer = appTrainer;
		this.trainingMode = trainingMode;
		this.appProperties = appProperties;
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public IntermediateResult get() {
		// TODO Auto-generated method stub
		_log.info("start running the future");
		return trainAndSaveIResults(this.iResult);
	}

	public IntermediateResult trainAndSaveIResults(IntermediateResult intermediateResult) {
		try {
			intermediateResult = trainIntermediateResult(intermediateResult, this.getPioPredictor());
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			_log.info("trainingEventRepo saving failed: " + e.getCause().getMessage());
		}
		//TODO this could be a problem for cache in session
		//getNeo4jSession().clear();

		return intermediateResult;


	}

	public IntermediateResult trainIntermediateResult(
			IntermediateResult intermediateResult, ITebloudPredictor pioPredictor)
			throws IOException, ClassNotFoundException, ExecutionException,
			InterruptedException {

		if (intermediateResult.isSamePageUpdate()) {
			if (!intermediateResult.getScreenNode().getScreenTrainedEntityId().isEmpty()) {
				try{
					getAppTrainer().deleteScreenNameTrainingEntity(intermediateResult.getScreenNode().getScreenTrainedEntityId());
				} catch (Exception e) {
					_log.info("trainingEventRepo deletescreenNameTrainingEntity failed: " + e.getMessage());
				}
			}
			if (!intermediateResult.getScreenNode().getUitrTrainedEntityIds().isEmpty()) {
				intermediateResult.getScreenNode().getUitrTrainedEntityIds().forEach(iId->{
					try {
						getAppTrainer().deleteUitrTrainingEntity(iId);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						_log.info("trainingEventRepo deleteUitrTrainedEntityIds failed: " + e.getMessage());
					}
				});
			}
			if (!intermediateResult.getScreenNode().getDecisionUitrTrainedEntityIds().isEmpty()) {
				intermediateResult.getScreenNode().getDecisionUitrTrainedEntityIds().forEach(iId->{
					try {
						getAppTrainer().deleteUitrDecisionTrainingEntity(iId);
					} catch (Exception e) {
						_log.info("trainingEventRepo deleteDecisionUitrTrainedEntityIds failed: " + e.getMessage());
					}
				});
			}
		}
		intermediateResult.getScreenNode().setScreenTrainedEntityId("");
		intermediateResult.getScreenNode().getUitrTrainedEntityIds().clear();
		intermediateResult.getScreenNode().getDecisionUitrTrainedEntityIds().clear();




		try {
			for (WebElementTrainingRecord uitr : intermediateResult.getScreenNode()
					.getUserInputUitrs()) {
				//GlobalUtils.nomalizeUitr(uitr, intermediateResult.getTopDocumentWidth(), intermediateResult.getTopDocumentHeight());
				// uitr.setPioPredictConfidence(1.0);
				//TODO distinguish create or update operation, if it is update, trainingInputPio needs to update the event records
				//if (StringUtils.isEmpty(uitr.getTrainedResult())) {
					trainInputPIO(uitr, intermediateResult.getScreenNode().getName(), true,  intermediateResult.getTestSuitesMap().get(0).getName(), intermediateResult.getTestSuitesMap().get(1).getName(), intermediateResult.getTestCaseName());
					intermediateResult.getScreenNode().getUitrTrainedEntityIds().add(uitr.getTrainedResult());
//					List<WebElementTrainingRecord> childrenUitrs = uitr.parseChildUitrs();
//					if (childrenUitrs.size()>1) {
//						childrenUitrs.forEach(childUitr->{
//							try {
//								trainInputPIO(uitr, intermediateResult.getScreenName());
//							} catch (Exception e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//						});
//					}
				//}
//				if (uitr.getElementCoreHtmlCode().isEmpty()) {
//					uitr.setElementCoreHtmlCode(uitr.getInputMLHtmlCode());
//				}
//				if (uitr.getElementCoreHtmlCodeWithoutGuidValue().isEmpty()) {
//					uitr.setElementCoreHtmlCodeWithoutGuidValue(GlobalUtils
//							.convertToComparableString(uitr
//									.getElementCoreHtmlCode()));
//				}

			}

			for (WebElementTrainingRecord uitr : intermediateResult.getScreenNode()
					.getActionUitrs()) {
				// uitr.setPioPredictConfidence(1.0);
				//GlobalUtils.nomalizeUitr(uitr, intermediateResult.getTopDocumentWidth(), intermediateResult.getTopDocumentHeight());
				//if (StringUtils.isEmpty(uitr.getTrainedResult())) {
					trainInputPIO(uitr, intermediateResult.getScreenNode().getName(), true, intermediateResult.getTestSuitesMap().get(0).getName(), intermediateResult.getTestSuitesMap().get(1).getName(), intermediateResult.getTestCaseName());
					intermediateResult.getScreenNode().getUitrTrainedEntityIds().add(uitr.getTrainedResult());
//					List<WebElementTrainingRecord> childrenUitrs = uitr.parseChildUitrs();
//					if (childrenUitrs.size()>1) {
//						childrenUitrs.forEach(childUitr->{
//							try {
//								trainInputPIO(uitr, intermediateResult.getScreenName());
//							} catch (Exception e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//						});
//					}
				}
//				if (uitr.getElementCoreHtmlCode().isEmpty()) {
//					uitr.setElementCoreHtmlCode(uitr.getInputMLHtmlCode());
//				}
//				if (uitr.getElementCoreHtmlCodeWithoutGuidValue().isEmpty()) {
//					uitr.setElementCoreHtmlCodeWithoutGuidValue(GlobalUtils
//							.convertToComparableString(uitr
//									.getElementCoreHtmlCode()));
//				}

			//}
			for (WebElementTrainingRecord uitr : intermediateResult.getScreenNode()
					.getScreenElementChangeUitrs()) {
				// uitr.setPioPredictConfidence(1.0);
				//GlobalUtils.nomalizeUitr(uitr, intermediateResult.getTopDocumentWidth(), intermediateResult.getTopDocumentHeight());
				//if (StringUtils.isEmpty(uitr.getTrainedResult())) {
					trainInputPIO(uitr, intermediateResult.getScreenNode().getName(), true, intermediateResult.getTestSuitesMap().get(0).getName(), intermediateResult.getTestSuitesMap().get(1).getName(), intermediateResult.getTestCaseName());
					intermediateResult.getScreenNode().getUitrTrainedEntityIds().add(uitr.getTrainedResult());
//					List<WebElementTrainingRecord> childrenUitrs = uitr.parseChildUitrs();
//					if (childrenUitrs.size()>1) {
//						childrenUitrs.forEach(childUitr->{
//							try {
//								trainInputPIO(uitr, intermediateResult.getScreenName());
//							} catch (Exception e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//						});
//					}
				//}
				// for case that user has made up everything on ui without
				// prediction processing
//				if (uitr.getElementCoreHtmlCode().isEmpty()) {
//					uitr.setElementCoreHtmlCode(uitr.getInputMLHtmlCode());
//				}
//				if (uitr.getElementCoreHtmlCodeWithoutGuidValue().isEmpty()) {
//					uitr.setElementCoreHtmlCodeWithoutGuidValue(GlobalUtils
//							.convertToComparableString(uitr
//									.getElementCoreHtmlCode()));
//				}

			}

			for (WebElementTrainingRecord uitr : intermediateResult.getScreenNode().getClickUitrs()
					) {
				// uitr.setPioPredictConfidence(1.0);
				GlobalUtils.normalizeUitr(uitr, intermediateResult.getScreenNode().getScreenWidth(), intermediateResult.getScreenNode().getTopDocumentWidth(), intermediateResult.getScreenNode().getTopDocumentHeight());
				//if (StringUtils.isEmpty(uitr.getTrainedResult())) {

					trainInputPIO(uitr, intermediateResult.getScreenNode().getName(), true, intermediateResult.getTestSuitesMap().get(0).getName(), intermediateResult.getTestSuitesMap().get(1).getName(), intermediateResult.getTestCaseName());
					intermediateResult.getScreenNode().getUitrTrainedEntityIds().add(uitr.getTrainedResult());
//					List<WebElementTrainingRecord> childrenUitrs = uitr.parseChildUitrs();
//					if (childrenUitrs.size()>1) {
//						childrenUitrs.forEach(childUitr->{
//							try {
//								trainInputPIO(uitr, intermediateResult.getScreenName());
//							} catch (Exception e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//						});
//					}
				//}
//				if (uitr.getElementCoreHtmlCode().isEmpty()) {
//					uitr.setElementCoreHtmlCode(uitr.getInputMLHtmlCode());
//				}
//				if (uitr.getElementCoreHtmlCodeWithoutGuidValue().isEmpty()) {
//					uitr.setElementCoreHtmlCodeWithoutGuidValue(GlobalUtils
//							.convertToComparableString(uitr
//									.getElementCoreHtmlCode()));
//				}

				if (uitr instanceof InScreenJumperTrainingRecord
						&& ((InScreenJumperTrainingRecord) uitr).isActionTrigger()) {
					int clickTimes = ((InScreenJumperTrainingRecord) uitr)
							.getClickTimes() + 1;
					((InScreenJumperTrainingRecord) uitr).setClickTimes(clickTimes);
					// ((InScreenJumperTrainingRecord)
					// uitr).setActionTrigger(false);
				}
				// if (uitr instanceof InScreenJumperTrainingRecord &&
				// ((InScreenJumperTrainingRecord) uitr).isActionTrigger()) {
				// int clickTimes = ((InScreenJumperTrainingRecord)
				// uitr).getClickTimes() + 1;
				// ((InScreenJumperTrainingRecord) uitr).setClickTimes(clickTimes);
				// ((InScreenJumperTrainingRecord) uitr).setActionTrigger(false);
				// }
			}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				_log.info("trainingEventRepo userTrainedUitr failed: " + GlobalUtils.parseStackTrace(e));
			}

		//Retrain model and reload the model
		String autName = intermediateResult.getDomainName();
		String retrainResult = getAppTrainer().trainRemoteFastTextModel(intermediateResult.getDomainName(), this.appProperties.getFasttext().getTrainingParams());
		if (retrainResult.contains("Failed"))
			retrainResult = getAppTrainer().trainRemoteFastTextModel(intermediateResult.getDomainName(), this.appProperties.getFasttext().getTrainingParams());
		_log.info("retrain fasttext result: " + retrainResult);
		if (this.getPioPredictor().getTebloudPredictorMachine().getAteFastText() instanceof IRemoteTebloudFastText) {
			((IRemoteTebloudFastText) this.getPioPredictor().getTebloudPredictorMachine().getAteFastText()).unloadModel();
			((IRemoteTebloudFastText) this.getPioPredictor().getTebloudPredictorMachine().getAteFastText()).loadModel(autName, this.trainingMode, this.appProperties);
			Thread.sleep(5000);
			_log.info("reload fast model done. ");
		}
		/////////////////////////////

		UitrParser uitrParser;
		if (intermediateResult
				.getLastScreenNodeIntermediateResult()==null) {
			uitrParser = new UitrParser(intermediateResult.getTestCaseName(), intermediateResult.getDomainName(), intermediateResult.getScreenNode().getName(),
					intermediateResult.getScreenNode().getSourcingDoms(), pioPredictor);
		} else {
			uitrParser = new UitrParser(intermediateResult.getTestCaseName(), intermediateResult.getDomainName(), intermediateResult.getScreenNode().getName(),
					intermediateResult.getLastScreenNodeIntermediateResult().getScreenNode().getSourcingDoms(),
				intermediateResult.getScreenNode().getSourcingDoms(), pioPredictor);
		}

		Map<Node, Double[]> pageWidthSimplingNodes = uitrParser.parseAllWidthSamplingNodes();
		double maxX = pageWidthSimplingNodes.values().stream().filter(value->value[1]>=0 && value[0]>=0).max((p1, p2)->Double.compare(p1[1], p2[1])).get()[1];
		double minX = pageWidthSimplingNodes.values().stream().filter(value->value[1]>=0 && value[0]>=0).min((p1, p2)->Double.compare(p1[0], p2[0])).get()[0];
		int effectiveWidthOfPage = (int) Math.round(maxX - minX);

		List<WebElementTrainingRecord> allUserTrainedUitrs = new ArrayList<WebElementTrainingRecord>();
		allUserTrainedUitrs.addAll(intermediateResult.getScreenNode().getUserInputUitrs());
		allUserTrainedUitrs.addAll(intermediateResult.getScreenNode().getActionUitrs());
		allUserTrainedUitrs.addAll(intermediateResult.getScreenNode().getScreenElementChangeUitrs());
		allUserTrainedUitrs.addAll(intermediateResult.getScreenNode().getClickUitrs());
		_log.info("start train user uitrs into decision repo");
		for (WebElementTrainingRecord uitr : allUserTrainedUitrs) {
			if (uitr!=null) {
				GlobalUtils.normalizeUitr(uitr, intermediateResult.getScreenNode().getScreenWidth(), effectiveWidthOfPage, intermediateResult.getScreenNode().getTopDocumentHeight());
				uitr.getComputedCssSizes().iterator().next().setLeftTopCornerX(uitr.getComputedCssSizes().iterator().next().getLeftTopCornerX()-minX);
				trainInputDecisionPIO(uitr, intermediateResult.getScreenNode().getName(), true, intermediateResult.getTestSuitesMap().get(0).getName(), intermediateResult.getTestSuitesMap().get(1).getName(), intermediateResult.getTestCaseName());
			}
		}

		_log.info("train user uitrs into decision repo done ");



				//List<WebElementTrainingRecord> allNewCreatedRawPredictedUitrs = parseAllNewCreatedUitrsRawPredicted(doms);



		List<WebElementTrainingRecord> allNewCreatedRawPredictedUitrs = uitrParser.parseAllNewCreatedUitrsRawPredicted(this.trainingMode.getRunningMode());

		//for (WebElementTrainingRecord uitr : allNewCreatedRawPredictedUitrs) {
		allNewCreatedRawPredictedUitrs.forEach(uitr->{
			if (uitr!=null) {
				GlobalUtils.normalizeUitr(uitr, intermediateResult.getScreenNode().getScreenWidth(), effectiveWidthOfPage, intermediateResult.getScreenNode().getTopDocumentHeight());
				uitr.getComputedCssSizes().iterator().next().setLeftTopCornerX(uitr.getComputedCssSizes().iterator().next().getLeftTopCornerX()-minX);
			}
		});
		_log.info("normalize all raw uitrs done");
		if (intermediateResult.getLastScreenNodeIntermediateResult()!=null &&
				!StringUtils.isEmpty(intermediateResult.getLastScreenNodeIntermediateResult()
						.getScreenNode().getName())
				&& !intermediateResult.getLastScreenNodeIntermediateResult()
				.getScreenNode().getName()
				.equalsIgnoreCase(intermediateResult.getScreenNode().getName())
				&& (GlobalUtils.getBodyEquevelantElement(uitrParser.getPreviousHtmlSource().get().get(0).getDomDocument()
						)).getAttribute("ate-guid").equalsIgnoreCase(
						(GlobalUtils.getBodyEquevelantElement(uitrParser.getNewHtmlSource().get(0).getDomDocument()))
								.getAttribute("ate-guid"))) {
			_log.info("popup html screen occurs");
			//popup html screen occurs, adjust location y to center of the page
			List<WebElementTrainingRecord> popupContainers = allNewCreatedRawPredictedUitrs.stream().parallel().filter(newCreatedUitr->{


				List<WebElementTrainingRecord> notIncludedUserTraingedUitrsInThisUitr = allUserTrainedUitrs.stream().parallel().filter(userUitr->{
					if (!StringUtils.isEmpty(userUitr.getCoreGuid()) && !newCreatedUitr.getElementCoreHtmlCode().contains(userUitr.getCoreGuid()))
						return true;
					return false;
				}).collect(Collectors.toList());

				if (notIncludedUserTraingedUitrsInThisUitr.size()>0) return false;
				return true;
			}).collect(Collectors.toList());

			if (popupContainers.size()>0) {
				//adjust all newCreatedUitrs ylocation
				//1. calculate the center y distance
				ComputedCssSize popContainerDimension = popupContainers.get(0).getComputedCssSizes().iterator().next();
				//double popupCenterX = popContainerDimension.getLeftTopCornerX() + popContainerDimension.getUitrWidth()/2;
				double popupCenterY = popContainerDimension.getLeftTopCornerY() + popContainerDimension.getUitrHeight()/2;
				double documentCenterY = popContainerDimension.getTopDocumentHeight()/2;
				double distanceY = documentCenterY - popupCenterY;
				allNewCreatedRawPredictedUitrs.stream().parallel().forEach(rawUitr->{
					rawUitr.getComputedCssSizes().iterator().next().setLeftTopCornerY(distanceY + rawUitr.getComputedCssSizes().iterator().next().getLeftTopCornerY());

				});
				allUserTrainedUitrs.stream().parallel().forEach(userUitr->{
					userUitr.getComputedCssSizes().iterator().next().setLeftTopCornerY(distanceY + userUitr.getComputedCssSizes().iterator().next().getLeftTopCornerY());
				});
			}

		}
		_log.info("before nonUsertrainedUitrs run, condition is: " + !this.trainingMode.getRunningMode().equals(RunningMode.SINGLE_APP_TESTING.toString()));
		//if (!this.trainingMode.getRunningMode().equals(RunningMode.SINGLE_APP_TESTING.toString())) {
			List<WebElementTrainingRecord> nonUserTrainedUitrs = uitrParser.parseNonUserTrainedHtmlNodeUitrsForTrainer(
					allNewCreatedRawPredictedUitrs,

					allUserTrainedUitrs
	//						.stream()
	//						.filter(userUitr -> userUitr
	//								.isBelongToCurrentTestCase())
	//						.collect(Collectors.toList())
							);


			nonUserTrainedUitrs.forEach(nonUserUitr->{

					try {
						if (!StringUtils.isEmpty(intermediateResult.getScreenNode().getName())) {
							intermediateResult.getScreenNode().getDecisionUitrTrainedEntityIds().add(getAppTrainer().sentUitrDecisionTrainingEntityToAppEngine(nonUserUitr,
									intermediateResult.getScreenNode().getName(), false, intermediateResult.getTestSuitesMap().get(0).getName(), intermediateResult.getTestSuitesMap().get(1).getName(), intermediateResult.getTestCaseName()));
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						_log.info("trainingEventRepo saveDecisionTrainingEntityToAppEngine failed: " + nonUserUitr.getInputLabelName() + ":" + e.getMessage());
					}

			});
		//}
		_log.info("before extedned core uitr parents and children saving");
		allUserTrainedUitrs.forEach(userTrainedUitrExtendedParentsAndChildren -> {

			try {
				if (!StringUtils.isEmpty(intermediateResult.getScreenNode().getName())) {
					intermediateResult.getScreenNode().getDecisionUitrTrainedEntityIds()
							.add(getAppTrainer().sentUitrDecisionTrainingEntityToAppEngine(
									userTrainedUitrExtendedParentsAndChildren,
									intermediateResult.getScreenNode().getName(), true, intermediateResult.getTestSuitesMap().get(0).getName(), intermediateResult.getTestSuitesMap().get(1).getName(), intermediateResult.getTestCaseName()));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				_log.info("trainingEventRepo userTrainedUitrExtendedParentsAndChildren failed: "
						+ userTrainedUitrExtendedParentsAndChildren.getInputLabelName() + ":" + ExceptionUtils.getStackTrace(e));
			}

		});
		_log.info("finished sending uitr decision data");


		// Optional<IntermediateResult> previousScreen =
		// Optional.ofNullable(intermediateResult.getLastScreenNodeIntermediateResult());
		// List<HTMLSource> previousDoms = new ArrayList<HTMLSource>();
		// previousDoms.addAll(previousScreen.map(IntermediateResult::getDomStrings).orElse(Collections.emptySet()));
		//
		// List<HTMLSource> newDoms = new ArrayList<HTMLSource>();
		// newDoms.addAll(intermediateResult.getDomStrings());
		// if
		// (!intermediateResult.getScreenName().equalsIgnoreCase(previousScreen.map(IntermediateResult::getScreenName).orElse("")))
		// {
		// markPreviousScreenDomProcessedElements(previousDoms, newDoms);
		// }

		// PredictionIOTrainer.trainScreenName(
		// new HashSet<HTMLSource>(newDoms),
		// intermediateResult.getScreenName(),
		// intermediateResult.getScreenNamePredictStrategy());

		try {
		intermediateResult.getScreenNode().setScreenTrainedEntityId(getAppTrainer().trainScreenNameForAppEngine(
				intermediateResult.getScreenNode().getInputMLHtmlWords(),
				intermediateResult.getScreenNode().getName()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			_log.info("trainingEventRepo screenName failed: " + e.getMessage());
		}
		_log.info("finish training repo saving");
		return intermediateResult;

	}

	private WebElementTrainingRecord trainInputPIO(
			WebElementTrainingRecord record, String screeName, boolean isUserTrainedUitr, String testSuiteName, String subSuiteName, String testCaseName)
			throws ExecutionException, InterruptedException, IOException {
		// List<Greeting> greetings = new ArrayList<Greeting>();

		if (null != record) {
			String eventId = getAppTrainer().sentUitrTrainingEntity(record, this.trainingMode.getRunningMode());
			record.setTrainedResult(eventId);
			_log.info("uitr sent to trainingrepo: " + eventId + ":" + record.getInputLabelName());
//			if (!StringUtils.isEmpty(screeName)) {
//				getAppTrainer().sentUitrDecisionTrainingEntityToAppEngine(record,
//						screeName, isUserTrainedUitr,testSuiteName, subSuiteName, testCaseName);
//			}
		}

		return record;
	}

	private WebElementTrainingRecord trainInputDecisionPIO(
			WebElementTrainingRecord record, String screeName, boolean isUserTrainedUitr, String testSuiteName, String subSuiteName, String testCaseName)
			throws ExecutionException, InterruptedException, IOException {
		// List<Greeting> greetings = new ArrayList<Greeting>();

		if (null != record) {
			if (!StringUtils.isEmpty(screeName)) {
				getAppTrainer().sentUitrDecisionTrainingEntityToAppEngine(record,
						screeName, isUserTrainedUitr,testSuiteName, subSuiteName, testCaseName);
			}
		}

		return record;
	}

	/**
	 * @return the predictor
	 */
	public ITebloudPredictor getPioPredictor() {
		return pioPredictor;
	}



	/**
	 * @return the iResult
	 */
	public IntermediateResult getiResult() {
		return iResult;
	}



	/**
	 * @return the appTrainer
	 */
	public ITrainingEntityPioRepo getAppTrainer() {
		return appTrainer;
	}
}
