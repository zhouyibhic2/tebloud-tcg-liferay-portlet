/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.tebloud.tcg.service.uitr;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bigtester.ate.tcg.controller.WebFormUserInputsCollectorHtmlTerms;
import com.bigtester.ate.tcg.model.domain.HTMLSource;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord.UserInputType;
import com.bigtester.ate.tcg.model.ws.api.ScreenStepsAdvice;
import com.bigtester.ate.tcg.service.ITebloudPredictor;
import com.bigtester.ate.tcg.service.IScreenNodeCrud;

// TODO: Auto-generated Javadoc
/**
 * This class ScreenProbabilityCalculator defines ....
 * @author Peidong Hu
 *
 */

public interface IScreenProbabilityCalculator {
	
	
	public void caculateProbability(
			Map<WebElementTrainingRecord, List<WebElementTrainingRecord>> probabilityReadyForCalculateUitrsForThisScreen,
			ScreenStepsAdvice stepAdvice,
			Map<WebElementTrainingRecord, List<WebElementTrainingRecord>> predictedUitrsWithSameLabeledsMap) ;
	/**
	 * @return the screenNodeCrud
	 */
	public IScreenNodeCrud getScreenNodeCrud() ;

	/**
	 * @param screenNodeCrud the screenNodeCrud to set
	 */
	public void setScreenNodeCrud(IScreenNodeCrud screenNodeCrud);

	/**
	 * @return the predictor
	 */
	public ITebloudPredictor getPredictor() ;

	/**
	 * @param predictor the predictor to set
	 */
	public void setPredictor(ITebloudPredictor predictor) ;

}
